$(document).ready(function () {

    const plans = JSON.parse($('#js_plans').val());
    $('#js-plan').on('change', function () {
        calc($('#js-amount').val());
    });
    $('#js-amount').on('change keyup', function () {
        calc($(this).val());
    });
    $(document).ready(function () {
        calc($('#js-amount').val());
    });
    function calc(amount) {
        let plan_id = Number($('#js-plan').val());
//        console.log(plans);
        let percent = 0;
        let count = 0;
        let seconds = 0;
        let plan_return = 0;
        let check = false;
        if (plans) {
            plans.forEach(function (plan) {
                if (amount >= Number(plan['min']) && amount <= Number(plan['max']) && plan_id === Number(plan['id'])) {
                    count = Number(plan['count']);
                    percent = Number(plan['percent']);
                    seconds = Number(plan['seconds']);
                    plan_return = Number(plan['return']);
                    check = true;
                }
            });
        }
        if (check) {
            const total = (amount / 100 * percent) * (count === 0 ? 1 : count) + Number(plan_return === 1 ? amount : 0);
            $('#js-profit').val((total - amount).toFixed(2) + ' ' + 'USD');
            $('#js-total').val(total.toFixed(2) + ' ' + 'USD');
        } else {
            $('#js-profit').val('-----');
            $('#js-total').val('-----');
        }
    }

    $('#plan').on('change', function () {
        setTimeout(function () {
            if ($('.isActive').hasClass('forDate')) {
                // console.log('data calc');
                calculateFor();
            }
            else {
                // console.log('no data calc');
                calculate();
            }
        }, 100);

    });

    $('#amount').on('input', function () {
        if ($('.isActive').hasClass('forDate')) {
            setTimeout(function () {
                calculateFor();
            }, 50);
        }
        else {
            setTimeout(function () {
                calculate();
            }, 50);
        }
    });

    $('#plan1').on('change', function () {
        if (($(this).val() == '2.1%') || ($(this).val() == '2.3%') || ($(this).val() == '2.9%')) {
            calculateFor1();
        }
        else {
            calculate1();
        }
    });

    $('#amount1').on('input', function () {
        if (($('#plan1').val() == '2.1%') || ($('#plan1').val() == '2.3%') || ($('#plan1').val() == '2.9%')) {
            calculateFor1();
        }
        else {
            calculate1();
        }
    });


    $('select#questions').on('change', function () {
        var _select_val = $(this).val();
		alert(_select_val);

        $(".category_block_none").each(function () {
            $(this).css('display', 'none');
        });

        $("#" + _select_val).css('display', 'block');

    });


    $(".question_block .answer").hide().prev().click(function () {
        $(this).parents(".question_block").find(".answer").not(this).slideUp().prev().removeClass("active");
        $(this).next().not(":visible").slideDown().prev().addClass("active");
    });


    // $(".news_block .news_detailes").prev().click(function() {
    //     $(this).parents(".news_block").find(".news_detailes").not(this).slideUp().prev().removeClass("active");
    //     $(this).next().not(":visible").slideDown().prev().addClass("active");
    // });

    $(".news_block").click(function () {
        $(this).next().slideToggle();
    });

    $('ul.top_menu li').each(function () {
        if (this.getElementsByTagName("a")[0].href == location.href) this.className = "active";
    });
    $('ul.left_navigation_menu li').each(function () {
        if (this.getElementsByTagName("a")[0].href == location.href) this.className = "active";
    });

    $(".language_select a").click(function () {
        $("ul.languages_list").toggle();
    });

    $(".show_plans").click(function () {
        $(".investment_inner").slideUp().removeClass("active");
        $(".investment_inner").not(":visible").slideDown().addClass("active");
        $('#foo2').carouFredSel(properties);
    });


    $("#foo2 li.invest_item").on('click', function () {
        count(this);
    });


    if ($('.paralax_zone').length) {
        var scene = document.getElementById('scene'),
        parallax = new Parallax(scene);
    }

    $('#plan').on('change', function () {
        $('.invest_item.isActive').removeClass('isActive');
        $('.invest_item[data-percent="' + this.value + '"]').addClass('isActive');

        var carouselIndex = $('#plan option').index($('#plan option[value="' + this.value + '"]'));

        $('#foo2').trigger('slideTo', carouselIndex - 1);

        setTimeout(function () {
            run_Slider(false)
        }, 500);
    });


    //scroll to top button
    $(window).scroll(function () {
        var scB = $(window).scrollTop() - $(document).height() + $(window).height();
        //console.log(scB);

        if ($(this).scrollTop() > 200) {
            $('.to-top-arrow').addClass('visible');
            if (scB > -300) {
                $('.to-top-arrow').addClass('bottom-position');
            }
            else {
                $('.to-top-arrow').removeClass('bottom-position');
            }
        }
        else {
            $('.to-top-arrow').removeClass('visible');
        }
    });

    $('.to-top-arrow').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });


    //<10$ not availiable
    function payBalance() {
        var balance = $('.pay-sys-balance .value');
        balance.each(function () {
            var value = parseInt($(this).html());
            if (value < 10) {
                $(this).closest('.pay-sys').addClass('disabled').css('opacity', '0.5');
            }
            else {
                $(this).closest('.pay-sys').removeAttr('style').removeClass('disabled');
            }
        });
        // $('.pay-sys.disabled').click(false);
        // $('.pay-sys.disabled').click(function(){return false;});
    }

    payBalance();




    $(document).off('focus', '#referal-popup form input').on('focus', '#referal-popup form input', function () {
        $(this).closest('div').removeClass('error');
    });

    $(document).off('click', '#referal-popup form button[type="submit"]').on('click', '#referal-popup form button[type="submit"]', function () {
        var inputs = $(this).closest('form').find('input');
        $.each(inputs, function (index, el) {
            if ($.trim($(el).val()) == '') {
                $(el).closest('div').addClass('error');
            }
        });
        return $(this).closest('form').find('div.error').length == 0;
    });

    /*
        $('#referal-popup form').validate({
            rules: {
                country: {
                    required: true,
                    lettersonly: true
                },
                name: {
                    required: true,
                    lettersonly: true
                },
                username: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true,
                },
                language: {
                    required: true,
                    lettersonly: true
                },
                skype: {
                    required: true,
                },
                social: {
                    required: true
                },
                tel: {
                    required: true,
                    number: true
                }
            },
            highlight: function(element) {
                $(element).parent('div').addClass('error');
            },
            unhighlight: function(element) {
                $(element).parent('div').removeClass('error');
            }
        });
        */


    //DEPOSIT BALANCE INPUT
    var select = $('.choose-pay-system .pay-sys');
    select.on('click', function () {
        var val = parseFloat($(this).attr('data-money'));
        $('#amount').attr('value', val);
        // $('#amount').val(val);
        // console.log(val, $('#amount').val());
    });


    countriesAnimation();
    bannerTabs();
    signUpPlaceholder();
});


function calculateFor() {
    var plan = $('#plan').val(),
    amount = $('#amount').val();
    var days = $('.isActive .forDays').html();

    var summ = (((parseFloat(plan) * parseFloat(days)) / 100) * parseFloat(amount)) + parseFloat(amount);

    $('#percent').text(plan);
    if (summ) {
        $('#total').text(summ.toFixed(2));
    }
}

function calculateFor1() {
    var plan = $('#plan1').val(),
    amount = $('#amount1').val();

    switch (plan) {
        case '2.1%':
        var days = 15;
        break;
        case '2.3%':
        var days = 30;
        break;
        case '2.9%':
        var days = 55;
        break;
    }

    var summ = (((parseFloat(plan) * parseFloat(days)) / 100) * parseFloat(amount)) + parseFloat(amount);

    $('#percent1').text(plan);
    if (summ) {
        $('#total1').text(summ);
    }
}

function calculate() {
    var plan = $('#plan').val(),
    amount = $('#amount').val();

    var summ = parseFloat(plan) * parseFloat(amount) / 100;

    $('#percent').text(plan);
    if (summ)
        $('#total').text(summ);
}

function calculate1() {
    var plan = $('#plan1').val(),
    amount = $('#amount1').val();

    var summ = parseFloat(plan) * parseFloat(amount) / 100;

    $('#percent1').text(plan);
    if (summ)
        $('#total1').text(summ);
}

function count(_this) {
    var total_percent = $(_this).attr('data-percent');

    $('#plan').val(total_percent);
    $('#percent').text(total_percent);
}

var calcBtn = $('.calculator_btn_block a');
calcBtn.on('click', function () {
    var setVal = $(this).attr('data-plan');
    $('#plan1').val(setVal);
    $('#amount1').val('');
    $('#total1').text('');
    $('#percent1').text($('#plan1').val());
});


//extenal function on skip button click
function skipTour() {
    $('#foo2').carouFredSel({
        prev: '.prev',
        next: '.next',
        width: 1192,
        height: 550,
        items: {
            visible: 4,
        },
        mousewheel: false,
        circular: false,
        infinite: false,
        scroll: {
            items: 1
        },
        auto: {
            play: true,
            timeoutDuration: 2000,
            delay: 0,
            pauseOnHover: true
        },
    });
}

$('.start-tour').on('click', function () {
    skipTour();
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

if (getCookie('skip-intro') === undefined) {
    $('.start-tour').click();
}
setCookie('skip-intro', '1', {path: '/', expires: 60 * 60 * 24 * 365});


///////////////////
// $(".anim-img").animated("scaleIn", "scaleOut");

///////////////////


function countriesAnimation() {
    $('.countries .item').on('click', function () {
        if ($(this).hasClass('collapsed')) {
            $(this).removeClass('collapsed');
        }
        else {
            $('.countries .item').removeClass('collapsed');
            $(this).toggleClass('collapsed');
        }
    });
}

function bannerTabs() {
    $('.banner-tabs a').on('click', function (e) {
        e.preventDefault();
        $('.banner-tabs a').removeClass('active-tab');
        $(this).addClass('active-tab');
    });
    $('a[href="#trumpBanners"]').on('click', function (e) {
        $('#trumpBanners').addClass('active');
        $('#refBanners').removeClass('active');
        $('#ctxBanners').removeClass('active');
    });
    $('a[href="#ctxBanners"]').on('click', function (e) {
        $('#ctxBanners').addClass('active');
        $('#refBanners').removeClass('active');
        $('#trumpBanners').removeClass('active');
    });
    $('a[href="#refBanners"]').on('click', function (e) {
        $('#ctxBanners').removeClass('active');
        $('#trumpBanners').removeClass('active');
        $('#refBanners').addClass('active');
    });
}

function signUpPlaceholder() {
    var acc = $('.accounts tr');
    acc.each(function () {
        var cls = $(this).attr('class');
        switch (cls) {
            case 'PerfectMoney':
            $(this).find('.cell').attr('placeholder', 'U1234567');
            break;
            case 'NixMoney':
            $(this).find('.cell').attr('placeholder', 'U12345678901234');
            break;
            case 'Payeer':
            $(this).find('.cell').attr('placeholder', 'P1234567');
            break;
            case 'AdvCash':
            $(this).find('.cell').attr('placeholder', 'sample@domain.zn');
            break;
            case 'Bitcoin':
            $(this).find('.cell').attr('placeholder', '39sfuA8pY4UfybgEZi7uvA13jkGzZpsg5K');
            break;
            default:
            break;
        }
    });

}

/*bs modal*/

// $(document).ready(function(){
//     $('button[data-target="#cardModal"]').on('click', function(){
//         // $('#cardModal').addClass('modal-active');
//         // $('#cardModal').removeClass('fade');
//         $('#cardModal').css({'display':'block', 'z-index':'100', 'background-color':'rgba(0,0,0,.4)'});
//         $('#cardModal button.close').
//     });
// });
