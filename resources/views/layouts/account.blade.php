<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
		<link type="image/png" sizes="32x32" rel="icon" href="{{ asset('components/images/favicon.png') }}" />
		<title>ArunaSP</title>

		<!-- Web Fonts
		============================================= -->
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

		<!-- Stylesheet
		============================================= -->
		<link rel="stylesheet" type="text/css" href="{{ asset('components/vendor/bootstrap/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('components/vendor/font-awesome/css/all.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('components/vendor/daterangepicker/daterangepicker.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('components/css/stylesheet.css') }}" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-R20G4QFBGX"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-R20G4QFBGX');
		</script>		
</head>
<body>
@if(auth()->user()->email_verified_at != null)

<div id="main-wrapper"> 

	@include('partial.first_header')
	@include('partial.second_header')

  <!-- Content
  ============================================= -->
  <div id="content" class="py-4">
    <div class="container">
      <div class="row"> 

		@include('partial.components.left_menu')

		@yield('content')
        
      </div>
    </div>
  </div>
  <!-- Content end --> 

</div>

@include('partial.footer')

@else
    <div class="container center">
        <h1 style="text-align: center">@lang('my-text.key_317')!</h1>
    </div>
@endif

	<script src="{{ asset('components/vendor/jquery/jquery.min.js') }}"></script> 
	<script src="{{ asset('components/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script> 
	<script src="{{ asset('components/vendor/daterangepicker/moment.min.js') }}"></script> 
	<script src="{{ asset('components/vendor/daterangepicker/daterangepicker.js') }}"></script> 
	<script>
	$(function() {
	 'use strict';
	  // Birth Date
	  $('#birthDate').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		autoUpdateInput: false,
		maxDate: moment().add(0, 'days'),
		}, function(chosen_date) {
	  $('#birthDate').val(chosen_date.format('MM-DD-YYYY'));
	  });
	  });
	</script> 
	<script src="{{ asset('components/js/theme.js') }}"></script> 
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


<script>
    function selectText(elementId) {
        var doc = document,
            text = doc.getElementById(elementId),
            range,
            selection;
        if (doc.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }}
    $(".copy").click(function() {
        selectText(this.id);
        document.execCommand("copy");
        alert('Cсылка скопирована в буфер обмена!');
    });
	
	$('#new_avatar').change(function (){

		$.ajax({
			url : "{{url('upload-avatar')}}",
			type : 'POST',
			data : new FormData($("#upload_form")[0]),
			processData: false,
			contentType: false,
			success: function(res) {

				alert('Аватар загружен успешно!');
				window.location.reload();

			},
			error: function (error) {

				alert('System Error!');

			}
		});	
	});

	function AddDeposit()
	{
		$('#error_plan').html('');
		$('#error_amount').html('');

		var formData = new FormData();
        formData.append("_token", $('#_token').val());
        formData.append("amount", $('#depo_amount').val());
        formData.append("plan", $('#depo_plan').val());
		
		$.ajax({
			url : "{{url('add-deposit')}}",
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success: function(res) {

				if(res.response)
				{
					Swal.fire({
						icon: 'success',
						title: "Вклад успешно добавлен!",
						confirmButtonColor: "#66BB6A"
					}).then((result) => {
						window.location.href = "{{url('account/deposit-history')}}";
					});
				}
				else
				{
					if(res.code == 0)
					{
						$('#error_plan').html(res.text);
					}
					else if(res.code == 1)
					{
						$('#error_amount').html(res.text);
					}
				}

			},
			error: function (error) {

				alert('System Error!');

			}
		});	
		
	}

	function Withdrawal()
	{
		$('#error_ps').html('');
		$('#error_w').html('');
		$('#error_amount').html('');

		var formData = new FormData();
        formData.append("_token", $('#_token').val());
        formData.append("payment_system", $('#payment_system').val());
        formData.append("wallet", $('#wallet').val());
        formData.append("amount", $('#amount').val());
        formData.append("payment_type", $('#payment_type').val());
		
		$.ajax({
			url : "{{url('payment-output')}}",
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success: function(res) {

				if(res.response)
				{
					Swal.fire({
						icon: 'success',
						title: "Заявка на вывод средств добавлена",
						confirmButtonColor: "#66BB6A"
					}).then((result) => {
						window.location.href = "{{url('account/payment-history')}}";
					});
				}
				else
				{
					if(res.code == 0)
					{
						$('#error_ps').html(res.text);
					}
					else if(res.code == 1)
					{
						$('#error_w').html(res.text);
					}					
					else if(res.code == 2)
					{
						$('#error_amount').html(res.text);
					}					
				}

			},
			error: function (error) {

				alert('System Error!');

			}
		});	
		
	}

	function AddTop()
	{
		var formData = new FormData();
        formData.append("_token", $('#_token_top').val());
        formData.append("uid", $('#uid_top').val());
        formData.append("dt", $('#dt_top').val());
        formData.append("amount", $('#amount_top').val());
        formData.append("system", $('#system_top').val());
        formData.append("status", $('#status_top').val());
		
		$.ajax({
			url : "{{url('add-top')}}",
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success: function(res) {

				$('#new a')[0].click();
				Swal.fire({
					icon: 'success',
					title: "Пополнение успешно добавлено!",
					confirmButtonColor: "#66BB6A"
				}).then((result) => {
					location.reload();

				});

			},
			error: function (error) {

				alert('System Error!');

			}
		});	
		
	}

	function AddRefOther()
	{
		var formData = new FormData();
        formData.append("_token", $('#_token_other').val());
        formData.append("uid", $('#uid_other').val());
        formData.append("call", $('#call').val());
        formData.append("count_ref", $('#count_ref').val());
        formData.append("active_ref", $('#active_ref').val());
        formData.append("amount_ref", $('#amount_ref').val());
		
		$.ajax({
			url : "{{url('add-other')}}",
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success: function(res) {

				$('#new a')[0].click();
				Swal.fire({
					icon: 'success',
					title: "Данные успешно сохранены!",
					confirmButtonColor: "#66BB6A"
				}).then((result) => {
					location.reload();

				});

			},
			error: function (error) {

				alert('System Error!');

			}
		});	
		
	}

	function AddRef()
	{
		var formData = new FormData();
        formData.append("_token", $('#_token_ref').val());
        formData.append("uid", $('#uid_ref').val());
        formData.append("login", $('#login').val());
        formData.append("dt", $('#dt_ref').val());
        formData.append("depo", $('#depo').val());
        formData.append("referals", $('#referals').val());
        formData.append("level", $('#level').val());
		
		$.ajax({
			url : "{{url('add-ref')}}",
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success: function(res) {

				$('#new a')[0].click();
				Swal.fire({
					icon: 'success',
					title: "Реферал успешно добавлен!",
					confirmButtonColor: "#66BB6A"
				}).then((result) => {
					location.reload();

				});

			},
			error: function (error) {

				alert('System Error!');

			}
		});	
		
	}

	function EditProfile()
	{
		var formData = new FormData();
        formData.append("_token", $('#_token_profile').val());
        formData.append("uid", $('#uid_profile').val());
        formData.append("balance", $('#balance').val());
        formData.append("last_depo", $('#last_depo').val());
        formData.append("total", $('#total').val());
        formData.append("ext_amount", $('#ext_amount').val());
        formData.append("withdrawals", $('#withdrawals').val());
        formData.append("active_depo", $('#active_depo').val());
        formData.append("total_depo", $('#total_depo').val());
		
		$.ajax({
			url : "{{url('edit-profile')}}",
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success: function(res) {

				$('#new a')[0].click();
				Swal.fire({
					icon: 'success',
					title: "Профиль успешно обновлен!",
					confirmButtonColor: "#66BB6A"
				}).then((result) => {
					location.reload();

				});
				
			},
			error: function (error) {

				alert('System Error!');

			}
		});	
		
	}

</script>

</body>
</html>
