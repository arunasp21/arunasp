@extends('layouts.account')

@section('content')

    <div class="card-page__wrapper">
        <div class="card-page__column">
            <img class="card-page__img" src="{{ asset('components/img/card-page__img.png') }}" alt="">
            <ul class="card-page__list">
                <li class="card-page__item">Банковская карта с балансом аккаунта <br>
                    Баланс аккаунта в личном кабинете - равен бонусу карты
                </li>
                <li class="card-page__item">Оплата в магазинах</li>
                <li class="card-page__item">Снятие наличных в банкоматах по всему миру - 1.5% комиссия
                </li>
                <li class="card-page__item">Бесплатное годовое обслуживание</li>
                <li class="card-page__item">Доставка почтой России или курьером <br> <b>Доставка карты
                        за 7 дней</b></li>
                <li class="card-page__item">Перевод электронных денежных средств - ООО “банк раунд”.
                </li>
            </ul>
            <button class="card-page__btn" data-toggle="modal" data-target="#cardModal">Активировать
                карту
            </button>
        </div>
        <div class="card-page__column">
            <form action="https://razleton.com/handlers/new_card.php" class="card-page__form"
                  method="post">
                <input type="hidden" name="csrf" value="9b2e3bfcaf28fb447160a08636dc6b38">
                <input type="hidden" name="from" value="/user/card/"> <label for="name"
                                                                             class="card-page__label">Ф.И.О</label>
                <input id="name" name="name" type="text" class="card-page__input" value=""
                       placeholder="Иванов Иван Иванович" required="">
                <label for="email" class="card-page__label">E-Mail</label>
                <input id="email" name="email" type="email" class="card-page__input" value=""
                       placeholder="ivanov.ivan@mail.ru" required="">
                <label for="country" class="card-page__label">Страна</label>
                <input id="country" name="country" type="text" class="card-page__input" value=""
                       placeholder="Российская Федерация" required="">
                <label for="city" class="card-page__label">Город</label>
                <input id="city" name="city" type="text" class="card-page__input" value=""
                       placeholder="Москва" required="">
                <label for="adress" class="card-page__label">Улица/дом/квартира</label>
                <input id="adress" name="address" type="text" class="card-page__input" value=""
                       placeholder="ул. Трубецкая, 16-32" required="">
                <label for="index" class="card-page__label">Индекс</label>
                <input id="index" name="postal_code" type="text" class="card-page__input" value=""
                       placeholder="100101" required="">
                <label for="phone" class="card-page__label">Номер телефона</label>
                <input id="phone" name="phone" type="tel" class="card-page__input" value=""
                       placeholder="+79531579648" required="">
                {{--<div id="captcha_modal_card">--}}
                    {{--<div style="width: 304px; height: 78px;">--}}
                        {{--<div>--}}
                            {{--<iframe src="./card_files/anchor.html" width="304" height="78"--}}
                                    {{--role="presentation" name="a-53wed0mv2hrh" frameborder="0"--}}
                                    {{--scrolling="no"--}}
                                    {{--sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>--}}
                        {{--</div>--}}
                        {{--<textarea id="g-recaptcha-response" name="g-recaptcha-response"--}}
                                  {{--class="g-recaptcha-response"--}}
                                  {{--style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <input class="card-page__btn" type="submit" value="Заказать карту">
            </form>
        </div>
    </div>

@endsection