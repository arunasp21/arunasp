@extends('layouts.account')

@section('content')

    <div class="tpl_wrap withdraw clearfix">
            <h3 style="float: left"><strong>Текущие плани:</strong> (активних может быть только 4)</h3>
            <a href="{{(count($plans) >= 4) ? '#' : route('plans.create')}}" style="float: right" type="button" class="btn btn-primary btn-lg" {{(count($plans) >= 4) ? 'disabled' : ''}}>Create plan</a>
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">План</th>
                    <th scope="col">Сума</th>
                    <th scope="col">Дней</th>
                    <th scope="col">Проценти</th>
                    <th scope="col">Действия</th>
                </tr>
                </thead>
                <tbody>
                @if (isset($plans))
                    @foreach($plans as $plan)
                        <tr>
                            <td scope="row">{{$plan->title}}</td>
                            <td>{{$plan->from_amount . ' - ' .$plan->to_amount}}</td>
                            <td>{{$plan->day}}</td>
                            <td>{{$plan->percent}}</td>
                            <td><a href="{{route('plans.edit', ['plan' => $plan->id])}}" class="btn btn-warning">Edit</a></td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
    </div>

@endsection