@extends('layouts.account')

@section('content')

    <div class="tpl_wrap withdraw clearfix">
        <h3>{{$action . ' '}} plan</h3>
        <form id="run_form" method="post" action="{{isset($plan) ? route('plans.update', ['plan' => $plan->id]) : route('plans.store')}}">
            @csrf
            @if (isset($plan))
                @method('PUT')
            @endif
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername1">Title</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="validationCustomUsername1" placeholder="title" value="{{isset($plan) ? $plan->title : ''}}" name="title" aria-describedby="inputGroupPrepend" required>
                        <div class="invalid-feedback">
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername2">From amount</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="validationCustomUsername2" value="{{isset($plan) ? $plan->from_amount : ''}}" placeholder="10" name="from_amount" aria-describedby="inputGroupPrepend" required>
                        <div class="invalid-feedback">
                            @if ($errors->has('from_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('from_amount') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername3">To amount</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="validationCustomUsername3" value="{{isset($plan) ? $plan->to_amount : ''}}" placeholder="1500" name="to_amount" aria-describedby="inputGroupPrepend" required>
                        <div class="invalid-feedback">
                            @if ($errors->has('to_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('to_amount') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername4">Day</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="validationCustomUsername4" value="{{isset($plan) ? $plan->day : ''}}" placeholder="Day" name="day" aria-describedby="inputGroupPrepend" required>
                        <div class="invalid-feedback">
                            @if ($errors->has('day'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('day') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername5">Percent</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="validationCustomUsername5" value="{{isset($plan) ? $plan->percent : ''}}" placeholder="12.5" name="percent" aria-describedby="inputGroupPrepend" required>
                        <div class="invalid-feedback">
                            @if ($errors->has('percent'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('percent') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12 mb-12" style="margin-top: 5%">
                    <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                </div>
            </div>

        </form>
    </div>

@endsection