@extends('layouts.account')

@section('content')
    <form method="post" action="{{route('update.statistics')}}">
        @csrf
    <div class="tpl_wrap withdraw clearfix">
        <h3 style="float: left"><strong> Статистика:</strong></h3>
        <table class="table table-hover table-dark">
            <tbody>
               <tr>
                   <td>Основана</td>
                   <td><input name="founded_date" value="{{$statistic->founded_date}}"></td>
               </tr>
               <tr>
                   <td>Старт</td>
                   <td><input name="start_date" value="{{$statistic->start_date}}"></td>
               </tr>
               <tr>
                   <td>Дней работы</td>
                   <td><input name="day_works" value="{{$statistic->day_works}}"></td>
               </tr>
               <tr>
                   <td>Всего пользователей</td>
                   <td><input name="count_users" value="{{$statistic->count_users}}"></td>
               </tr>
               <tr>
                   <td>Всего вкладов</td>
                   <td><input name="all_input" value="{{$statistic->all_input}}"></td>
               </tr>
               <tr>
                   <td>Всего выплат</td>
                   <td><input name="all_output" value="{{$statistic->all_output}}"></td>
               </tr>
               <tr>
                   <td>Последний пользователь</td>
                   <td><input name="last_user" value="{{$statistic->last_user}}"></td>
               </tr>
               <tr>
                   <td>Последний вклад</td>
                   <td><input name="last_input" value="{{$statistic->last_input}}"></td>
               </tr>
               <tr>
                   <td>Последняя выплата</td>
                   <td><input name="last_output" value="{{$statistic->last_output}}"></td>
               </tr>
               <tr>
                   <td colspan="2"><button type="submit" class="btn btn-success" href="#">Update</button></td>
               </tr>
            </tbody>
        </table>
    </div>
    </form>
@endsection