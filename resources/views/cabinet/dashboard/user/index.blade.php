@extends('layouts.account')

@section('content')
    <div class="tpl_wrap withdraw clearfix">
        <table class="table table-hover table-dark">
            <thead>
            <tr>
                <th scope="col">№</th>
                <th scope="col">Email</th>
                <th scope="col">Skype</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @if (isset($users))
                @foreach($users as $key => $user)
                    <tr>
                        <td scope="row">{{++$key}}</td>
                        <td><a href="{{route('change_all', ['user' => $user->id])}}">{{$user->email}}</a></td>
                        <td>{{$user->skype}}</td>
                        <td><a href="{{route('change.status', ['user' => $user->id])}}" class="btn btn-{{$user->status==STATUS_ACTIVE ? 'danger' : 'success'}}">{{$user->status==STATUS_ACTIVE ? 'Деактивировать' : 'Возобновить'}}</a></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@endsection