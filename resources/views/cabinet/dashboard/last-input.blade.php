@extends('layouts.account')

@section('content')
    <form method="post" action="{{route('update.statistics.last.input')}}">
        @csrf
        <div class="tpl_wrap withdraw clearfix">
            <h3 style="float: left"><strong> Последние инвестиции:</strong></h3>
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">User</th>
                    <th scope="col">Amount</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input name="user_1" value="{{$last_input->user_1}}"></td>
                    <td><input name="amount_1" value="{{$last_input->amount_1}}"></td>
                </tr>
                <tr>
                    <td><input name="user_2" value="{{$last_input->user_2}}"></td>
                    <td><input name="amount_2" value="{{$last_input->amount_2}}"></td>
                </tr>
                <tr>
                    <td><input name="user_3" value="{{$last_input->user_3}}"></td>
                    <td><input name="amount_3" value="{{$last_input->amount_3}}"></td>
                </tr>
                <tr>
                    <td><input name="user_4" value="{{$last_input->user_4}}"></td>
                    <td><input name="amount_4" value="{{$last_input->amount_4}}"></td>
                </tr>
                <tr>
                    <td><input name="user_5" value="{{$last_input->user_5}}"></td>
                    <td><input name="amount_5" value="{{$last_input->amount_5}}"></td>
                </tr>
                <tr>
                    <td><input name="user_6" value="{{$last_input->user_6}}"></td>
                    <td><input name="amount_6" value="{{$last_input->amount_6}}"></td>
                </tr>
                <tr>
                    <td><input name="user_7" value="{{$last_input->user_7}}"></td>
                    <td><input name="amount_7" value="{{$last_input->amount_7}}"></td>
                </tr>
                <tr>
                    <td><input name="user_8" value="{{$last_input->user_8}}"></td>
                    <td><input name="amount_8" value="{{$last_input->amount_8}}"></td>
                </tr>
                <tr>
                    <td><input name="user_9" value="{{$last_input->user_9}}"></td>
                    <td><input name="amount_9" value="{{$last_input->amount_9}}"></td>
                </tr>
                <tr>
                    <td><input name="user_10" value="{{$last_input->user_10}}"></td>
                    <td><input name="amount_10" value="{{$last_input->amount_10}}"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-success" href="#">Update</button></td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
@endsection