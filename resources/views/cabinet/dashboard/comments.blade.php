@extends('layouts.account')

@section('content')
    <div class="tpl_wrap withdraw clearfix">
        <h3 style="float: left"><strong>Новые отзывы</strong></h3>
        <table class="table table-hover table-dark">
            <thead>
            <tr>
                <th scope="col">Автор</th>
                <th scope="col">Текст</th>
                <th scope="col" colspan="2">Действия</th>
            </tr>
            </thead>
            <tbody>
            @if (isset($comments))
                @foreach($comments as $key => $comment)
                    <tr>
                        <td scope="row">{{++$key}}</td>
                        <td>{{$comment->author->name}}</td>
                        <td>{{$comment->text}}</td>
                        <td><a href="{{route('approve.comment', ['comment' => $comment->id])}}" class="btn btn-warning">Одобрить</a></td>
                        <td><a href="{{route('delete.comment', ['comment' => $comment->id])}}" class="btn btn-danger">Удалить</a></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@endsection