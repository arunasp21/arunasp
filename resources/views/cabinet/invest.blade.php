@extends('layouts.account')

@section('content')

        <div class="col-lg-9"> 
          <!-- All Transactions
          ============================================= -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <form action="#" method="post">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">@lang('my-text.key_355'): {{number_format(auth()->user()->detail->amount, 2, '.', '')}} $</h3>
			<hr>
            <!-- Title
            =============================== -->

                    <div class="col-12 d-flex mb-3">
					<div class="col-3 btn-group-toggle" onclick="SystemFail()">
                      <label class="btn btn-outline-secondary btn-sm shadow-none w-100" id="pay2">
                        <input type="radio" name="system" value="Payeer" id="pay_sys_">
						<svg width="150" height="60"
                                                                         id="bm_icon_payment_colored_payeer"
                                                                         viewBox="-297 420.9 170 60" width="100%"
                                                                         height="100%">		<path fill="#414141"
                                                                                                     d="M-259.5 439.6c-1.7-1-3.8-1.8-6.4-1.8h-9.4v28.7h5.6v-9.2h3.8c2.5-.1 5.3-.8 6.8-2.2 4.5-4.4 3.7-13.1-.4-15.5zm-3.1 9.1c-.4 2.6-1.7 3.6-3.5 3.6h-3.5v-9.8c3.8-.5 6.4.3 7.1 3.1v3.1h-.1zM-242.5 437.8h-5.4l-9.6 28.7h5.5l2.1-5.9h9.3l2.3 5.9h5.6l-9.8-28.7zm-5.9 17.8l3.2-9 3.2 9h-6.4zM-226.1 466.5h5.6l-.1-11 8.3-17.7h-6.2l-4.8 10.4-4.7-10.4h-6.2l8.1 17.7z"></path>		<path
                                                                                fill="#3B97D3"
                                                                                d="M-208.9 437.8h15.7v4.6h-10.1v6.9h8.7v5.1h-8.7v7.6h11.5v4.5h-17.1zM-188.3 437.8h15.8v4.5h-10.2v7h8.6v5.1h-8.6v7.7h11.4v4.4h-17zM-154.2 454.2c1.4-.6 2.5-1.2 3.3-2.1 4.5-5 1.8-11.4-1.4-13.1-1.3-.7-2.5-1.2-4.2-1.2h-11.2v28.7h5.3v-11.6h2.3l6.4 11.6h6.4l-6.9-12.3zm-2-4.4c-.6.3-.9.5-1.6.5h-4.5v-7.8h4.1c.7.2 1.8.4 2.3.9 1.9 1.7 1.3 5.4-.3 6.4z"></path>	</svg>
                        </label>
   				    </div>	
					<div class="col-3 btn-group-toggle" onclick="setSystem(1)">
                      <label class="btn btn-outline-secondary btn-sm shadow-none w-100" id="pay1">
                        <input type="radio" name="system" value="PerfectMoney" id="pay_sys_">
																							<svg width="150" height="60"
                                                                         id="bm_icon_payment_colored_perfectmoney"
                                                                         viewBox="-297 420.9 170 60" width="100%"
                                                                         height="100%">		<path fill="#EE2A27"
                                                                                                     d="M-134.6 443.5c-1.5 1.9-4.2 2.6-6.5 1.6s-3.5-3.5-3-6c.5-2.3 2.6-4.1 5-4.2 2.5-.1 4.7 1.5 5.4 3.9.4 2.5-.4 4-.9 4.7zm-.2-1c-.7-.6-.7-3.4-.1-4.1-.8 0-.8 0-1.2.7-.4.8-.8 1.6-1.3 2.5-.5-1-.9-1.7-1.2-2.6-.3-.8-.3-.8-1.2-.6.6.5.5 3.3-.2 4h1.2c-.6-.5-.6-1.4-.2-2.9.5 1 .9 1.9 1.4 3 .5-1 1-1.9 1.5-2.8 0 .9.5 1.9-.2 2.7.5.1.9.1 1.5.1zm-8.1-4.1c.6.3.5.8.5 1.2v.5c0 .2-.1.4.2.4s.3-.2.3-.4v-.8c0-.2-.2-.6.2-.6.3-.1.7 0 .9.3.4.6.2 1.4-.5 1.6-.5.1-.9.1-1.6.2.8.1 1.4.1 2-.1s1-.6.9-1.2c-.1-.6-.5-1-1.1-1.1h-1.8zm1.5 4.2c-.6-.2-.5-.7-.5-1.1 0-.2.1-.4-.2-.5-.3 0-.3.2-.3.4 0 .4-.1.8-.7 1.1h.6c.3.1.7.1 1.1.1z"></path>		<path
                                                                                fill="#191717"
                                                                                d="M-192.6 467.1c0-1 0-1.9-.1-2.9 0-.9.1-1.8-.1-2.7 0-.2-.2-.6.2-.6.3 0 .7-.3.8.3.1.3.2.1.3 0 .7-.5 1.5-.6 2.3-.1.8.6.9 1.5.8 2.4-.2.9-.7 1.5-1.6 1.7-.6.1-1-.1-1.6-.4v1.5c0 .3-.1.5-.3.7-.2.1-.5.1-.7.1zm3-4c0-.9-.5-1.5-1.2-1.3-.6.1-1 .8-.8 1.7.1.6.4.9 1 1 .6 0 1-.6 1-1.4z"></path>		<path
                                                                                fill="#EE2A27"
                                                                                d="M-210.3 441.4c-.4 3.6-.5 7.2-.6 10.9 0 .3 0 .6.1.8.2 1.1.4 1.2 1.4 1.3h.4c.3 0 .4.1.4.4 0 .3-.3.4-.5.3-.8-.1-1.7-.1-2.5-.1s-1.5 0-2.3.1c-.3 0-.5.1-.6-.3s.2-.4.5-.4c1.5-.2 1.6-.4 1.8-1.8.4-4.1.7-8.3.8-12.5 0-1.2-.2-1.4-1.4-1.7l-.6-.1s-.3-.1-.3-.3c0-.2.1-.3.3-.3h3.8c.2 0 .3.1.3.3.1 1.4.7 2.6 1.3 3.8 1.5 3.2 3.1 6.3 4.6 9.4.1.1.1.2.2.4.4-.5.7-1 .9-1.6 1.6-3.2 3.2-6.5 4.7-9.7.3-.7.7-1.4.8-2.2.1-.3.2-.4.5-.4h3.5c.2 0 .5 0 .5.3 0 .4-.2.3-.5.4l-.8.1c-.7.1-1.1.5-1.2 1.2-.2 1.7-.1 3.5-.1 5.2 0 2.5.2 5 .3 7.5.1 1.6.2 1.8 1.8 2 .3 0 .5.1.5.4 0 .3-.2.4-.5.4-1.8-.2-3.7-.2-5.5 0-.3 0-.6 0-.6-.4s.3-.3.6-.3c1.4-.1 1.6-.3 1.6-1.7 0-3.5 0-7.1-.1-10.6 0-.2.1-.4-.1-.6-.3.3-.5.7-.6 1.1-1.9 4.1-4.2 8.1-6 12.3-.1.2-.1.5-.4.5-.2 0-.3-.2-.3-.4-1.5-3.7-3.3-7.2-4.9-10.8-.4-1.1-.8-2-1.2-2.9zM-170.2 450.3v2.9c0 .8.3 1.1 1.1 1.2.3 0 .6 0 .6.4s-.4.4-.7.3c-1.3-.1-2.6-.1-3.9 0-.3 0-.6.1-.7-.3-.1-.4.3-.4.5-.4.7-.1 1-.5 1-1.2v-5.2c0-.9-.3-1.8-1.3-2.1-1.1-.4-2.2-.3-3.1.5-.4.4-.6.9-.6 1.4v5.4c0 .7.3 1.2 1.1 1.3.3 0 .5.1.4.5-.1.4-.3.3-.6.3-1.3-.1-2.7-.1-4 0-.3 0-.5.1-.6-.3-.1-.4.2-.4.5-.5.8 0 1.1-.5 1.1-1.2v-6.2c0-.5-.3-.8-.7-1.1-.5-.3-.4-.5.1-.7l2.1-1c.4-.2.5-.1.5.3.1 1.2.1 1.2 1.3.7.7-.3 1.4-.6 2.1-.7 2.1-.3 3.6.9 3.6 3 .2.7.2 1.7.2 2.7zM-288.9 447.2h5.5c1 0 1.9-.1 2.8-.3 2.1-.5 3.2-2.3 3-4.6-.2-2.3-1.4-3.6-3.6-3.8-1.9-.2-1.9-.2-1.9 1.7v5.8c0 .5-.1.6-.6.6-1.6 0-1.6 0-1.6-1.5 0-1.7 0-3.4-.1-5.1-.1-1.2-.3-1.4-1.5-1.5h-.2c-.3 0-.5 0-.5-.4s.3-.4.5-.4c2-.2 4-.2 6-.1 1.2 0 2.4.2 3.5.8 1.6.8 2.4 2.2 2.5 4 .1 1.7-.5 3.1-2 4-2.4 1.5-5.1 1.6-7.9 1.5-1.3-.1-2.6-.3-3.9-.7zM-192.2 449.9c0-3.3 2.5-5.7 5.7-5.7 3.1.1 5.3 2.4 5.3 5.5 0 3.4-2.4 5.7-5.6 5.7-3.2 0-5.4-2.3-5.4-5.5zm8.7-.3c0-1-.3-2.4-1.4-3.6-1.2-1.3-3.1-1.2-4.1.3-.5.7-.8 1.5-.8 2.3-.2 1.9 0 3.7 1.4 5.1 1.2 1.3 3.2 1 4.1-.5.6-.9.8-1.9.8-3.6zM-253.8 449.8v3.2c0 1.1.2 1.3 1.3 1.4.3 0 .8-.2.7.4 0 .6-.5.3-.8.3-1.4-.1-2.9-.1-4.3 0-.3 0-.5.1-.6-.3 0-.4.2-.4.5-.4 1-.2 1.2-.3 1.2-1.3v-6.8c0-.4-.2-.5-.5-.5h-.5c-.2 0-.4.1-.5-.2 0-.3.1-.4.3-.4 1.1-.3 1.2-1 1.3-2 .1-1.9.5-3.7 1.8-5.1 1.3-1.5 3-2 4.9-1.6.5.1 1 .4 1.2.8.3.5.3.9-.2 1.3-.4.4-.9.3-1.3 0-.2-.2-.3-.4-.5-.5-1.3-1.1-2.8-.7-3.4.9-.3.9-.5 1.9-.5 2.9v2.3c0 .5.2.7.7.6h2.3c.3 0 .6 0 .6.5 0 .4-.1.7-.6.7h-2.3c-.5 0-.7.2-.6.7-.2.9-.2 2-.2 3.1zM-152.3 452.5c1.1-2.1 1.9-4.1 2.7-6.2.2-.5 0-.8-.5-.9l-.7-.1s-.3-.2-.3-.4.1-.3.4-.3c1.3.1 2.7.1 4 0 .2 0 .3.1.4.3 0 .2-.1.4-.3.4-1.1.1-1.6 1-2 1.9-2.2 4.2-4.1 8.6-6.2 12.9-.6 1.2-1.2 1.6-2.1 1.4-.6-.1-1-.5-1-1.1 0-.6.4-1.1 1-1.1 1 .1 1.4-.5 1.9-1.2 1.3-1.9 1.6-3.9.5-6-.8-1.6-1.3-3.3-1.9-5-.3-.9-.7-1.6-1.8-1.7-.3 0-.4-.2-.3-.5 0-.3.2-.3.5-.3 1.4.1 2.8.1 4.2 0 .2 0 .4 0 .5.3 0 .3-.1.4-.4.5-1 .3-1.1.3-.7 1.4.7 1.8 1.2 3.7 2.1 5.7zM-270.4 448.3h-2.8c-.3 0-.6 0-.6.4-.1 2.3 1 4.8 3.8 5.1 1 .1 1.9-.2 2.6-.9.2-.2.3-.6.7-.3.3.3 0 .5-.1.7-1.2 1.6-2.8 2.2-4.8 1.9s-3.2-1.4-3.7-3.3c-1-3.3.7-6.7 3.7-7.6 1.7-.5 3.2-.2 4.4 1.2.2.3.4.6.5.9.5 1.3.1 1.8-1.3 1.8-.8.1-1.6.1-2.4.1zm-1.1-.8l2.1-.1c.8 0 .8-.5.7-1-.1-.4-.3-.7-.7-.9-1.3-.7-3.5 0-4 1.5-.2.4-.1.6.4.6.5-.1 1-.1 1.5-.1zM-162.9 448.3h-2.8c-.3 0-.6 0-.6.5-.1 2.3 1.2 4.8 3.9 5.1 1 .1 1.9-.2 2.6-1 .2-.2.3-.5.6-.2.4.3.1.5-.1.7-1.2 1.7-2.9 2.3-5 1.9-2-.4-3.2-1.6-3.7-3.6-.8-3.2.8-6.4 3.7-7.2 1.9-.6 3.8 0 4.7 1.5.1.2.3.5.4.8.3 1.2.1 1.5-1.2 1.5h-2.5zm-1.1-.8c.7 0 1.4 0 2.1-.1.8-.1.9-.4.7-1.1-.1-.5-.5-.7-.9-.9-1.3-.6-3.3.3-3.8 1.6-.1.4 0 .5.4.5h1.5zM-245 448.3h-2.6c-.8-.1-1 .3-.9 1 .1 1.2.4 2.4 1.3 3.4 1.2 1.3 4 1.9 5.2.2.1-.2.3-.4.6-.1.2.2.1.4-.1.6-1.5 2.5-5.7 2.9-7.6.5-1.5-1.8-1.5-4-.8-6.1.7-2 2.2-3.1 4.3-3.4 1.5-.2 2.7.2 3.7 1.4.3.3.4.7.5 1.1.3 1.2.1 1.5-1.2 1.5-.8-.1-1.6-.1-2.4-.1zm-1.2-.8c.7 0 1.4 0 2.1-.1.8-.1 1-.4.7-1.2-.1-.3-.3-.5-.6-.7-1.4-.8-3.7 0-4.2 1.5-.1.3 0 .5.3.4.6.1 1.1.1 1.7.1zM-240.6 450.2c.1-5.2 5.2-6.7 8.5-5.5.6.2.9 1.4.4 1.9-.4.4-.7.1-1-.2-1.3-1.3-2.8-1.5-4.2-.6-1.4.8-2.1 2.7-1.7 4.5.5 2.5 2.6 4 5 3.5.8-.1 1.3-.6 1.8-1.1.2-.2.3-.5.6-.3s.1.5 0 .7c-1 1.6-2.6 2.4-4.5 2.3-3.1-.2-4.9-2.2-4.9-5.2zM-262.4 445.8c.6-.4 1.1-.8 1.6-1.1s1-.5 1.6-.4c.4.1.8.3.9.8.1.5 0 .9-.4 1.2-.4.3-.8.4-1.3.1-.2-.1-.5-.3-.8-.4-.7-.2-1.1 0-1.4.7-.2.4-.2.9-.2 1.4v4.8c0 1.2.2 1.4 1.4 1.5.2 0 .5-.1.6.2.1.4 0 .6-.5.5-1.6-.1-3.2-.2-4.7 0-.3 0-.4-.1-.4-.3-.1-.3.1-.4.4-.4 1.1-.3 1.2-.3 1.2-1.5v-5.7c0-.6-.1-1-.6-1.3-.5-.3-.6-.6 0-.8l2.1-1c.3-.2.5-.1.5.3v1.4zM-226.9 449.3c0 1-.1 2 0 3 .2 1.7 1.3 2.3 2.9 1.6.2-.1.3-.3.5 0s0 .4-.2.6c-.9 1-3.2 1.3-4.3.6-.8-.5-1-1.3-1-2.1v-6.1c0-.7-.1-1.2-.9-1-.2 0-.4 0-.5-.3 0-.2.1-.4.3-.4 1.1-.3 1.8-1 2.3-2 .1-.2.2-.3.5-.3.3.1.3.3.3.5.1 1.3.1 1.3 1.3 1.3h1.8c.4 0 .5.2.5.5s0 .6-.5.6h-2.5c-.6 0-.7.2-.7.7.1 1 .1 1.9.2 2.8zM-283.1 450.9v1.5c0 1.5.4 1.9 1.9 2 .3 0 .7-.1.7.4s-.4.4-.7.3c-1.9-.1-3.8-.1-5.7 0-.3 0-.5.1-.6-.3s.2-.4.5-.5c1.3-.2 1.5-.4 1.6-1.7.1-1.2.1-2.4.1-3.7 0-.4.1-.5.5-.5 1.6.1 1.6.1 1.6 1.7.1.4.1.6.1.8z"></path>		<path
                                                                                fill="#191717"
                                                                                d="M-205.2 463.2v1.5c0 .6-.5.9-.9.5-.3-.4-.5-.1-.7 0-1.1.5-2 .1-2.3-1.1-.2-.9 0-1.8-.1-2.7 0-.5.4-.3.6-.4.3 0 .6-.1.5.4v2.1c0 .5.1 1 .8 1 .6 0 .8-.4.8-1v-2c0-.6.3-.5.6-.5s.6 0 .6.4l.1 1.8zM-171.8 463.5h-1c-.6 0-.5.3-.2.6.4.5 1 .6 1.6.5.4-.1.8-.3.8.3.1.6-.4.4-.7.5-.9.2-1.9.1-2.5-.6-.6-.7-.6-1.6-.3-2.5.4-1 1.4-1.5 2.4-1.3.9.2 1.5 1.1 1.4 2.2 0 .3-.1.4-.5.4-.3-.2-.6-.1-1-.1zm-.5-.8c.2-.2.8.3.8-.3 0-.4-.3-.7-.8-.7s-.9.3-.9.7c.1.5.6.1.9.3zM-184.5 463.5h-.9c-.2 0-.4-.1-.5.1-.1.2.1.5.2.6.5.4 1.1.4 1.7.3.2-.1.5-.2.6.3.1.4-.1.5-.4.6-1.6.4-2.8-.1-3.2-1.4-.5-1.7.6-3.2 2.3-3.1 1.3.1 1.8 1.3 1.7 2.4 0 .2-.2.2-.3.2h-1.2zm-.5-.8c.2-.3.8.3.8-.3 0-.4-.4-.7-.8-.7-.5 0-.8.3-.8.7 0 .5.5.1.8.3zM-177.4 463.6v-1.2c0-.3.1-.5-.4-.6-.4-.1-.3-.7 0-.8.6-.1.4-.5.5-.8.2-.9 1-1.5 1.9-1.3.3 0 .5.1.5.5 0 .3-.1.5-.5.4-.4-.1-.6.2-.7.6-.1.3-.1.6.4.6.3 0 .5 0 .5.4s-.2.5-.5.4c-.4-.1-.4.2-.4.5v2.5c0 .5-.3.5-.6.4-.3 0-.7.1-.7-.5v-1.1zM-197.7 459.9c0 .6-.1 1.2.8 1 .3 0 .2.3.2.5 0 .3-.1.4-.3.4-.8-.1-.7.3-.7.9v1.1s.1.7.6.7c.3 0 .4.1.4.4 0 .3 0 .5-.3.6-.9.3-1.7-.2-1.8-1.2v-1.5c0-.4.3-1-.5-1-.2 0-.1-.3-.1-.5s0-.4.3-.4c.2 0 .3 0 .4-.2.1-.7.1-.7 1-.8zM-162.7 459.9v.6c0 .3.1.5.4.4.3 0 .6-.1.6.4s-.3.5-.6.4c-.4 0-.5.1-.5.5v1.4c0 .4.1.8.6.7.3 0 .4.1.3.4 0 .2.1.5-.3.6-.8.3-1.7-.2-1.8-1.1-.1-.6 0-1.2 0-1.8 0-.3.1-.7-.4-.7-.3 0-.2-.5-.2-.6.2-.3.7-.3.6-.8.2-.2.5-.3 1.3-.4zM-201.8 460.9c1 0 1.2.2 1 .8-.1.2-.1.3-.4.2-.2-.1-.4-.1-.7-.1-.2 0-.4.1-.5.3-.1.3.1.4.3.5l.9.4c.5.3.7.7.7 1.3s-.4.9-.9 1.1c-.6.2-1.1.2-1.7 0-.3-.1-.5-.2-.4-.6.1-.5.4-.3.6-.3l.4.1c.3 0 .7.1.8-.4.1-.4-.3-.5-.6-.6-.6-.3-1.3-.5-1.3-1.3 0-.9.7-1.4 1.8-1.4zM-210.9 461.8c-.1.7.1 1.6-.1 2.4-.3 1-1.6 1.5-2.6 1.2-.4-.2-.2-.4-.2-.6 0-.3.1-.3.4-.3 1.2-.1 1.3-.2 1.3-1.4v-3.1c0-.5.2-.5.6-.6.4 0 .6.1.6.5-.1.5 0 1.2 0 1.9zM-166.6 460.8c.3.2 1-.2 1 .6 0 .3-.1.5-.5.4-.6-.2-1.1 0-1.5.5-.3.5-.4 1.1 0 1.6.3.5.8.7 1.4.5.3-.1.6-.1.7.4.1.4-.2.5-.5.6-1 .2-1.9.1-2.5-.7-.6-.8-.6-1.6-.2-2.5.3-1 1.1-1.3 2.1-1.4zM-181.6 463.1v-1.6c0-.2-.1-.5.2-.5.2 0 .6-.1.7.1.2.5.4.2.6 0 .3-.2.6-.3.9-.2.2.1.1.5.1.7 0 .3-.1.3-.3.3-.6.1-.9.5-.9 1.1v1.8c0 .6-.4.4-.7.4-.3 0-.6 0-.6-.4.1-.5 0-1.1 0-1.7z"></path>	</svg>
                        </label>
					 </div>
					<div class="col-3 btn-group-toggle" onclick="SystemFail()">
                      <label class="btn btn-outline-secondary btn-sm shadow-none w-100" id="pay3">
                        <input type="radio" name="system" value="" id="">
							<img width="150px" height="60px" src="{{ asset('components/img/advcash.png') }}">						
						</label>
					 </div>
					<div class="col-3 btn-group-toggle" onclick="SystemFail()">
                      <label class="btn btn-outline-secondary btn-sm shadow-none w-100">
                        <input type="radio" name="system" value="" id="">
							<img width="150px" height="60px" src="{{ asset('components/img/qiwi.png') }}">
                        </label>
				   </div>
                    </div>
                    <div class="col-12 d-flex">
						<div class="col-4">
							<div class="input-group">
							  <div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
							  <input type="text" name="amount" id="_amount" placeholder="{{ __('my-text.key_356') }}" class="form-control" required>
							</div>
						</div>
						<div class="col-8">
							<input type="hidden" name="system" id="system" value="">
							<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

							<button type="button" class="btn btn-primary mb-1" onclick="runFormPayment()">{{__('my-text.key_357')}}</button>
						</div>
					</div>
					<div id="pay_block" style="display: none"></div>
            </form>
          </div>
          <!-- All Transactions End --> 
        </div> 

    <script>

        function setSystem(id) {
            document.getElementById('system').value = id;

            if (id == 3) {
				document.getElementById('pay1').style.backgroundColor = "#FFFFFF";
				document.getElementById('pay2').style.backgroundColor = "#FFFFFF";
				document.getElementById('pay3').style.backgroundColor = "#6c757d";
            }
            if (id == 1) {
				document.getElementById('pay1').style.backgroundColor = "#6c757d";
				document.getElementById('pay2').style.backgroundColor = "#FFFFFF";
				document.getElementById('pay3').style.backgroundColor = "#FFFFFF";
            }
            if (id == 2) {
				document.getElementById('pay1').style.backgroundColor = "#FFFFFF";
				document.getElementById('pay2').style.backgroundColor = "#6c757d";
				document.getElementById('pay3').style.backgroundColor = "#FFFFFF";
            }
        }

        function runFormPayment() {
            let id = document.getElementById('system').value;
			
			if(Number(id) < 1)
				alert('Выберите платежную систему!');
			
            if (id == 3) {
                pay_adv();
            }
            if (id == 1) {
                pay_perfect_money();
            }
            if (id == 2) {
                pay_payeer();
            }
        }

		function SystemFail()
		{
			Swal.fire(
			  '',
			  'Данная платежная система временно недоступна. Воспользуйтесь пожалуыста другой. Приносим извинения за временные неудобства.',
			  'info'
			)			
		}

        function pay_payeer() {

            let amount, token;
            amount=document.getElementById('_amount').value;
            token=document.getElementById('_token').value;

            if (amount < 1){
                alert('Некорректно введена сумма.');
            }else{
                $.ajax({
                    url: '{{action('PaySystemController@PayeerInit')}}',
                    method: 'POST',
                    data: {
                        _token: token,
                        amount: amount
                    },
                    success: function (data) {

                        console.log(data);

                        document.getElementById('pay_block').style.display='none';

                        $('#pay_block').html('');
                        $('#pay_block').append(function () {
                            var html;
                            html =
                                '<form id="pay_payeer" method="post" action="https://payeer.com/merchant/">' +
                                '<input type="hidden" name="m_shop" value="' + data[0] + '">' +
                                '<input type="hidden" name="m_orderid" value="' + data[1] + '">' +
                                '<input type="text" readonly name="m_amount" value="' + data[2] + '">' +
                                '<input type="text" readonly name="m_curr" value="' + data[3] + '">' +
                                '<input type="hidden" name="m_desc" value="' + data[4] + '">' +
                                '<input type="hidden" name="m_sign" value="' + data[5] + '">' +
                                '<a class="style_11" onclick="document.getElementById(\'pay_payeer\').submit()">PAY</a>' +
                                '</form>';
                            return html;
                        });
                        document.forms["pay_payeer"].submit();
                    },
                    statusCode: {
                        200: function (data_2) {
                            console.log(data_2);
                        },
                        500: function (data_2) {
                            console.log(data_2);
                        }
                    }
                })
            }
        }

        function pay_perfect_money() {

            let amount, token;
            amount=document.getElementById('_amount').value;
            token=document.getElementById('_token').value;

            if (amount < 1){
                alert('Некорректно введена сумма.');
            }else{
                $.ajax({
                    url: '{{action('PaySystemController@Perfect_MoneyInit')}}',
                    method: 'POST',
                    data: {
                        _token: token,
                        amount: amount,
                    },
                    success: function (data) {
						
                        $('#pay_block').html('');
                        $('#pay_block').append(function () {
                            var html;
                            html =
                                '<input type="text" name="cur" readonly value="'+data[12]+'">'+
                                '<form id="pay_perfect" action="https://perfectmoney.is/api/step1.asp" method="POST">'+
                                '<input type="hidden" name="PAYEE_ACCOUNT" value="'+data[0]+'">'+
                                '<input type="hidden" readonly name="PAYEE_NAME" value="'+data[1]+'">'+
                                '<input type="hidden" name="PAYMENT_ID" value="'+data[2]+'"><BR>'+
                                '<input type="text" readonly name="PAYMENT_AMOUNT" value="'+data[3]+'"><BR>'+
                                '<input type="hidden" name="PAYMENT_UNITS" value="'+data[4]+'">'+
                                '<input type="hidden" name="STATUS_URL" value="'+data[5]+'">'+
                                '<input type="hidden" name="PAYMENT_URL" value="'+data[6]+'">'+
                                '<input type="hidden" name="PAYMENT_URL_METHOD" value="'+data[7]+'">'+
                                '<input type="hidden" name="NOPAYMENT_URL" value="'+data[8]+'">'+
                                '<input type="hidden" name="NOPAYMENT_URL_METHOD" value="'+data[9]+'">'+
                                '<input type="hidden" name="SUGGESTED_MEMO" value="'+data[10]+'">'+
                                '<input type="hidden" name="BAGGAGE_FIELDS" value="'+data[11]+'">'+
                                '<a class="style_11" onclick="document.getElementById(\'pay_perfect\').submit()">{{__('my-text.key_358')}}</a>'+
                                '</form>';
                            return html;
                        });

                        document.forms["pay_perfect"].submit();
                    }
                })
            }
        }

        function pay_adv() {

            let amount, token;
            amount=document.getElementById('_amount').value;
            token=document.getElementById('_token').value;

            if (amount < 1){
               alert('Некорректно введена сумма.');
            }else{
                $.ajax({
                    url: '{{action('PaySystemController@AdvCashInit')}}',
                    method: 'POST',
                    data: {
                        _token: token,
                        amount: amount,
                    },
                    success: function (data) {

                        $('#pay_block').html('');
                        $('#pay_block').append(function () {
                            var html;
                            html =
                                '<form id="pay_adv" action="https://wallet.advcash.com/sci/" method="POST">'+
                                '<input type="hidden" name="ac_account_email" value="'+data[0]+'">'+
                                '<input type="hidden" readonly name="ac_sci_name" value="'+data[1]+'">'+
                                '<input type="text" readonly name="ac_amount" value="'+data[2]+'"><BR>'+
                                '<input type="text" readonly  name="ac_currency" value="'+data[3]+'"><BR>'+
                                '<input type="hidden" name="ac_order_id" value="'+data[4]+'">'+
                                '<input type="hidden" name="ac_sign" value="'+data[5]+'">'+
                                '<input type="hidden" name="ac_success_url" value="'+data[6]+'">'+
                                '<input type="hidden" name="ac_success_url_method" value="'+data[7]+'">'+
                                '<input type="hidden" name="ac_fail_url" value="'+data[8]+'">'+
                                '<input type="hidden" name="ac_fail_url_method" value="'+data[9]+'">'+
                                '<input type="hidden" name="ac_status_url" value="'+data[10]+'">'+
                                '<input type="hidden" name="ac_status_url_method" value="'+data[11]+'">'+
                                '<a class="style_11" onclick="document.getElementById(\'pay_adv\').submit()">{{__('my-text.key_358')}}</a>'+
                                '</form>';
                            return html;
                        });

                        document.forms["pay_adv"].submit();
                    }
                })
            }
        }
    </script>

@endsection
