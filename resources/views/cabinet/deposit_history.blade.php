@extends('layouts.account')

@section('content')

        <div class="col-lg-9"> 
          <!-- All Transactions
          ============================================= -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">@lang('my-text.key_54')</h3>
            <!-- Title
            =============================== -->
            <div class="transaction-title py-2 px-4">
              <div class="row">
                <div class="col col-sm-5">@lang('my-text.key_373')</div>
                <div class="col-2 col-sm-3">@lang('my-text.key_374')</div>
                <div class="col-auto col-sm-2 d-none d-sm-block">@lang('my-text.key_375')</div>
                <div class="col-3 col-sm-2">@lang('my-text.key_347')</div>
              </div>
            </div>
            <!-- Title End --> 
            
            <!-- Transaction List
            =============================== -->
            @foreach($deposits as $deposit)
            <div class="transaction-list">
              <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail">
                <div class="row align-items-center flex-row">
                  <div class="col col-sm-5"> <span class="text-nowrap">{{\Carbon\Carbon::parse($deposit->from_date)->format('m.d.Y H:i')}}</span></div>
                  <div class="col-2 col-sm-3"> <span class="text-nowrap">{{$deposit->amount}}</span></div>
				  @if($deposit->custom == 1)
					<div class="col-auto col-sm-2 d-none d-sm-block text-3"><span class="text-nowrap">{$deposit->profit}}</span></div>
				  @else
					<div class="col-auto col-sm-2 d-none d-sm-block text-3"><span class="text-nowrap">{{$deposit->amount * ($deposit->plan->percent * $deposit->plan->day)/100}}</span></div>
			      @endif
                  <div class="col-3 col-sm-2 text-4"><span class="text-nowrap">{{$deposit->status==1 ? __('my-text.key_376') : __('my-text.key_377')}}</span></div>
                </div>
              </div>
            </div>
            @endforeach
            <!-- Transaction List End --> 
            
            <!-- Pagination
            ============================================= 
            <ul class="pagination justify-content-center mt-4 mb-0">
              <li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1"><i class="fas fa-angle-left"></i></a> </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item active"> <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a> </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item d-flex align-content-center flex-wrap text-muted text-5 mx-1">......</li>
              <li class="page-item"><a class="page-link" href="#">15</a></li>
              <li class="page-item"> <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a> </li>
            </ul>
            Paginations end --> 
            
          </div>
          <!-- All Transactions End --> 
        </div> 

@endsection