@extends('layouts.account')

@section('content')

	<!-- Middle Panel
	============================================= -->
	<div class="col-lg-9"> 
	  
	  <!-- Personal Details
	  ============================================= -->
	  <div class="bg-white shadow-sm rounded p-4 mb-4">
		<h3 class="text-5 font-weight-400 d-flex align-items-center mb-4">@lang('my-text.key_333')</h3>
		<hr class="mx-n4 mb-4">
		<div class="btn-link text-3">{{auth()->user()->detail->referral_url}}</div>
	  </div>
	  <!-- Personal Details End --> 
	  
	  <!-- Recent Activity
	  =============================== -->
	  <div class="bg-white shadow-sm rounded p-4 mb-4">
		
		<!-- Transaction List
		=============================== -->
		<div class="transaction-list">
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_334'):</span></div>
				@if(!is_null($invited_it))
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{$invited_it->nickname}}</span></div>
				@else
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">-</span></div>
				@endif
			</div>
		  </div>
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_335'):</span></div>
				<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{$ref_cnt}}</span></div>
			</div>
		  </div>
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_336'):</span></div>
				<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{$ref_active_cnt}}</span></div>
			</div>
		  </div>
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_337'):</span></div>
				<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{ $total_ref }}</span></div>
			</div>
		  </div>
		</div>
		<!-- Transaction List End --> 

	  </div>
	  <!-- Recent Activity End --> 

	  <div class="bg-white shadow-sm rounded mb-4">
		<!-- Transaction List
		=============================== -->
		<div class="transaction-list">
		  <div class="transaction-item px-4 py-3">
			<h3 class="text-5 font-weight-400 d-flex align-items-center mb-4">@lang('my-text.key_342')</h3>
			<hr class="mx-n4">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_338')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_339')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.deposit')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_341')</span></div>
			</div>
		  </div>
		@foreach($first_ref as $ref)
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->nickname }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->created_at }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->depo }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->amount }}</span></div>
			</div>
		  </div>
		@endforeach
		</div>
		<!-- Transaction List End --> 
	  </div>
	  <div class="bg-white shadow-sm rounded mb-4">
		<!-- Transaction List
		=============================== -->
		<div class="transaction-list">
		  <div class="transaction-item px-4 py-3">
			<h3 class="text-5 font-weight-400 d-flex align-items-center mb-4">@lang('my-text.key_343')</h3>
			<hr class="mx-n4">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_338')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_339')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.deposit')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_341')</span></div>
			</div>
		  </div>
		@foreach($second_ref as $ref)
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->nickname }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->created_at }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->depo }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->amount }}</span></div>
			</div>
		  </div>
		@endforeach
		</div>
		<!-- Transaction List End --> 
	  </div>
	  <div class="bg-white shadow-sm rounded mb-4">
		<!-- Transaction List
		=============================== -->
		<div class="transaction-list">
		  <div class="transaction-item px-4 py-3">
			<h3 class="text-5 font-weight-400 d-flex align-items-center mb-4">@lang('my-text.key_344')</h3>
			<hr class="mx-n4">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_338')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_339')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.deposit')</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">@lang('my-text.key_341')</span></div>
			</div>
		  </div>
		@foreach($third_ref as $ref)
		  <div class="transaction-item px-4 py-3">
			<div class="row align-items-center flex-row">
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->nickname }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->created_at }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->depo }}</span></div>
				<div class="col col-sm-3"> <span class="d-block text-3">{{ $ref->amount }}</span></div>
			</div>
		  </div>
		@endforeach
		</div>
		<!-- Transaction List End --> 
	  </div>


	</div>
	<!-- Middle Panel End --> 

@endsection