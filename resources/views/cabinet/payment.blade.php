@extends('layouts.account')

@section('content')

        <div class="col-lg-9"> 
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">@lang('my-text.key_355'): {{number_format(auth()->user()->detail->amount, 2, '.', '')}} $</h3>
			<hr>
            <!-- Title
            =============================== -->
				<form method="POST">
                    @csrf
                    <div class="col-12 d-flex mb-4">
						<div class="col-6">
							<div class="input-group">
								<select name="payment_system" id="payment_system" class="custom-select">
									<option value="{{null}}">@lang('my-text.key_350')</option>
									@foreach($paymentSystem as $id => $system)
										<option value="{{$id}}">{{$system}}</option>
									@endforeach
								</select>
							</div>
							<span id="error_ps" style="color: red;"></span>
						</div>
						<div class="col-6">
							<div class="input-group">
								<input type="text" name="wallet" id="wallet" placeholder="{{__('my-text.key_352')}}" value="" class="form-control">
							</div>
							<span id="error_w" style="color: red;"></span>
						</div>
					</div>
                    <div class="col-12 d-flex">
						<div class="col-6">
							<div class="input-group">
								<div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
								<input type="text" name="amount" id="amount" placeholder="{{__('my-text.key_353')}}" class="form-control">
							</div>
							<span id="error_amount" style="color: red;"></span>
						</div>
						<div class="col-6">
							<input type="hidden" name="payment_type" id="payment_type" value="2">
							<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
							<button type="button" onclick="Withdrawal();" class="btn btn-primary mb-1">{{__('my-text.key_354')}}</button>
						</div>
					</div>
				</form>
          </div>
        </div> 



    <div class="tpl_wrap withdraw clearfix">
        <form action="{{route('payment.output')}}" method="post">
            <input type="hidden" name="payment_type" value="2">
            @csrf
            <div class="with_green clearfix">
                <table>
                    <tbody>
                    <tr>
                        <td class="cell first_w_cell">@lang('my-text.key_349'):</td>
                        <td colspan="2" class="blue">&nbsp;{{number_format(auth()->user()->detail->amount, 2, '.', '')}}
                            <i class="fa fa-usd"></i></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="col-md-8"></div>
            <div class="balance_section"></div>
            <div class="account_balance_section">
                <div class="deposit">
                    <ul class="choose-pay-system clearfix" data-step="6"
                        data-intro="&lt;h4&gt;{{__('my-text.key_299')}}&lt;/h4&gt;&lt;p&gt;{{__('my-text.key_351')}}&lt;/p&gt;"
                        data-position="top">
                    </ul>
                </div>
            </div>
            <br><br>
            <div class="deposit">
                <div class="amount clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <select name="payment_system" class="form-control">
                                <option value="{{null}}">@lang('my-text.key_350')</option>
                                @foreach($paymentSystem as $id => $system)
                                    <option value="{{$id}}">{{$system}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" value="" name="wallet" placeholder="{{__('my-text.key_352')}}" class="inpts s14">
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" value="" name="amount" placeholder="{{__('my-text.key_353')}}" class="inpts s14">
                        </div>
                        <div class="col-md-6">
                            <input type="submit" placeholder="{{__('my-text.key_354')}}" value="{{__('my-text.key_354')}}" class="sbmt" data-step="8">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection