@extends('layouts.account')

@section('content')

    <div class="tpl_wrap clearfix edition_area">

        <h3>Личная информация пользователя</h3>

        <div class="col-md-4">
        </div>
        <table cellspacing="0" cellpadding="2" border="0" class="accounts">
            <tbody>
				<tr>
					<td class="username_info">@lang('my-text.key_331'):
						<span disabled="">{{$user_data->name}}</span>
					</td>
				</tr>
				<tr>
					<td class="username_info">@lang('my-text.key_77')
						<span disabled="">{{$user_data->email}}</span>
					</td>
				</tr>
            </tbody>
        </table>
                    <a href="#deposit"><input type="submit" value="История вкладов" class="sbmt"></a>
                    <a href="#top_up"><input type="submit" value="История пополнений" class="sbmt"></a>
                    <a href="#ref"><input type="submit" value="Партнеры" class="sbmt"></a>
                    <a href="#add_ref"><input type="submit" value="Добавить реферала" class="sbmt"></a>
                    <a href="#edit_profile"><input type="submit" value="Редактирование профиля" class="sbmt"></a>

					<div id="deposit" class="modalDialog">
						<div>
							<div id="new"><a href="#close" title="Закрыть" class="close">X</a></div>
							<h2>Добавить вклад</h2>
							<hr>
							<form>
							<p><input class="form-control" type="text" name="dt" id="dt" placeholder="Дата" style="width: 250px;"></p>
							<p><input type="text" name="amount" id="amount" placeholder="Сумма" class="form-control" style="width: 250px;" required=""></p>
							<p><input type="text" name="profit" id="profit" placeholder="Прибыль" class="form-control" style="width: 250px;" required=""></p>
							<p><select class="form-control" style="width: 250px;" name="status" id="status">
								<option>Статус</option>
								<option value="0">Завершен</option>
								<option value="1">Активен</option>
							</select></p>
							<p><select class="form-control" style="width: 250px;" name="plan" id="plan">
								<option>Выберите план</option>
								<option value="1">Silver</option>
								<option value="2">Gold</option>
								<option value="3">Platinum</option>
								<option value="4">Brilliant</option>
							</select></p>
							<input type="hidden" value="{{$user_data->id}}" name="uid" id="uid">
							<input type="hidden" id="_token" value="{{ csrf_token() }}">
							<p><input type="button" onclick="AddDeposit();" value="Добавить" class="sbmt"></p>
							</form>
						</div>
					</div>

					<div id="top_up" class="modalDialog">
						<div>
							<div id="new"><a href="#close" title="Закрыть" class="close">X</a></div>
							<h2>Добавить пополнение</h2>
							<hr>
							<form>
							<p><input type="text" name="amount_top" id="amount_top" placeholder="Сумма" class="form-control" style="width: 250px;" required=""></p>
							<p><input class="form-control" type="text" name="dt_top" id="dt_top" placeholder="Дата" style="width: 250px;"></p>
							<p><select class="form-control" style="width: 250px;" name="system_top" id="system_top">
								<option>Платежная система</option>
								<option value="1">Perfect Money</option>
								<option value="2">Payeer</option>
								<option value="3">Qiwi</option>
								<option value="4">Visa/Mastercard</option>
								<option value="5">BitCoin</option>
							</select></p>
							<p><select class="form-control" style="width: 250px;" name="status_top" id="status_top">
								<option>Статус</option>
								<option value="1">Expected</option>
								<option value="2">Complete</option>
								<option value="3">Denied</option>
							</select></p>
							<input type="hidden" value="{{$user_data->id}}" name="uid_top" id="uid_top">
							<input type="hidden" id="_token_top" value="{{ csrf_token() }}">
							<p><input type="button" onclick="AddTop();" value="Добавить" class="sbmt"></p>
							</form>
						</div>
					</div>

					<div id="ref" class="modalDialog">
						<div>
							<div id="new"><a href="#close" title="Закрыть" class="close">X</a></div>
							<h2>Редактировать</h2>
							<hr>
							<form>
							<p><span>Вас пригласил</span><br /><input type="text" name="call" id="call" value="{{ $user_data->invited }}" class="form-control" style="width: 250px;"></p>
							<p><span>Количество рефералов</span><br /><input type="text" name="count_ref" id="count_ref" value="{{ $user_data->count_ref }}" class="form-control" style="width: 250px;"></p>
							<p><span>Активных рефералов</span><br /><input type="text" name="active_ref" id="active_ref" value="{{ $user_data->active_ref }}" class="form-control" style="width: 250px;"></p>
							<p><span>Получено с рефералов</span><br /><input type="text" name="amount_ref" id="amount_ref" value="{{ $user_data->amount_ref }}" class="form-control" style="width: 250px;"></p>
							<input type="hidden" value="{{$user_data->id}}" name="uid_other" id="uid_other">
							<input type="hidden" id="_token_other" value="{{ csrf_token() }}">
							<p><input type="button" onclick="AddRefOther();" value="Добавить" class="sbmt"></p>
							</form>
						</div>
					</div>

					<div id="add_ref" class="modalDialog">
						<div>
							<div id="new"><a href="#close" title="Закрыть" class="close">X</a></div>
							<h2>Добавить реферала</h2>
							<hr>
							<form>
							<p><input type="text" name="login" id="login" placeholder="Логин" class="form-control" style="width: 250px;" required=""></p>
							<p><input class="form-control" type="text" name="dt_ref" id="dt_ref" placeholder="Дата" style="width: 250px;"></p>
							<p><input type="text" name="depo" id="depo" placeholder="Пополнено" class="form-control" style="width: 250px;" required=""></p>
							<p><input type="text" name="referals" id="referals" placeholder="Реферальные" class="form-control" style="width: 250px;" required=""></p>
							<p><select class="form-control" style="width: 250px;" name="level" id="level">
								<option>Уровень</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select></p>
							<input type="hidden" value="{{$user_data->id}}" name="uid_ref" id="uid_ref">
							<input type="hidden" id="_token_ref" value="{{ csrf_token() }}">
							<p><input type="button" onclick="AddRef();" value="Добавить" class="sbmt"></p>
							</form>
						</div>
					</div>

					<div id="edit_profile" class="modalDialog">
						<div>
							<div id="new"><a href="#close" title="Закрыть" class="close">X</a></div>
							<h2>Редактирование профиля</h2>
							<hr>
							<form>
							<p><input type="text" name="balance" id="balance" placeholder="Баланс" class="form-control" style="width: 250px;" required></p>
							<p><input type="text" name="last_depo" id="last_depo" placeholder="Последний вклад" class="form-control" style="width: 250px;" required></p>
							<p><input type="text" name="total" id="total" placeholder="Итого" class="form-control" style="width: 250px;" required></p>
							<p><input type="text" name="ext_amount" id="ext_amount" placeholder="Средства в рассмотрении" class="form-control" style="width: 250px;" required></p>
							<p><input type="text" name="withdrawals" id="withdrawals" placeholder="Всего выведенных средств" class="form-control" style="width: 250px;" required></p>
							<p><input type="text" name="active_depo" id="active_depo" placeholder="Активные вклады" class="form-control" style="width: 250px;" required></p>
							<p><input type="text" name="total_depo" id="total_depo" placeholder="Всего вкладов" class="form-control" style="width: 250px;" required></p>
							<input type="hidden" value="{{$user_data->id}}" name="uid_profile" id="uid_profile">
							<input type="hidden" id="_token_profile" value="{{ csrf_token() }}">
							<p><input type="button" onclick="EditProfile();" value="Добавить" class="sbmt"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></p>
							</form>
						</div>
					</div>

				
    </div>

@endsection
