@extends('layouts.account')

@section('content')

        <!-- Middle Panel
        ============================================= -->
        <div class="col-lg-9"> 
          
          <!-- Personal Details
          ============================================= -->
          <div class="bg-white shadow-sm rounded p-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center mb-4">@lang('my-text.your_profile')</h3>
            <hr class="mx-n4 mb-4">
            <div class="form-row align-items-center">
              <p class="col-sm-3 text-muted text-sm-right mb-0 mb-sm-3">@lang('my-text.key_361'):</p>
              <p class="col-sm-9 text-3">{{ auth()->user()->email }}</p>
            </div>
            <div class="form-row align-items-center">
              <p class="col-sm-3 text-muted text-sm-right mb-0 mb-sm-3">@lang('my-text.key_362'):</p>
              <p class="col-sm-9 text-3">{{\Carbon\Carbon::parse(auth()->user()->created_at)->format('d.m.Y H:i:s')}}</p>
            </div>
            @if(!is_null(auth()->user()->last_login))
				<div class="form-row align-items-center">
				  <p class="col-sm-3 text-muted text-sm-right mb-0 mb-sm-3">@lang('my-text.key_363'):</p>
				  <p class="col-sm-9 text-3">{{\Carbon\Carbon::parse(auth()->user()->last_login)->format('d.m.Y H:i:s')}}</p>
				</div>
            @endif
          </div>
          <!-- Personal Details End --> 
          
          <!-- Recent Activity
          =============================== -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">@lang('my-text.info_panel')</h3>
            <hr style="margin-bottom: 0;">
            
            <!-- Transaction List
            =============================== -->
            <div class="transaction-list">
              <div class="transaction-item px-4 py-3">
                <div class="row align-items-center flex-row">
					<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_366'):</span></div>
					@if(auth()->user()->custom_profile == 1)
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format(auth()->user()->total, 2, '.', '')}} $</span></div>
					@else
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">0.00 $</span></div>
					@endif
                </div>
              </div>
              <div class="transaction-item px-4 py-3">
                <div class="row align-items-center flex-row">
					<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_367'):</span></div>
					@if(auth()->user()->custom_profile == 1)
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format(auth()->user()->ext_amount, 2, '.', '')}} $</span></div>
					@else
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format($allAmountOutputExpected, 2, '.', '')}} $</span></div>
					@endif
                </div>
              </div>
              <div class="transaction-item px-4 py-3">
                <div class="row align-items-center flex-row">
					<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_368'):</span></div>
					@if(auth()->user()->custom_profile == 1)
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format(auth()->user()->withdrawals, 2, '.', '')}} $</span></div>
					@else
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format($allAmountOutputComplete, 2, '.', '')}} $</span></div>
					@endif
                </div>
              </div>
              <div class="transaction-item px-4 py-3">
                <div class="row align-items-center flex-row">
					<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_369'):</span></div>
					@if(auth()->user()->custom_profile == 1)
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format(auth()->user()->active_depo, 2, '.', '')}} $</span></div>
					@else
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format($activeDepositAmount, 2, '.', '')}} $</span></div>
					@endif
                </div>
              </div>
              <div class="transaction-item px-4 py-3">
                <div class="row align-items-center flex-row">
					<div class="col col-sm-9"> <span class="d-block text-3">@lang('my-text.key_370'):</span></div>
					@if(auth()->user()->custom_profile == 1)
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format(auth()->user()->total_depo, 2, '.', '')}} $</span></div>
					@else
					<div class="col-3 col-sm-2 text-right text-4"> <span class="d-block text-4">{{number_format($allDepositAmount, 2, '.', '')}} $</span></div>
					@endif
                </div>
              </div>
            </div>
            <!-- Transaction List End --> 
   
            <!-- View all Link
            =============================== -->
            <div class="text-center mt-4">@lang('my-text.key_371'): <div class="btn-link text-3">{{auth()->user()->detail->referral_url}}</div></div>
            <hr style="margin-bottom: 1.4rem;">
            <div class="text-center text-4">@lang('my-text.key_372'): <span>{{\Carbon\Carbon::now()->format('d.m.Y')}}</span>&nbsp;<span id="time">{{date('H:i:s')}}</span></div>
            <!-- View all Link End --> 
            
          </div>
          <!-- Recent Activity End --> 
        </div>
        <!-- Middle Panel End --> 

@endsection

<script>

	function update() {

	  var unixtimestamp = parseInt($('#unixtimestamp').html());
	  var date = new Date(unixtimestamp*1000);

	  var hours = date.getHours();
	  if (hours < 10) hours = '0' + hours;

	  var minutes = date.getMinutes();
	  if (minutes < 10) minutes = '0' + minutes;

	  var seconds = date.getSeconds();
	  if (seconds < 10) seconds = '0' + seconds;
	  $('#time').html(hours+':'+minutes+':'+seconds);
	  unixtimestamp++;
	  $('#unixtimestamp').html(unixtimestamp);

	}

	setInterval(update, 1000);

</script>

	<span style="display:none" id="unixtimestamp">{{time()}}</span>
