@extends('layouts.account')

@section('content')

        <div class="col-lg-9"> 
          <!-- All Transactions
          ============================================= -->
          <h3 class="text-6 font-weight-400 d-flex justify-content-center px-4 mb-4"><strong>@lang('my-text.key_388')</strong></h3>
			
			  <!-- Style 5
			  ============================================= -->
			  <div class="row">
			  @foreach(getPlans() as $plan)
				<div class="col-sm-6 col-lg-3 mb-4">
				  <div class="featured-box style-5 rounded">
					<div class="featured-box-icon text-info" style="font-size: 40px; margin: 30px 0px; color: #0062cc;"><strong>{{$plan->title}}</strong></div>
					<div class="featured-box-icon" style="font-size: 30px; margin: 20px 0px; color: #1e1d1c;"><strong>{{$plan->percent}}%</strong></div>
					<div class="text-center mt-4">
						<h3>{{__('my-text.daily')}} - {{$plan->day}} {{__('my-text.days')}}</h3><br>
						<h3>{{__('my-text.minimum')}}: {{$plan->from_amount}}$</h3><br>
						<h3>{{__('my-text.maximum')}}: {{$plan->to_amount}}$</h3><br>
					</div>
				  </div> 
				</div>
			  @endforeach
			  </div>
			  <!-- Style 5 end --> 
			
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">@lang('my-text.key_355'): {{number_format(auth()->user()->detail->amount, 2, '.', '')}} $</h3>
			<hr>
            <!-- Title
            =============================== -->
				<form method="POST">
                    @csrf
                    <div class="col-12 d-flex">
						<div class="col-4">
							<div class="input-group">
								<select class="custom-select" name="depo_plan" id="depo_plan">
								  <option value selected>{{__('my-text.select_plan')}}</option>
								  @foreach(getPlans() as $key => $plan)
								  <div>
									<option value="{{++$key}}">
										{{$plan->title}}
									</option>
								  </div>
								  @endforeach
								</select>                    
							</div>
							<span id="error_plan" style="color: red;"></span>
						</div>
						<div class="col-4">
							<div class="input-group">
								<div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
								<input type="number" name="depo_amount" id="depo_amount" placeholder="{{__('my-text.sum_depo')}}" class="form-control">
							</div>
							<span id="error_amount" style="color: red;"></span>
						</div>
						<div class="col-4">
							<input type="hidden" name="system" id="system" value="">
							<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

							<button type="button" onclick="AddDeposit();" class="btn btn-primary mb-1">{{__('my-text.btn_depo')}}</button>
						</div>
					</div>
				</form>
          </div>
          <!-- All Transactions End --> 
        </div> 

@endsection