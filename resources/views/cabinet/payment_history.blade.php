@extends('layouts.account')

@section('content')

        <div class="col-lg-9"> 
          <!-- All Transactions
          ============================================= -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">@lang('my-text.key_56')</h3>
            <!-- Title
            =============================== -->
            <div class="transaction-title py-2 px-4">
              <div class="row">
                <div class="col col-sm-3">@lang('my-text.key_339')</div>
                <div class="col-2 col-sm-2">@lang('my-text.key_348')</div>
                <div class="col-auto col-sm-3 d-none d-sm-block">@lang('my-text.key_345')</div>
                <div class="col-3 col-sm-2">@lang('my-text.key_346')</div>
                <div class="col-3 col-sm-2">@lang('my-text.key_347')</div>
              </div>
            </div>
            <!-- Title End --> 
            
            <!-- Transaction List
            =============================== -->
            @foreach($payments as $payment)
            <div class="transaction-list">
              <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail">
                <div class="row align-items-center flex-row">
                  <div class="col col-sm-3"> <span class="text-nowrap">{{!is_null($payment->completed_at) ? \Carbon\Carbon::parse($payment->completed_at)->format('m.d.Y H:i') : \Carbon\Carbon::parse($payment->created_at)->format('m.d.Y H:i')}}</span></div>
                  <div class="col-2 col-sm-2"> <span class="text-nowrap">{{number_format($payment->amount, 2, '.', '')}}</span></div>
				  <div class="col-auto col-sm-3 d-none d-sm-block text-3"><span class="text-nowrap">{{\App\Entity\Payment::$paymentSystem[$payment->payment_system]}}</span></div>
                  <div class="col-3 col-sm-2 text-2"><span class="text-nowrap">{{$payment->wallet}}</span></div>
                  <div class="col-3 col-sm-2 text-2"><span class="text-nowrap">{{\App\Entity\Payment::$paymentStatus[$payment->status]}}</span></div>
                </div>
              </div>
            </div>
            @endforeach
            <!-- Transaction List End --> 
            
            <!-- Pagination
            ============================================= 
            <ul class="pagination justify-content-center mt-4 mb-0">
              <li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1"><i class="fas fa-angle-left"></i></a> </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item active"> <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a> </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item d-flex align-content-center flex-wrap text-muted text-5 mx-1">......</li>
              <li class="page-item"><a class="page-link" href="#">15</a></li>
              <li class="page-item"> <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a> </li>
            </ul>
            Paginations end --> 
            
          </div>
          <!-- All Transactions End --> 
        </div> 

@endsection