  <!-- Secondary Menu
  ============================================= -->
  <div class="bg-primary">
    <div class="container d-flex justify-content-center">
      <ul class="nav secondary-nav">
        <li class="nav-item"> <a class="nav-link {{ Request::segment(2) == 'profile' ? 'active' : '' }}" href="{{route('account')}}">@lang('my-text.key_53')</a></li>
        <li class="nav-item"> <a class="nav-link {{ Request::segment(2) == 'deposit-history' ? 'active' : '' }}" href="{{route('deposit.list')}}">@lang('my-text.key_54')</a></li>
        <li class="nav-item"> <a class="nav-link {{ Request::segment(2) == 'invest-history' ? 'active' : '' }}" href="{{route('invest.list')}}">@lang('my-text.key_55')</a></li>
        <li class="nav-item"> <a class="nav-link {{ Request::segment(2) == 'payment-history' ? 'active' : '' }}" href="{{route('payment.list')}}">@lang('my-text.key_56')</a></li>
        <li class="nav-item"> <a class="nav-link {{ Request::segment(2) == 'referral' ? 'active' : '' }}" href="{{route('referral')}}">@lang('my-text.key_57')</a></li>
      </ul>
    </div>
  </div>
  <!-- Secondary Menu end --> 
