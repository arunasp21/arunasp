<div class="left_menu" style="height: 500px">
    <h3 style="color: darkred; font-weight: 600">@lang('my-text.key_69')</h3>
    <ul class="left_navigation_menu">
        <li class="menu-item">
            <a href="{{route('plans.index')}}">
                <i class="fa" aria-hidden="true"></i>
                <span>@lang('my-text.key_70')</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{route('moderation.comments')}}">
                <i class="fa" aria-hidden="true"></i>
                <span>@lang('my-text.key_71')</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{route('users')}}">
                <i class="fa" aria-hidden="true"></i>
                <span>@lang('my-text.key_72')</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{route('statistics.admin')}}">
                <i class="fa" aria-hidden="true"></i>
                <span>@lang('my-text.key_73')</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{route('statistics.last.input')}}">
                <i class="fa" aria-hidden="true"></i>
                <span>@lang('my-text.key_74')</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{route('statistics.last.output')}}">
                <i class="fa" aria-hidden="true"></i>
                <span>@lang('my-text.key_75')</span>
            </a>
        </li>
    </ul>
</div>