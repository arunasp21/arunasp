        <!-- Left Panel
        ============================================= -->
        <aside class="col-lg-3"> 
          
          <!-- Profile Details
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="profile-thumb mt-3 mb-4"> <img class="rounded-circle" width="100" height="100" src="{{ asset('components/images/def-avatar.png') }}" alt="">
<!--
              <div class="profile-thumb-edit custom-file bg-primary text-white" data-toggle="tooltip" title="Change Profile Picture"> 
				<i class="fas fa-camera position-absolute"></i>
				<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="{{route('upload.avatar')}}" >
					{{ csrf_field() }}
					<input type="file" class="custom-file-input" name="new_avatar" id="new_avatar">
				</form>
              </div>
-->			  
            </div>
            <p class="text-3 font-weight-500 mb-2">@lang('my-text.hi'), {{ auth()->user()->name }}</p>
<!--
            <p class="mb-2"><a href="profile.html" class="text-5 text-light" data-toggle="tooltip" title="Edit Profile"><i class="fas fa-edit"></i></a></p>
-->
          </div>
          <!-- Profile Details End --> 
          
          <!-- Available Balance
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="text-17 text-light my-3"><i class="fas fa-wallet"></i></div>
			@if(auth()->user()->custom_profile == 1)
				<h3 class="text-9 font-weight-400">${{ number_format(auth()->user()->detail->amount, 2, '.', '') . ' ' }}</h3>
			@else
				<h3 class="text-9 font-weight-400">${{ number_format(auth()->user()->detail->amount, 2, '.', '') . ' ' }}</h3>
			@endif
            <p class="mb-2 text-muted opacity-8">@lang('my-text.key_364')</p>
          </div>
          <!-- Available Balance End --> 
          <div class="col-12 mb-4">
			  <div class="row">
				  <button type="button" class="col-lg-12 btn btn-primary btn-sm mb-1" onclick="window.location.href='{{route('invest')}}'">@lang('my-text.key_50')</button>
				  <button type="button" class="col-lg-12 btn btn-primary btn-sm mb-1" onclick="window.location.href='{{route('deposit')}}'">@lang('my-text.deposit')</button>
				  <button type="button" class="col-lg-12 btn btn-primary btn-sm" onclick="window.location.href='{{route('payment')}}'">@lang('my-text.key_52')</button>
			  </div>
		  </div>
		  
          <!-- Need Help?
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="text-17 text-light my-3"><i class="fas fa-comments"></i></div>
            <h3 class="text-5 font-weight-400 my-4">@lang('my-text.help')</h3>
            <p class="text-muted opacity-8 mb-4">@lang('my-text.help_text')</p>
            <a href="{{route('support')}}#support" class="btn btn-primary btn-block">@lang('my-text.help_btn')</a> </div>
          <!-- Need Help? End --> 
          
        </aside>
        <!-- Left Panel End --> 
