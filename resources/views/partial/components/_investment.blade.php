<div class="investment {{($_SERVER['REQUEST_URI']=='/' || $_SERVER['REQUEST_URI']=='/ru' || $_SERVER['REQUEST_URI']=='/de' || $_SERVER['REQUEST_URI']=='/en') ? '' : 'investment_inner'}}" id="invest">
    <div class="container">
        <input type="hidden" value="{{getJsonPlans()}}" name="js_plans" id="js_plans">
        <h3>@lang('my-text.key_42')</h3>
        <div class="calculator" data-step="10" data-intro="<h4>{{__('my-text.key_16')}}</h4><p>
                            {{__('my-text.key_17')}}</p>"
             data-highlightclass="menu-highlight" data-position="bottom">
            <div class="input_box" data-step="11"
                 data-intro="<h4>{{__('my-text.key_18')}}</h4><p>{{__('my-text.key_19')}}</p>"
                 data-highlightclass="menu-highlight" data-position="bottom">
                <label for="plan">@lang('my-text.key_28')</label>
                <div class="select_box">
                    <select name="plan" id="js-plan" class="form-control">
                        <option value selected>@lang('my-text.key_29')</option>
                        @foreach(getPlans() as $key => $plan)
                        <div>
                            <option value="{{++$key}}">
                               {{$plan->title}}
                            </option>
                        </div>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input_box" data-step="12"
                 data-intro="<h4>{{__('my-text.key_20')}}</h4><p>{{__('my-text.key_21')}}</p>"
                 data-highlightclass="menu-highlight" data-position="bottom">
                <label for="amount">@lang('my-text.key_30')</label>
                <input type="number" id="js-amount" name="amount">
            </div>
            <div class="input_box" data-step="13" data-intro="<h4>{{__('my-text.key_22')}}</h4><p>
                            {{__('my-text.key_23')}}</p>"
                 data-highlightclass="menu-highlight" data-position="bottom">
                <label>@lang('my-text.key_31') </label>
                <div id="js-profit" class="count_box"></div>
            </div>
            <div class="input_box" data-step="14" data-intro="<h4>{{__('my-text.key_24')}}</h4><p>
                        {{__('my-text.key_25')}}</p>"
                 data-highlightclass="menu-highlight" data-position="bottom">
                <label>@lang('my-text.key_32')</label>
                <div id="js-total" class="count_box"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="list_carousel" data-step="9" data-intro="<h4>{{__('my-text.key_26')}}</h4><p>{{__('my-text.key_27')}}</p>" data-highlightclass="menu-highlight"
             data-tooltipclass="centered-tooltip" data-position="top">
            <ul id="foo2">
                @foreach(getPlans() as $plan)
                    <li class="invest_item forDate" id="{{$plan->title}}" data-name="{{$plan->title}}" data-percent="{{$plan->percent}}">
                    <div class="bottle_tittle">
                        {{--<div class="marker_choice marker_green">--}}
                            {{--<span class="popular_marker">Popular</span>--}}
                        {{--</div>--}}
                        <span style="font-weight: 900">{{$plan->title}}</span>
                    </div>
                    <div class="percent_number">{{$plan->percent}}%</div>
                        <div class="days_info my_sett">@lang('my-text.key_33') <span class="forDays">{{$plan->day}} @lang('my-text.key_34') </span></div>
                    <div class="general_info" style="width: 230px;">
                        <div>
                            <b style="font-weight: 900">@lang('my-text.key_35')</b>
                            <span>{{$plan->from_amount}}</span>
                        </div>
                        <div>
                            <b style="font-weight: 900">@lang('my-text.key_36')</b>
                            <span>{{$plan->to_amount}}</span>
                        </div>
                        <div>
                            <b style="font-weight: 900">@lang('my-text.key_37') <span>@lang('my-text.key_38')</span></b>
                        </div>
                        <div>
                            <b style="font-weight: 900">@lang('my-text.key_39') <span>@lang('my-text.key_40')</span></b>
                        </div>
                    </div>
                    <a href="{{route('deposit')}}" class="deposit_button">@lang('my-text.key_41')</a>
                </li>
                @endforeach
            </ul>
            <div class="clearfix"></div>
            <a class="nav_btn prev" href="#"></a>
            <a class="nav_btn next" href="#"></a>
        </div>
    </div>
</div>
