<div style="background-color: #ffffff;background-image: url('components/images/ng22header2.jpg');background-position: top center;background-repeat: no-repeat;background-attachment: scroll;">
	<div class="container">
		<div style="padding-top: 5px;padding-left: 0px;padding-bottom: 1px;padding-right: 0px;">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-12" style="text-align: center;">
						<span class="new-header-notice" style="color: white;  text-align: center;">
							<span style="font-size: 15px;color: #ffffff;">
								<a href="{{route('news')}}" class="header-notice-button" style="color: #000000;font-weight: 400; text-transform: uppercase;"><h6><span style="color: #fc2403;">НОВОГОДНЯЯ АКЦИЯ!</span><p style="color: #0320fc;">ПРИВЕТСТВЕННЫЙ БОНУС 10$ ЗА РЕГИСТРАЦИЮ!</p></h6></a>
							</span>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>  
<!-- Header ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start"> 
          <!-- Logo
          ============================= -->
          <div class="logo"> <a class="d-flex" href="/" title="ArunaSP"><img src="{{ asset('components/images/logo1.png') }}" width="140" height="60"  alt="ArunaSP" /></a> </div>
          <!-- Logo end --> 
          <!-- Collapse Button
          ============================== -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
          <!-- Collapse Button end --> 
          
          <!-- Primary Navigation
          ============================== -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav mr-auto">
                <li class="{{ Request::segment(1) == '' ? 'active' : '' }}"><a href="{{route('welcome')}}">@lang('my-text.head_menu_1')</a></li>
                <li style="width: 67px;" class="{{ Request::segment(1) == 'about' ? 'active' : '' }}"><a href="{{route('about')}}">@lang('my-text.head_menu_2')</a></li>
                <li style="width: 120px;" class="{{ Request::segment(1) == 'started' ? 'active' : '' }}"><a href="{{route('started')}}">@lang('my-text.head_menu_6')</a></li>
                <li class="{{ Request::segment(1) == 'faq' ? 'active' : '' }}"><a href="{{route('faq')}}">@lang('my-text.head_menu_7')</a></li>
                <li class="{{ Request::segment(1) == 'rules' ? 'active' : '' }}"><a href="{{route('rules')}}">@lang('my-text.head_menu_8')</a></li>
                <li class="{{ Request::segment(1) == 'reviews' ? 'active' : '' }}"><a href="{{route('reviews')}}">@lang('my-text.head_menu_9')</a></li>
                <li class="{{ Request::segment(1) == 'reviews' ? 'active' : '' }}"><a href="{{route('news')}}">@lang('my-text.head_menu_5')</a></li>
                <li class="{{ Request::segment(1) == 'support' ? 'active' : '' }}"><a href="{{route('support')}}">@lang('my-text.head_menu_10')</a></li>
              </ul>
            </div>
          </nav>
          <!-- Primary Navigation end --> 
        </div>
        <div class="header-column justify-content-end"> 
          <!-- Login & Signup Link
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              @auth
				  <li><a href="{{route('account')}}">@lang('my-text.account')</a> </li>
				  <li class="align-items-center h-auto ml-sm-3">
					<a class="btn btn-primary" href="#" onclick="event.preventDefault();
																	document.getElementById('logout-form').submit();">@lang('my-text.exit')</a>
				  </li>
                  <form id="logout-form" action="{{ LaravelLocalization::localizeUrl('logout') }}" method="POST" style="display: none;">
					@csrf
                  </form>&nbsp;&nbsp;&nbsp;&nbsp;
				  <li class="dropdown language"> <a class="dropdown-toggle" href="#">{{ str_replace('_', '-', app()->getLocale()) }}</a>
					<ul class="dropdown-menu">
					  <li><a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a></li>
					  <li><a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a></li>
					</ul>
				  </li>
				  <li></li>
              @else
				  <li><a href="{{route('register')}}">@lang('my-text.registration')</a> </li>
				  <li class="align-items-center h-auto ml-sm-3">
					<a class="btn btn-primary" href="{{route('login')}}">@lang('my-text.login')</a>
				  </li>&nbsp;&nbsp;&nbsp;&nbsp;
				  <li class="dropdown language"> <a class="dropdown-toggle" href="#">{{ str_replace('_', '-', app()->getLocale()) }}</a>
					<ul class="dropdown-menu">
					  <li><a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a></li>
					  <li><a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a></li>
					</ul>
				  </li>
				  <li></li>
              @endauth
            </ul>
          </nav>
          <!-- Login & Signup Link end --> 
        </div>
      </div>
    </div>
  </header>
  <!-- Header End --> 
