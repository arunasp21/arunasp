<div class="header_small header">
    <div class="container pills">
        <div class="row">
            <div class="col-xs-12 logo_row">

                <div class="col-xs-2 logo">
                    <a href="{{ LaravelLocalization::localizeUrl('/') }}">
                        <img style="margin-left: 3px" src="{{ asset('components/img/cs_logo.gif') }}" alt="logo">
                    </a>

                </div>

                <div class="col-xs-5 adress pull-right">
                    <div class="language_select" style="margin-left: 30px;">
                        <div class="header-language pull-left">
                            <div style="position: absolute; top: 26px;">
                                <a style="" href="{{ LaravelLocalization::getLocalizedURL('ru') }}" class="flag flag-ru"></a>
                                <a style="" href="{{ LaravelLocalization::getLocalizedURL('en') }}" class="flag flag-gb"></a>
                                {{--<a style="" href="{{env('APP_URL') . '/de'}}" class="flag flag-de"></a>--}}
                            </div>
<!--
							<div style="position: absolute;display: flex;  top: 29px;" class="language_flags">
                                <a style="margin-left: 70px" href="#" class="flag flag-ru"></a>
                                {{--<a--}}
                                {{--style="" href="https://razleton.com/support/?lng=en" class="flag flag-us"></a><a--}}
                                {{--style="" href="https://razleton.com/support/?lng=de" class="flag flag-de"></a>--}}
                            </div>
-->							
                        </div>
                    </div>

                    <a href="{{route('account')}}" class="account_btn">
                        <svg id="add_user" viewBox="0 0 328.5 328.5" width="100%" height="100%">
                            <path d="M96.3,150.9v-15H55.7V95.3h-15v40.7H0v15h40.7v40.7h15v-40.7H96.3z M259.4,185.9H145.9c-38.1,0-69.1,31-69.1,69.1V295h251.8  v-39.9C328.5,216.9,297.5,185.9,259.4,185.9L259.4,185.9z M313.5,280H91.7v-24.9c0-29.8,24.3-54.1,54.1-54.1h113.5  c29.8,0,54.1,24.3,54.1,54.1L313.5,280L313.5,280z M202.6,178.8c40.1,0,72.7-32.6,72.7-72.7s-32.6-72.7-72.7-72.7  S130,66.1,130,106.2S162.6,178.8,202.6,178.8z M202.6,48.5c31.8,0,57.7,25.9,57.7,57.7s-25.9,57.7-57.7,57.7  c-31.8,0-57.7-25.9-57.7-57.7S170.8,48.5,202.6,48.5L202.6,48.5z"></path>
                        </svg>
                        @lang('my-text.account')
                    </a>
                    <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                       class="login_btn">
                        <svg id="exit" viewBox="0 0 512 512" width="100%" height="100%">
                            <path d="M1.6,226.5c1.1-2.6,2.6-5,4.6-7l64-64c8.3-8.3,21.8-8.3,30.2,0c8.3,8.3,8.3,21.8,0,30.2l-27.6,27.6H192  c11.8,0,21.3,9.6,21.3,21.3S203.8,256,192,256H72.8l27.6,27.6c8.3,8.3,8.3,21.8,0,30.2c-4.2,4.2-9.6,6.3-15.1,6.3  s-10.9-2.1-15.1-6.3l-64-64c-2-2-3.5-4.3-4.6-7C-0.5,237.6-0.5,231.7,1.6,226.5z M149.3,298.7c11.8,0,21.3,9.6,21.3,21.3v106.7H256  V85.3c0-9.4,6.2-17.7,15.2-20.4l74.1-22.2H170.7v106.7c0,11.8-9.5,21.3-21.3,21.3s-21.3-9.6-21.3-21.3v-128C128,9.6,137.5,0,149.3,0  h341.3c0.8,0,1.5,0.3,2.2,0.4c1,0.1,1.9,0.3,2.9,0.5c2.2,0.6,4.3,1.5,6.2,2.6c0.5,0.3,1,0.3,1.5,0.7l0.4,0.5  c2.3,1.8,4.3,4.1,5.7,6.7c0.3,0.6,0.4,1.2,0.6,1.8c0.7,1.6,1.4,3.2,1.7,5c0.1,0.6-0.1,1.2-0.1,1.9c0,0.4,0.3,0.8,0.3,1.2V448  c0,10.2-7.2,18.9-17.2,20.9l-213.3,42.7c-1.4,0.3-2.8,0.4-4.2,0.4c-4.9,0-9.7-1.7-13.5-4.8c-4.9-4.1-7.8-10.1-7.8-16.5v-21.3H149.3  c-11.8,0-21.3-9.6-21.3-21.3V320C128,308.2,137.5,298.7,149.3,298.7L149.3,298.7z"></path>
                        </svg>
                        @lang('my-text.exit')
                    </a>
                    <form id="logout-form" action="{{ LaravelLocalization::localizeUrl('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>


                <div class="col-xs-12">


                    <ul class="top_menu" id="profile-tour-nav" style="display: flex; justify-content: center">
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="{{route('welcome')}}" class="home-a">
                                <span>@lang('my-text.head_menu_1')</span>
                            </a>
                        </li>
                        <li data-hover="sub-menu-link"
                            class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="javascript:void(0);" data-step="3"
                               data-intro="<h4>{{__('my-text.head_menu_2')}}</h4><p>{{__('my-text.key_6')}}</p>"
                               data-highlightclass="menu-highlight" data-position="bottom">
                                <span>@lang('my-text.head_menu_2')</span>
                            </a>
                            <ul class="sub-menu">
                                <li class="sub-menu-li">
                                    <a href="{{route('about')}}" class="sub-menu-a">@lang('my-text.head_menu_3')</a>
                                </li>
                                <li class="sub-menu-li">
                                    <a href="{{route('legal.doc')}}" class="sub-menu-a">@lang('my-text.head_menu_4')</a>
                                </li>
                                <li class="sub-menu-li">
                                    <a href="{{route('news')}}" class="sub-menu-a">@lang('my-text.head_menu_5')</a>
                                </li>
                            </ul>
                        </li>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="{{route('started')}}" data-step="4"
                               data-intro="<h4>{{__('my-text.head_menu_6')}}</h4><p>{{__('my-text.key_7')}}</p>"
                               data-highlightclass="menu-highlight" data-position="bottom">
                                <span>@lang('my-text.head_menu_6')</span>
                            </a>
                        </li>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="{{route('faq')}}" data-step="5"
                               data-intro="<h4>{{__('my-text.key_8')}}</h4><p>{{__('my-text.key_9')}}</p>"
                               data-highlightclass="menu-highlight" data-position="bottom">
                                <span>@lang('my-text.head_menu_7')</span>
                            </a>
                        </li>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="{{route('rules')}}" data-step="6"
                               data-intro="<h4>{{__('my-text.key_10')}}</h4><p>{{__('my-text.key_11')}}</p>"
                               data-highlightclass="menu-highlight" data-position="bottom">
                                <span>@lang('my-text.head_menu_8')</span>
                            </a>
                        </li>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="{{route('reviews')}}">
                                <span>@lang('my-text.head_menu_9')</span>
                            </a>
                        </li>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                            <a href="{{route('support')}}" data-step="8"
                               data-intro="<h4>{{__('my-text.key_12')}}</h4><p>{{__('my-text.key_13')}}</p>"
                               data-highlightclass="menu-highlight" data-position="bottom">
                                <span>@lang('my-text.head_menu_10')</span>
                            </a>
                        </li>
                        {{--<li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">--}}
                            {{--<a href="javascript:void(0);" class="start-tour" onclick="introJs().start()"--}}
                               {{--data-step="30" data-intro="<h4>--}}
								{{--{{__('my-text.key_14')}}</h4><p>{{__('my-text.key_15')}}</p>"--}}
                               {{--data-highlightclass="menu-highlight" data-position="left">--}}
                                {{--<i class="fa fa-info info-raz" aria-hidden="true"></i></a>--}}
                        {{--</li>--}}
                    </ul>

                </div>

                <div class="header_buttons_area header_buttons_area_acc">
                    <div class="investment_plans col-xs-12">
                        <a href="javascript:void(0)" class="show_plans">
                            <p>@lang('my-text.key_43')</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>