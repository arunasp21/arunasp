  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link active" href="{{route('welcome')}}">@lang('my-text.head_menu_1')</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('about')}}">@lang('my-text.head_menu_2')</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('started')}}">@lang('my-text.head_menu_6')</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('faq')}}">@lang('my-text.head_menu_7')</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('rules')}}">@lang('my-text.head_menu_8')</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('reviews')}}">@lang('my-text.head_menu_9')</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('support')}}">@lang('my-text.head_menu_10')</a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">ArunaSP &copy; 2021</p>
          </div>
<!--
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link active" href="#">Security</a></li>
              <li class="nav-item"> <a class="nav-link" href="#">Terms</a></li>
              <li class="nav-item"> <a class="nav-link" href="#">Privacy</a></li>
            </ul>
          </div>
-->
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end --> 
  
</div>
<!-- Document Wrapper end --> 

<!-- Back to Top
============================================= --> 
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a> 

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end --> 
