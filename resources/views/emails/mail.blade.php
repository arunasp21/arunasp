<html>

<head>
  <style>
    html,
    body {
      padding: 0;
      margin: 0;
      display: block;
      width: 100%;
      max-width: 100%
    }
    
    iframe {
      display: block;
      border: none;
    }
    
    img {
      max-width: 100%;
      margin: 0 auto;
      height: auto;
    }
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <style type="text/css">
    @media only screen and (max-width: 640px) {
      .sp-hidden-mob {
        display: none !important
      }
    }
  </style>
  <style type="text/css">
    /* SendPulse custom CSS Reset */
    
    table,
    td {
      border-collapse: collapse;
    }
    
    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
    }
    
    a {
      text-decoration: none;
    }
    
    p {
      font-size: 14px;
      line-height: 1.5;
      margin: 0 0 10px 0;
    }
    
    ul {
      margin: 0 0 10px 0;
    }
    
    ul > li {
      font-size: 14px;
      line-height: 1.5;
      margin: 0;
    }
    
    h1,
    h2,
    h3,
    h4,
    h5,
    h5 {
      line-height: 1.2;
      margin: 0 0 10px 0;
      font-weight: normal;
    }
    
    h1 {
      font-size: 36px;
    }
    
    h2 {
      font-size: 30px;
    }
    
    h3 {
      font-size: 24px;
    }
    
    h4 {
      font-size: 20px;
    }
    
    h5 {
      font-size: 14px;
    }
    
    hr {
      margin: 0;
    }
    
    th.tc,
    th.social_element {
      font-weight: normal;
      text-align: left;
    }
    
    tr,
    td,
    th {
      border-color: transparent;
    }
    
    .content-cell {
      vertical-align: top;
    }
    
    .content-cell table.sp-button,
    .content-cell table.social,
    .content-cell table.sp-button td,
    .content-cell table.social td,
    .content-cell table.sp-button th,
    .content-cell table.social th,
    .content-cell table.sp-button table,
    .content-cell table.social table {
      border-color: transparent;
      border-width: 0;
      border-style: none;
      border: 0;
    }
    
    .content-cell table.sp-button td,
    .content-cell table.social td,
    .content-cell table.sp-button th,
    .content-cell table.social th {
      padding: 0;
    }
    
    .content-cell table.social {
      line-height: 1;
    }
    
    .content-cell > center > .sp-button {
      margin-left: auto;
      margin-right: auto;
    }
    
    .content-cell .sp-button table td {
      line-height: 1;
    }
    
    .content-cell .sp-button-side-padding,
    .content-cell .sp-button-text,
    .content-cell .social,
    .content-cell .social_element {
      border-color: transparent;
      border-width: 0;
      border-style: none;
    }
    
    .content-cell .sp-button-side-padding {
      width: 21px;
    }
    
    .content-cell .sp-button-text a {
      text-decoration: none;
      display: block;
    }
    
    .content-cell .sp-button-text a img {
      max-width: 100%;
    }
    
    .content-cell span[style*="color"] > a {
      color: inherit;
    }
    
    .content-cell > div > .sp-img,
    .content-cell > div > a > .sp-img {
      margin: 0;
    }
    
    .content-cell .link_img {
      display: block;
    }
    
    .content-cell .sp-button-img td {
      display: table-cell !important;
      width: initial !important;
    }
    
    .email-text > p,
    .content-cell > p {
      line-height: inherit;
      color: inherit;
      font-size: inherit;
    }
    
    .email-text em,
    .content-cell em {
      color: inherit;
    }
    
    .email-text > table,
    .content-cell > table {
      border-color: #ddd;
      border-width: 1px;
      border-style: solid;
    }
    
    .email-text > table > tr > td,
    .content-cell > table > tr > td,
    .email-text > table > tr > th,
    .content-cell > table > tr > th,
    .email-text > table > tbody > tr > td,
    .content-cell > table > tbody > tr > td,
    .email-text > table > tbody > tr > th,
    .content-cell > table > tbody > tr > th {
      border-color: #ddd;
      border-width: 1px;
      border-style: solid;
    }
    
    .email-text > table td,
    .content-cell > table td,
    .email-text > table th,
    .content-cell > table th {
      padding: 3px;
    }
    
    .social_element,
    .content-cell table.social .social_element {
      padding: 2px 5px;
      font-size: 13px;
      font-family: Arial, sans-serif;
      line-height: 32px;
    }
    
    .social_element img.social,
    .content-cell table.social .social_element img.social {
      display: block;
    }
    
    .content-cell table.social .social_element_t_3 img.social,
    .content-cell table.social .social_element_t_4 img.social,
    .content-cell table.social .social_element_t_5 img.social,
    .content-cell table.social .social_element_v_i_t img.social {
      display: inline;
    }
    
    .email-text table th {
      text-align: center;
    }
    
    .email-text pre {
      background-color: transparent;
      border: 0;
      color: inherit;
      padding: 0;
      margin: 1em 0;
    }
    
    .email-wrapper span[style*="color"] > a {
      color: inherit;
    }
    
    .sp-video a {
      display: block;
      overflow: auto;
    }
    
    .sp-video img {
      max-width: 100%;
    }
    
    @media only screen and (max-width: 640px) {
      .sp-hidden-mob {
        display: none !important;
      }
    }
    /* CSS Helpers and Hacks */
    /* What it does: Remove spaces around the email design added by some email clients. */
    
    body {
      margin: 0;
      padding: 0;
    }
    /* What it does: Stops email clients resizing small text. */
    
    * {
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
    /* What it does: Stops Outlook from adding extra spacing to tables. */
    
    table,
    td {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    /* Outlook.com hacks */
    
    #outlook a {
      padding: 0;
    }
    
    .ReadMsgBody {
      width: 100%;
    }
    
    .ExternalClass {
      width: 100%;
    }
    
    .ExternalClass * {
      line-height: 100%;
    }
    /* What it does: Uses a better rendering method when resizing images in IE. */
    
    img {
      -ms-interpolation-mode: bicubic;
    }
    
    table {
      margin-bottom: 0 !important;
      border-color: transparent;
    }
    /* TARGETING Gmail */
    
    u ~ div .gmail-hide {
      display: none;
    }
    
    u ~ div .gmail-show {
      display: block !important;
    }
    /* TARGETING YAHOO! MAIL AND AOL */
    
    @media yahoo {
      .yahoo-hide {
        display: none;
      }
      .yahoo-show {
        display: block !important;
      }
    }
    /* What it does: Prevents Gmail from changing the text color in conversation threads. */
    
    .im {
      color: inherit !important;
    }
    /* Ukr.net hacks */
    
    td[class^='xfmc'] {
      width: inherit !important;
    }
    
    @media only screen and (max-width: 640px) {
      .wrapper-table {
        min-width: 296px;
      }
      .sp-demo-label-link {
        display: block;
      }
      table {
        width: 100% !important;
      }
      table,
      hr {
        width: 100%;
        max-width: 100% !important;
      }
      td,
      div {
        width: 100% !important;
        height: auto !important;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
      }
      img:not(.p100_img),
      .content-cell img {
        width: auto;
        height: auto;
        max-width: 100% !important;
      }
      td,
      th {
        display: block !important;
        margin-bottom: 0;
        height: inherit !important;
      }
      td.content-cell,
      th.content-cell {
        padding: 15px !important;
      }
      td.content-cell .social,
      th.content-cell .social {
        width: auto !important;
      }
      td.content-cell .social td .share td,
      th.content-cell .social td .share td,
      td.content-cell .social th,
      th.content-cell .social th,
      td.content-cell .share th,
      th.content-cell .share th {
        display: inline !important;
        display: inline-block !important;
      }
      td.content-cell .social td .share td.social_element_t_3,
      th.content-cell .social td .share td.social_element_t_3,
      td.content-cell .social th.social_element_t_3,
      th.content-cell .social th.social_element_t_3,
      td.content-cell .share th.social_element_t_3,
      th.content-cell .share th.social_element_t_3,
      td.content-cell .social td .share td.social_element_t_4,
      th.content-cell .social td .share td.social_element_t_4,
      td.content-cell .social th.social_element_t_4,
      th.content-cell .social th.social_element_t_4,
      td.content-cell .share th.social_element_t_4,
      th.content-cell .share th.social_element_t_4 {
        display: block !important;
      }
      td.content-cell .social td .share td a > img,
      th.content-cell .social td .share td a > img,
      td.content-cell .social th a > img,
      th.content-cell .social th a > img,
      td.content-cell .share th a > img,
      th.content-cell .share th a > img {
        width: 32px !important;
        height: 32px !important;
      }
      td.content-cell > td,
      th.content-cell > td {
        width: 100%;
      }
      td.content-cell > p,
      th.content-cell > p {
        width: 100% !important;
      }
      td.content-cell.padding-lr-0,
      th.content-cell.padding-lr-0 {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      td.content-cell.padding-top-0,
      th.content-cell.padding-top-0 {
        padding-top: 0 !important;
      }
      td.content-cell.padding-bottom-0,
      th.content-cell.padding-bottom-0 {
        padding-bottom: 0 !important;
      }
      .sp-video {
        padding-left: 15px !important;
        padding-right: 15px !important;
      }
      .wrapper-table > tbody > tr > td {
        padding: 0;
      }
      .block-divider {
        padding: 2px 15px !important;
      }
      .social_share {
        width: 16px !important;
        height: 16px !important;
      }
      .sp-button td {
        display: table-cell !important;
        width: initial !important;
      }
      .sp-button td.sp-button-side-padding {
        width: 21px !important;
      }
      input {
        max-width: 100% !important;
      }
      table {
        border-width: 1px;
      }
      .tc {
        width: 100% !important;
      }
      .small_img,
      table.email-checkout.email-checkout-yandex {
        width: auto !important;
      }
      table.smallImg td.smallImg {
        display: inline !important;
      }
      .inline-item {
        display: inline !important;
      }
      table.origin-table {
        width: 95% !important;
      }
      table.origin-table td {
        display: table-cell !important;
        padding: 0 !important;
      }
      table.origin-table td img.small_img {
        max-width: 120px !important;
      }
      .p100_img {
        width: 100% !important;
        max-width: 100% !important;
        height: auto !important;
      }
      /*! prevent replacing brackets */
    }
    
    @media only screen and (max-width: 640px) and screen and (-ms-high-contrast: active),
    only screen and (max-width: 640px) and (-ms-high-contrast: none) {
      td,
      th {
        float: left;
        width: 100%;
        clear: both;
      }
      img:not(.p100_img),
      .content-cell img {
        width: auto;
        height: auto;
        max-width: 269px !important;
        margin-right: auto;
        display: block !important;
        margin-left: auto;
      }
    }
    
    .content-cell {
      word-break: break-word;
    }
    
    .content-cell * {
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
    }
    
    .rollover {
      font-size: 0;
    }
    
    .rollover .rollover-second {
      max-height: 0 !important;
      display: none !important;
    }
    
    .rollover:hover .rollover-first {
      max-height: 0 !important;
      display: none !important;
    }
    
    .rollover:hover .rollover-second {
      max-height: none !important;
      display: block !important;
      -o-object-fit: cover;
      object-fit: cover;
    }
    
    @media only screen and (max-width: 640px) {
      .sp-hidden-mob {
        display: none !important;
      }
    }
  </style>
</head>

<body style="color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-size:14px; line-height:1.5; margin:0">
  <table class="wrapper-table" cellpadding="5" cellspacing="0" width="100%" border="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa; background-repeat:repeat" bgcolor="#f5f6fa">
    <tbody>
      <tr style="border-color:transparent">
        <td align="center" style="border-collapse:collapse; border-color:transparent">
          <table cellpadding="0" cellspacing="0" width="600px" id="bodyTable" border="0" bgcolor="#f5f6fa" style="border-collapse:collapse; font-size:14px; line-height:1.5">
            <tbody>
              <tr style="border-color:transparent">
                <td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; width:100%" border="0" width="100%">
                    <tbody>
                      <tr style="border-color:transparent">
                        <th width="300" style="border-color:transparent; font-weight:normal; text-align:left; vertical-align:top" cellpadding="0" cellspacing="0" class="tc" align="left" valign="top">
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5">
                                    <tbody>
                                      <tr class="content-row" style="border-color:transparent; color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif">
                                        <td class="content-cell padding-lr-0 padding-top-0 padding-bottom-0" width="300" style="border-collapse:collapse; border-color:transparent; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0" valign="top">
                                          <div id="style" font-size:14px="" line-height:1.5="" width:100="" height:68="" display:block="" width="100%" height="68">
                                            <a target="_blank" class="link_img" href="https://arunasp.com" style="text-decoration:none; color:#0089bf; display:block"><img border="0" width="153" height="auto" class=" sp-img small_img " align="left" alt="" src="https://s7896243.sendpul.se/files/emailservice/userfiles/9ccb0f6fcd833a3272c26530d35f15f77896243/logo1.png" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none; margin:0; display:block; -ms-interpolation-mode:bicubic"></a>
                                          </div>
                                          <div style="font-size:14px; line-height:1.5; clear:both"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </th>
                        <th width="300" style="border-color:transparent; font-weight:normal; text-align:left; vertical-align:top" cellpadding="0" cellspacing="0" class="tc" align="left" valign="top">
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table width="100%" cellpadding="0" cellspacing="0" id="w" style="border-collapse:collapse; font-size:14px; line-height:1.5; text-color:black; font-weight:normal; color:#6565ba; margin:0">
                                    <tbody>
                                      <tr class="content-row" style="border-color:transparent; color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif">
                                        <td class="content-cell padding-lr-0 padding-bottom-0" width="300" style="border-collapse:collapse; border-color:transparent; vertical-align:top; padding-left:0; padding-right:0; padding-top:15px; padding-bottom:0" valign="top">
                                          <h4 style="line-height:1.2; margin:0 0 10px 0; font-weight:normal; font-size:20px; color:#6565ba; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; text-align:center" align="center"><em style="color:inherit"><span style="font-size: 20px;">Начните</span><span style="font-size: 20px;"> зарабатывать</span></em><br><em style="color:inherit"><span style="font-size: 20px;">сегодня вместе с ArunaSP</span></em></h4>
                                          <div style="font-size:14px; line-height:1.5; clear:both"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; width:100%" border="0" width="100%">
                    <tbody>
                      <tr style="border-color:transparent">
                        <th width="600" style="border-color:transparent; font-weight:normal; text-align:left; vertical-align:top" cellpadding="0" cellspacing="0" class="tc" align="left" valign="top">
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table class="separator" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0; height:40px" height="40">
                                    <tbody>
                                      <tr style="border-color:transparent">
                                        <td height="40" style="border-collapse:collapse; border-color:transparent"></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; width:100%" border="0" width="100%">
                    <tbody>
                      <tr style="border-color:transparent">
                        <th width="600" style="border-color:transparent; font-weight:normal; text-align:left; vertical-align:top" cellpadding="0" cellspacing="0" class="tc" align="left" valign="top">
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table width="100%" cellpadding="0" cellspacing="0" id="w" style="border-collapse:collapse; font-size:14px; line-height:1.5; text-color:black; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-family-short:georgia; font-weight:normal; color:#656565; margin:0">
                                    <tbody>
                                      <tr class="content-row" style="border-color:transparent; color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif">
                                        <td class="content-cell padding-lr-0" width="585" style="border-collapse:collapse; border-color:transparent; vertical-align:top; padding-left:0; padding-right:15px; padding-top:15px; padding-bottom:15px" valign="top">
                                          <h2 style="line-height:1.2; margin:0 0 10px 0; font-weight:normal; font-size:30px; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; margin-bottom:10px; margin-top:0; text-align:center" align="center"><span style="font-family: 'trebuchet ms', geneva; color: #6565ba;">Деятельность компании</span></h2>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp; Наша цель - обеспечить население планеты стабильными поставками здоровых и экологически чистых продуктов питания в области аквакультуры. При этом сохранив мировой океан и окружающую среду.</p>
                                          <div style="font-size:14px; line-height:1.5; clear:both"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table width="100%" cellpadding="0" cellspacing="0" id="w" style="border-collapse:collapse; font-size:14px; line-height:1.5; text-color:black; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-family-short:georgia; font-weight:normal; color:#656565; margin:0">
                                    <tbody>
                                      <tr class="content-row" style="border-color:transparent; color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif">
                                        <td class="content-cell padding-lr-0" width="585" style="border-collapse:collapse; border-color:transparent; vertical-align:top; padding-left:0; padding-right:15px; padding-top:15px; padding-bottom:15px" valign="top">
                                          <h2 style="line-height:1.2; margin:0 0 10px 0; font-weight:normal; font-size:30px; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; margin-bottom:10px; text-align:center" align="center"><span style="font-family: 'trebuchet ms', geneva; color: #6565ba;">Почему выбирают нас?</span></h2>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp; С ArunaSP Вы обеспечиваете себя пассивным доходом получая прибыль каждый день. Для этого нужно всего лишь выбрать понравившийся инвестиционный план.</p>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp; Низкий порог входа. Начать инвестировать можно имея всего лишь 10$.</p>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp; Высокая доходность. В зависимости от выбранного инвестиционного плана, Ваш доход будет составлять от 2% в день от открытого депозита.</p>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp; Быстрые выплаты. Вывод средств 24/7 без комиссий в любое удобное для Вас время.</p>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp; Трехуровневая реферальная система. У нас Вы можете зарабатывать без каких либо вложений, получая доход с каждого депозита приглашенного Вами инвестора.</p>
                                          <div style="font-size:14px; line-height:1.5; clear:both"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table width="100%" cellpadding="0" cellspacing="0" id="w" style="border-collapse:collapse; font-size:14px; line-height:1.5; text-color:black; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-family-short:georgia; font-weight:normal; color:#656565; margin:0">
                                    <tbody>
                                      <tr class="content-row" style="border-color:transparent; color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif">
                                        <td class="content-cell padding-lr-0" width="585" style="border-collapse:collapse; border-color:transparent; vertical-align:top; padding-left:0; padding-right:15px; padding-top:15px; padding-bottom:15px" valign="top">
                                          <h2 style="line-height:1.2; margin:0 0 10px 0; font-weight:normal; font-size:30px; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; margin-bottom:10px; text-align:center" align="center"><span style="font-family: 'trebuchet ms', geneva; color: #6565ba;">Присоединяйтесь уже сегодня!</span></h2>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; font-weight:normal; padding:0">&nbsp; &nbsp;Подробно ознакомиться с деятельностью компании, тарифными планами, зарегистрироваться в системе и открыть депозит Вы сможете на нашем <a target="_blank" href="https://arunasp.com" style="text-decoration:none; color:#0089bf">официальном сайте.</a></p>
                                          <p style="font-size:inherit; line-height:inherit; margin:0 0 10px 0; color:#656565; font-family:Georgia, &quot;Times New Roman&quot;, Times, serif; text-align:center; font-weight:normal; padding:0" align="center"><strong><span style="font-size: 18px;">Ждем Вас в коллективе наших партнеров!</span></strong></p>
                                          <div style="font-size:14px; line-height:1.5; clear:both"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <div class="block-divider" style="font-size:14px; line-height:1.5; padding-left:0; padding-right:0; padding-top:5px; padding-bottom:5px">
                                    <hr id="style" margin:0="" border-top-style:solid="" border-top-width:1px="" border-top-color:="" border-bottom:0="" border-left:0="" border-right:0="">
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5; background-color:#f5f6fa" bgcolor="#f5f6fa">
                            <tbody>
                              <tr style="border-color:transparent">
                                <td cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-color:transparent">
                                  <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:14px; line-height:1.5">
                                    <tbody>
                                      <tr class="content-row" style="border-color:transparent; color:#444; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif">
                                        <td class="content-cell" width="570" style="border-collapse:collapse; border-color:transparent; vertical-align:top; padding-left:15px; padding-right:15px; padding-top:15px; padding-bottom:15px" valign="top">
                                          <center>
                                            <table cellpadding="0" border="0" cellspacing="0" class="sp-button gradient fixed-width" width="100% !important" style="border-collapse:collapse; font-size:14px; line-height:1.5; border-color:transparent; border-width:0; border-style:none; border:0; margin-left:auto; margin-right:auto; width:100% !important; max-width:252px !important; border-radius:5px; box-shadow:none; background-color:#206b9e; background:linear-gradient(to top, #206b9e, #5aa9de)" bgcolor="#206b9e">
                                              <tbody>
                                                <tr style="border-color:transparent">
                                                  <td class="sp-button-text" style="border-collapse:collapse; border-color:transparent; border-width:0; border-style:none; border:0; padding:0; align:center; border-radius:5px; width:252px; height:40px; vertical-align:middle; text-align:center" width="252" height="40" valign="middle" align="center">
                                                    <table cellpadding="0" border="0" cellspacing="0" width="100%" style="border-collapse:collapse; font-size:14px; line-height:1.5; border-color:transparent; border-width:0; border-style:none; border:0">
                                                      <tbody>
                                                        <tr style="border-color:transparent">
                                                          <td align="center" style="border-collapse:collapse; border-color:transparent; border-width:0; border-style:none; border:0; padding:0; line-height:1"><a target="_blank" style="text-decoration:none; color:#FFF; display:block; padding:12px 0; font-family:Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-family-short:arial; font-size:16px; font-weight:bold" href="https://arunasp.com">Перейти на сайт</a></td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </center>
                                          <div style="font-size:14px; line-height:1.5; clear:both"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>

</body>

</html>