<div>
    <h1>@lang('my-text.key_319')</h1>
    <h3>@lang('my-text.key_320'):</h3>
    <a href="{{config('app.url') . '/verification-email?email=' . $data}}">Verification email</a>
</div>