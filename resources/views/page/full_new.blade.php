@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
  <section class="page-header page-header-text-light bg-dark-3 py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}');">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-8 order-1 order-md-0">
          <h1>Новости</h1>
        </div>
        <div class="col-md-4 order-0 order-md-1">
          <ul class="breadcrumb justify-content-start justify-content-md-end mb-0">
            <li><a href="#">Главная</a></li>
            <li class="active">Новости</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
    <div class="container">
      <div class="row"> 
        
        <!-- Middle Panel
        ============================================= -->
        <div class="col-lg-8 col-xl-9">
          <div class="row">
            <div class="col-12">
              <div class="blog-post card shadow-sm border-0 mb-4"> <img class="card-img-top" src="{{ asset('components/images/news/c1.jpg') }}" alt="">
                <div class="card-body">
                  <h4 class="title-blog">Новогодний приветственный бонус от ArunaSP Club!</h4>
                  <ul class="meta-blog">
                    <li><i class="fas fa-calendar-alt"></i> 19.12.2021</li>
                    <li><a href=""><i class="fas fa-user"></i> ArunaSP</a></li>
<!--                    <li><a href=""><i class="fas fa-comments"></i> 10</a></li>-->
                  </ul>
                  <p>ArunaSP Club поздравляет всех с наступающим Новым годом и приветствует новых партнеров, зарегистрировавшихся в период с 19.12.2021 по 14.12.2022 включительно, бонусом в размере 10$ на счет. Для получения бонуса не обязательно совершать пополнение счета. Достаточно при регистрации в форме отметить поле "Получить приветственный бонус" и 10$ будут автоматически зачислены на Ваш счет после верификации электронной почты.</p>
                  <p>Бонус можно использовать для создания депозита и получения по нему процентов с последующим полным или частичным выводом. Проценты  начисляются точно такие же, как и на лично введенные средства.</p>
                  <p>Как вывести бонусные средства, а так же начисленные по ним проценты, если они поучавствовали в депозите? Для вывода нужно в период действия акции, а именно, с 19.12.2021 по 14.12.2022 включительно, открыть депозит на любую сумму в любом тарифном плане. При этом должны использоваться личные средства. После завершения срока депозита согласно выбранному тарифному плану, бонус и начисленные по нему проценты, если таковые имеются, сразу же доступны на вывод. В случае отсутствия депозита из личных средств в период действия акции, по ее завершении, бонусные средства и проценты по ним аннулируются. Депозит из бонуса в данном случае не учитывается.</p>
                  <p>Пусть в Новом году самые смелые планы воплотятся в реальность, самые интересные идеи станут проектами, самые сложные цели будут достигнуты. Желаем веры в успех, в собственные силы и совместные усилия. Пускай этот год будет успешным в личной и рабочей сферах! Счастливого Нового года!</p>
				  <p>С уважением, администрация ArunaSP Club!</p>
				</div>
              </div>
            </div>
       
        </div>
        <!-- Middle Panel End --> 
       
      </div>
	  
        <!-- Right Sidebar
        ============================================= -->
        <aside class="col-lg-4 col-xl-3"> 
          
          <!-- Recent Posts
          =============================== -->
          <div class="bg-white shadow-sm rounded p-3 mb-4">
            <h4 class="text-5 font-weight-400">Последние новости</h4>
            <hr class="mx-n3">
            <div class="side-post">
              <div class="item-post">
                <div class="img-thumb"><a href="#"><img class="rounded" src="{{ asset('components/images/news/65.jpg') }}" width="65" height="65" title="" alt=""></a></div>
                <div class="caption"> <a href="blog-single.html">Новогодний бонус от ArunaSP Club!</a>
                  <p class="date-post">19.12.2021</p>
                </div>
              </div>
            </div>
          </div>
          
        </aside>
        <!-- Right Sidebar End --> 
	  
    </div>
  </div>
  <!-- Content end --> 

@endsection
