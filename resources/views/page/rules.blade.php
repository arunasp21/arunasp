@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}'); margin: 0;">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="index.html">Home</a></li>
          <li class="active">@lang('my-text.about_us')</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>@lang('my-text.about_us')</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
    
    <!-- Who we are
    ============================================= -->
  <section class="section bg-white py-4">
    <div class="container">
      <h2 class="text-9 text-center">@lang('my-text.key_160')</h2>
      <p class="lead text-center">@lang('my-text.key_161'):</p>
		<div class="row">
		  <div class="col-lg-12 d-flex">
			<div class="my-auto px-0 px-lg-5 mx-2">
			  <p class="text-4">
                <h4>@lang('my-text.key_162')</h4>
                <ul>
                    <li>
                        1.1 @lang('my-text.key_163').
                    </li>
                    <li>
                        1.2 @lang('my-text.key_164').
                    </li>
                    <li>
                        1.3 @lang('my-text.key_165').
                    </li>
                    <li>
                        1.4 @lang('my-text.key_166').
                    </li>
                    <li>
                        1.5 @lang('my-text.key_167').
                    </li>
                    <li>
                        1.6 @lang('my-text.key_168').
                    </li>
                </ul>
                <h4>@lang('my-text.key_169')</h4>
                <ul>
                    <li>
                        2.1 @lang('my-text.key_170').
                    </li>
                    <li>
                        2.2 @lang('my-text.key_171').
                    </li>
                    <li>
                        2.3 @lang('my-text.key_172').
                    </li>
                    <li>
                        2.4 @lang('my-text.key_173').
                    </li>
                    <li>
                        2.5 @lang('my-text.key_174').
                    </li>
                    <li>
                        2.6 @lang('my-text.key_175').
                    </li>
                    <li>
                        2.7 @lang('my-text.key_176').
                    </li>
                    <li>
                        2.8 @lang('my-text.key_177').
                    </li>
                    <li>
                        2.9 @lang('my-text.key_178').
                    </li>
                    <li>
                        2.10 @lang('my-text.key_179').
                    </li>
                    <li>
                        2.11 @lang('my-text.key_180').
                    </li>
                </ul>
                <h4>@lang('my-text.key_181')</h4>
                <ul>
                    <li>
                        3.1 @lang('my-text.key_182').
                    </li>
                    <li>
                        3.2 @lang('my-text.key_183').
                    </li>
                    <li>
                        3.3 @lang('my-text.key_184').
                    </li>
                    <li>
                        3.4 @lang('my-text.key_185').
                    </li>
                    <li>
                        3.5 @lang('my-text.key_186').
                    </li>
                    <li>
                        3.6 @lang('my-text.key_187').
                    </li>
                    <li>
                        3.7 @lang('my-text.key_188').
                    </li>
                    <li>
                        3.8 @lang('my-text.key_189').
                    </li>
                    <li>
                        3.9 @lang('my-text.key_190').
                    </li>
                    <li>
                        3.10 @lang('my-text.key_191').
                    </li>
                    <li>
                        3.11 @lang('my-text.key_192').
                    </li>
                </ul>
                <h4>@lang('my-text.key_193')</h4>
                <ul>
                    <li>
                        4.1 @lang('my-text.key_194') 100.00 <i class="fa fa-usd"></i> @lang('my-text.key_195'): Payeer, PerfectMoney, Qiwi, YandexMoney, BitCoin, VISA/MasterCard.
                    </li>
                    <li>
                        4.2 @lang('my-text.key_196'): 1
                        <i class="fa fa-usd"></i>. @lang('my-text.key_197').
                    </li>
                    <li>
                        4.3 @lang('my-text.key_198').
                    </li>
                    <li>
                        4.4 @lang('my-text.key_199').
                    </li>
                    <li>
                        4.5 @lang('my-text.key_200').
                    </li>
                </ul>
                <h4>@lang('my-text.key_201')</h4>
                <ul>
                    <li>
                        5.4 @lang('my-text.key_202').
                    </li>
                    <li>
                        5.5 @lang('my-text.key_203').
                    </li>
                    <li>
                        5.6 @lang('my-text.key_204').
                    </li>
                    <li>
                        5.7 @lang('my-text.key_205').
                    </li>
                    <li>
                        5.8 @lang('my-text.key_206').
                    </li>
                    <li>
                        @lang('my-text.key_207').
                    </li>
                    <li>
                        @lang('my-text.key_208').
                    </li>
                </ul>
			  </p>
			</div>
		  </div>
		</div>
    </div>
  </section>

@endsection
