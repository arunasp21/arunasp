@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}');">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="index.html">Home</a></li>
          <li class="active">@lang('my-text.about_us')</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>@lang('my-text.about_us')</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
    
    <!-- Who we are
    ============================================= -->
    <section class="section" style="padding: 0;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 d-flex">
            <div class="my-auto px-0 px-lg-5 mx-2">
              <p class="text-4">
				<img class="img-fluid shadow-lg rounded-lg" src="{{ asset('components/images/Matorka.jpeg') }}" width="555" height="370" style="float: right; margin: 15px 0 15px 15px;" alt="">
				@lang('my-text.about_text')
			  </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Who we are end -->

    <section class="section bg-white" style="padding-top: 2rem; padding-bottom: 0.9rem;">
      <div class="container">
        <h2 class="text-9 text-center">@lang('my-text.our_priorities')</h2>
        <div class="row">
          <div class="col-sm-6 col-md-3 text-center mb-4 mb-md-0">
            <div class="team rounded d-inline-block"> <img class="img-fluid rounded" alt="" src="{{ asset('components/images/Visser-met-zeewier.jpg') }}">
              <h3>@lang('my-text.ocean_conservation')</h3>
              <p class="text-muted">@lang('my-text.ocean_conservation_text')</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 text-center mb-4 mb-md-0">
            <div class="team rounded d-inline-block"> <img class="img-fluid rounded" alt="" src="{{ asset('components/images/fish2.jpg') }}">
              <h3>@lang('my-text.food_security')</h3>
              <p class="text-muted">@lang('my-text.food_security_text')</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 text-center mb-4 mb-md-0">
            <div class="team rounded d-inline-block"> <img class="img-fluid rounded" alt="" src="{{ asset('components/images/ban1_1.jpg') }}" style="height: 174px;">
              <h3>@lang('my-text.environment')</h3>
              <p class="text-muted">@lang('my-text.environment_text')</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 text-center mb-4 mb-md-0">
            <div class="team rounded d-inline-block"> <img class="img-fluid rounded" alt="" src="{{ asset('components/images/pharma.jpeg') }}" style="height: 174px;">
              <h3>@lang('my-text.health_benefits')</h3>
              <p class="text-muted">@lang('my-text.health_benefits_text')</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Leadership end -->
    
    <!-- Our Values
    ============================================= -->
    <section class="section" style="padding-top: 2rem; padding-bottom: 3rem;">
      <div class="container">
        <h2 class="text-9 text-center mb-4">@lang('my-text.invest_with_us')</h2>
        <div class="row no-gutters">
          <div class="col-lg-6 order-2 order-lg-1">
            <div class="row">
              <div class="col-6 col-lg-7 ml-auto mb-lg-n5"> <img class="img-fluid rounded-lg shadow-lg" src="{{ asset('components/images/unsplash.jpg') }}" alt=""> </div>
              <div class="col-6 col-lg-8 mt-lg-n5"> <img class="img-fluid rounded-lg shadow-lg" src="{{ asset('components/images/revenue_invest.jpg') }}" alt=""> </div>
            </div>
          </div>
          <div class="col-lg-6 d-flex order-1 order-lg-2">
            <div class="my-auto px-0 px-lg-5">
              <h4 class="text-4 font-weight-500">@lang('my-text.invest_with_us_text1')</h4>
              <p class="tex-3" >@lang('my-text.invest_with_us_text2')</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Values end -->
    
  </div>
  <!-- Content end --> 


@endsection
