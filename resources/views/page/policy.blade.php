@extends('welcome')

@section('content')

    @include('partial.second_header')

    @include('partial.components._investment')

    <div class="tpl_wrap clearfix rules">
        <div class="container">
            <div class="row">
                <h2>@lang('my-text.key_104')</h2>

                <p align="justify">
                    @lang('my-text.key_105')
                </p>
                <br>
                <h3>@lang('my-text.key_106')</h3>

                <p align="justify">
                    @lang('my-text.key_107')
                    <br>
                    <br>
                    @lang('my-text.key_108')
                    <br>
                    <br>
                    @lang('my-text.key_109')
                </p>
                <br>
                <h3>@lang('my-text.key_110')</h3>

                <p align="justify">
                    @lang('my-text.key_111')
                    <br>
                    <br>
                    @lang('my-text.key_112')
                </p>
                <br>
                <h3>@lang('my-text.key_113')</h3>

                <p align="justify">
                    @lang('my-text.key_114')
                </p>
                <br>
                <h3>@lang('my-text.key_115') </h3>

                <p align="justify">
                    @lang('my-text.key_116')
                    <br>
                    <br>
                    @lang('my-text.key_117')
                    <br>
                    <br>
                    @lang('my-text.key_118')
                </p>
                <br>
            </div>
        </div>
    </div>

@endsection
