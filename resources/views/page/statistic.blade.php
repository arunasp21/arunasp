@extends('welcome')

@section('content')

    @include('partial.second_header')

    @include('partial.components._investment')

    <div class="edit_account">
        <div class="container">
            <div class="row">
                <div class="col-xs-12" style="margin-left: 136px;">
                    <div class="tpl_wrap clearfix earn_history_tpl_wrap">
                        <div class="earn_history clearfix">
                            <div class="ptable four">
                                <div class="col-xs-6">
                                    <div>
                                        <h3 style="text-align: center;">@lang('my-text.key_90')</h3>
                                    </div>
                                    <table class="white_table" cellspacing="1" cellpadding="2" border="0" width="100%">
                                        <thead>
                                        <tr class="dark_inheader">
                                            <td class="inheader" width="235px"
                                                style="padding-left:10px; text-align: center">
                                                @lang('my-text.key_88')
                                            </td>
                                            <td class="inheader" width="170px">
                                                @lang('my-text.key_87') (<i class="fa fa-usd"></i>)
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($last_input as $item)
                                            <tr>
                                                <td width="235px" style="padding-left:10px; text-align: center;">
                                                    <span>{{$item->user_id}}</span>
                                                </td>
                                                <td width="170px">
                                                    <div class="amount">
                                                        <i class="fa fa-usd"></i> {{$item->amount}}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-6">
                                    <div>
                                        <h3 style="text-align: center;">@lang('my-text.key_89')</h3>
                                    </div>
                                    <table class="white_table" cellspacing="1" cellpadding="2" border="0" width="100%">
                                        <thead>
                                        <tr class="dark_inheader">
                                            <td class="inheader" width="235px"
                                                style="padding-left:10px; text-align: center">
                                                @lang('my-text.key_88')
                                            </td>
                                            <td class="inheader" width="170px">
                                                @lang('my-text.key_87') (<i class="fa fa-usd"></i>)
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($last_output as $item)
                                            <tr>
                                                <td width="235px" style="padding-left:10px; text-align: center;">
                                                    <span>{{$item->user_id}}</span>
                                                </td>
                                                <td width="170px">
                                                    <div class="amount">
                                                        <i class="fa fa-usd"></i> {{$item->amount}}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection