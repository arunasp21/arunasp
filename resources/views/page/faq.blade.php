@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}'); margin: 0;">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="index.html">Home</a></li>
          <li class="active">FAQ</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>FAQ</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
    
  <section class="section bg-white py-4">
    <div class="container">
      <div class="row" id="cathegory1">
        <div class="col-md-10 col-lg-8 mx-auto">
		<h2 class="text-9 text-center">@lang('my-text.key_213')</h2>
          <hr class="mb-0">
          <div class="accordion accordion-alternate arrow-right" id="popularTopics">
            <div class="card">
              <div class="card-header" id="heading1">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">@lang('my-text.key_214')</a> </h5>
              </div>
              <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_215')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading2">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">@lang('my-text.key_216')</a> </h5>
              </div>
              <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_217')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading3">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">@lang('my-text.key_218')</a> </h5>
              </div>
              <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_219')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading4">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">@lang('my-text.key_220')</a> </h5>
              </div>
              <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_221')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading5">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">@lang('my-text.key_222')</a> </h5>
              </div>
              <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_223')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading6">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">@lang('my-text.key_224')</a> </h5>
              </div>
              <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_225')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading7">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">@lang('my-text.key_226')</a> </h5>
              </div>
              <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_227')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading8">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">@lang('my-text.key_228')</a> </h5>
              </div>
              <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_229')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading9">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">@lang('my-text.key_230')</a> </h5>
              </div>
              <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_231')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading10">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">@lang('my-text.key_232')</a> </h5>
              </div>
              <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_233')</div>
              </div>
            </div>
          </div>
          <hr class="mt-0">
        </div>
      </div>
      <div class="row" id="cathegory2">
        <div class="col-md-10 col-lg-8 mx-auto">
		<h2 class="text-9 text-center">@lang('my-text.key_234')</h2>
          <hr class="mb-0">
          <div class="accordion accordion-alternate arrow-right" id="popularTopics">
            <div class="card">
              <div class="card-header" id="heading11">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">@lang('my-text.key_235')</a> </h5>
              </div>
              <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_236')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading12">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">@lang('my-text.key_237')</a> </h5>
              </div>
              <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_238')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading13">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">@lang('my-text.key_239')</a> </h5>
              </div>
              <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_240')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading14">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">@lang('my-text.key_241')</a> </h5>
              </div>
              <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_242')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading15">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">@lang('my-text.key_243')</a> </h5>
              </div>
              <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_244')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading16">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">@lang('my-text.key_245')</a> </h5>
              </div>
              <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_246')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading17">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">@lang('my-text.key_247')</a> </h5>
              </div>
              <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_248')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading19">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="collapse19">@lang('my-text.key_251')</a> </h5>
              </div>
              <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_252')</div>
              </div>
            </div>
          </div>
          <hr class="mt-0">
        </div>
      </div>
      <div class="row" id="cathegory3">
        <div class="col-md-10 col-lg-8 mx-auto">
		<h2 class="text-9 text-center">@lang('my-text.key_253')</h2>
          <hr class="mb-0">
          <div class="accordion accordion-alternate arrow-right" id="popularTopics">
            <div class="card">
              <div class="card-header" id="heading20">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="collapse20">@lang('my-text.key_254')</a> </h5>
              </div>
              <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_255')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading21">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="collapse21">@lang('my-text.key_256')</a> </h5>
              </div>
              <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_257')</div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="heading22">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22">@lang('my-text.key_258')</a> </h5>
              </div>
              <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_259')</div>
              </div>
            </div>
          </div>
          <hr class="mt-0">
        </div>
      </div>
      <div class="row" id="cathegory4">
        <div class="col-md-10 col-lg-8 mx-auto">
		<h2 class="text-9 text-center">@lang('my-text.key_260')</h2>
          <hr class="mb-0">
          <div class="accordion accordion-alternate arrow-right" id="popularTopics">
            <div class="card">
              <div class="card-header" id="heading23">
                <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23">@lang('my-text.key_261')</a> </h5>
              </div>
              <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#popularTopics">
                <div class="card-body">@lang('my-text.key_262')</div>
              </div>
            </div>
          </div>
          <hr class="mt-0">
        </div>
      </div>
    </div>
  </section>
  <!-- Frequently asked questions end --> 

@endsection
