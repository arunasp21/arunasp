@extends('welcome')

@section('content')

    @include('partial.second_header')

    @include('partial.components._investment')

    <div class="legal-docs">
        <div class="container">
            <h2>@lang('my-text.key_139')</h2>
            <div class="row">
                <div class="col-xs-7">
                    <p>
                        @lang('my-text.key_140')
                        <a
                                href="https://companycheck.co.uk/company/07106043/TAXI-CAB-SOLUTIONS-LANCS-LIMITED/companies-house-data" target="_blank">
                            https://companycheck.co.uk/company/07106043/TAXI-CAB-SOLUTIONS-LANCS-LIMITED/companies-house-data
                        </a>
                        .
                    </p>
                    <div class="legal-docs__green-section">
                        <h1><b>@lang('my-text.key_141')</b></h1>
                        <blockquote class="quote_bottom">
                            <p>
                                @lang('my-text.key_142')
                            </p>
                        </blockquote>
                    </div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-4">
                    <div class="legal-docs__slider">
                    <span>
                        <img style="width: 385px; height: 581px" src="{{ asset('components/img/doc_img.png') }}" alt="Certificate of Incorporation" class="">
                    </span>
                        <span>
                        <img style="width: 385px; height: 581px" src="{{ asset('components/img/new_doc_1.png') }}" alt="Forex Investing LTD" class="">
                    </span>
                        <span>
                        <img style="width: 385px; height: 581px" src="{{ asset('components/img/new_doc_2.png') }}" alt="Forex Investing LTD 2-nd page" class="">
                    </span>
                        <span>
                        <img style="width: 385px; height: 581px" src="{{ asset('components/img/new_doc_3.png') }}" alt="Forex Investing LTD 2-nd page" class="">
                    </span>
                    </div>
                    <div class="legal-docs__buttons">
                        <a class="legal-docs__btn" id="legal_prev">
                            <img src="{{ asset('components/img/slider_arrow_left.png') }}" alt="">
                        </a>
                        <a class="legal-docs__btn" id="legal_next">
                            <img src="{{ asset('components/img/slider_arrow_left.png') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="legal-docs__bottom">
                <h1><b>@lang('my-text.key_143')</b></h1>
                <p>
                    @lang('my-text.key_144')
                </p>
            </div>
        </div>
    </div>

@endsection