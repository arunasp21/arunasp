@extends('welcome')

@section('content')

    @include('partial.second_header')

    @include('partial.components._investment')

    <div class="tpl_wrap clearfix rules">
        <div class="container">
            <div class="row">
                <h2>@lang('my-text.key_154')</h2>

                <p align="justify">
                    @lang('my-text.key_155')
                    <br>
                    <br>
                    @lang('my-text.key_156')
                    <br>
                    <br>
                    @lang('my-text.key_157')
                    <br>
                    <br>
                    @lang('my-text.key_158')
                    <br>
                    <br>
                    @lang('my-text.key_159')
                </p>
            </div>
        </div>
    </div>

@endsection
