@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}'); margin: 0;">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="#">Главная</a></li>
          <li class="active">Отзывы</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>Отзывы</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
  
  <section class="section py-4">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto">
		@foreach($comments as $comment)
          <div class="blog-post card shadow-sm border-0 mb-4">
            <div class="row no-gutters p-3">
              <div class="col-md-1"> <a href="blog-single.html"><img class="card-img d-block" src="{{ asset('components/images/def-avatar.png') }}" alt=""></a> </div>
              <div class="col-md-11">
                <div class="card-body pb-0 pt-3 pt-md-0 pl-0 pl-md-4 pr-0">
                  <p>{{ $comment->text }}</p>
                  <ul class="meta-blog">
                    <li><i class="fas fa-calendar-alt"></i> {{ $comment->author->updated_at }}</li>
                    <li><a href=""><i class="fas fa-user"></i> {{ $comment->author->name }}</a></li>
                  </ul>
				</div>
			  </div>
			</div>
		  </div>
		  @endforeach
		</div>
	  </div>
  </section>
  <!-- Frequently asked questions end --> 
  <section class="section bg-white py-4">
	<div class="container">
        <h2 class="text-9 text-center">@lang('my-text.key_100')</h2>
		@auth
			<div class="col-md-12 mx-auto">
				<form id="run_comment" method="post" action="{{route('save.comment')}}">
					@csrf
					<div class="form-group">
						<textarea name="text" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
					</div>
				  <button class="btn btn-primary" onclick="document.getElementById('run_comment').submit()">@lang('my-text.key_101')</button>
				</form>
			</div>
		@else
			<p>@lang('my-text.key_102')</p>
		@endauth
	</div>
  </section>  

@endsection
