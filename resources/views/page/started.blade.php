@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}');">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="index.html">Home</a></li>
          <li class="active">@lang('my-text.about_us')</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>@lang('my-text.about_us')</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
    
    <!-- Who we are
    ============================================= -->
    <section class="section" style="padding: 0;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 d-flex">
            <div class="my-auto px-0 px-lg-5 mx-2">
				<img class="img-fluid shadow-lg rounded-lg" src="{{ asset('components/img/Scr1.png') }}" width="555" height="370" style="float: right; margin: 0 0 15px 15px;" alt="">
				<h3><span>1.</span> @lang('my-text.key_91') ArunaSP</h3>
				<p class="text-4">
					@lang('my-text.key_92')
				</p>
            </div>
          </div>
        </div><br>
        <div class="row">
          <div class="col-lg-12 d-flex">
            <div class="my-auto px-0 px-lg-5 mx-2">
				<img class="img-fluid shadow-lg rounded-lg" src="{{ asset('components/img/Scr2.png') }}" width="555" height="370" style="float: left; margin: 0 20px 15px 0;" alt="">
				<h3><span>2.</span> @lang('my-text.key_93')</h3>
				<p class="text-4">
					@lang('my-text.key_94')
				</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 d-flex">
            <div class="my-auto px-0 px-lg-5 mx-2">
				<img class="img-fluid shadow-lg rounded-lg" src="{{ asset('components/img/Scr3.png') }}" width="555" height="370" style="float: right; margin: 0 0 15px 15px;" alt="">
				<h3><span>3.</span> @lang('my-text.key_95')</h3>
				<p class="text-4">
					@lang('my-text.key_96')
				</p>
            </div>
          </div>
        </div><br>
        <div class="row">
          <div class="col-lg-12 d-flex">
            <div class="my-auto px-0 px-lg-5 mx-2">
				<img class="img-fluid shadow-lg rounded-lg" src="{{ asset('components/img/Scr4.png') }}" width="555" height="370" style="float: left; margin: 0 20px 15px 0;" alt="">
				<h3><span>4.</span> @lang('my-text.key_97')</h3>
				<p class="text-4">
					@lang('my-text.key_98')
				</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Who we are end -->

@endsection
