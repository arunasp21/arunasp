@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

  <!-- Content
  ============================================= -->
  <div id="content"> 
    
    <!-- Slideshow
    ============================================= -->
    <div class="owl-carousel owl-theme single-slideshow" data-autoplay="true" data-loop="true" data-autoheight="true" data-nav="true" data-items="1">
      <div class="item">
        <section class="hero-wrap">
          <div class="hero-mask opacity-7 bg-dark"></div>
          <div class="hero-bg dark" style="background-image:url({{ asset('components/images/bg/ban1_1.png') }});"></div>
          <div class="hero-content d-flex fullscreen-with-header py-5">
            <div class="container my-auto text-center">
              <h2 class="text-16 text-white">ArunaSP CLUB</h2>
              <p class="text-5 text-white mb-4">@lang('my-text.slogan_1_1'),<br class="d-none d-lg-block">
				@lang('my-text.slogan_1_2')<br class="d-none d-lg-block">
				@lang('my-text.slogan_1_3').
			  </p>
              <a href="{{auth()->check() ? route('invest') : route('register')}}" class="btn btn-primary m-2">@lang('my-text.key_2')</a>
			</div>
          </div>
        </section>
      </div>
      <div class="item">
        <section class="hero-wrap">
          <div class="hero-mask opacity-7 bg-dark"></div>
          <div class="hero-bg" style="background-image:url({{ asset('components/images/bg/ban1_1.jpeg') }});"></div>
          <div class="hero-content d-flex fullscreen-with-header py-5">
            <div class="container my-auto">
              <div class="row">
                <div class="col-12 col-lg-8 col-xl-7 text-center text-lg-left">
                  <h2 class="text-13 text-white">@lang('my-text.slogan_2_1')</h2>
                  <p class="text-5 text-white mb-4">@lang('my-text.slogan_2_2')</p>
                  <p class="text-5 text-white mb-4">- @lang('my-text.slogan_2_3')</p>
                  <a href="{{auth()->check() ? route('invest') : route('register')}}" class="btn btn-primary mr-3">@lang('my-text.key_2')</a> 
				</div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- Slideshow end --> 

    <!-- Send Money
    ============================================= -->
    <section class="hero-wrap">
      <div class="hero-content d-flex flex-column fullscreen-with-header">
        <div class="container my-auto py-5">
		
          <!-- Style 5
          ============================================= -->
          <div class="row mb-5">
		  @foreach(getPlans() as $plan)
            <div class="col-sm-6 col-lg-3 mb-4">
              <div class="featured-box style-5 rounded">
                <div class="featured-box-icon text-info" style="font-size: 40px; margin: 30px 0px; color: #0062cc;"><strong>{{$plan->title}}</strong></div>
                <div class="featured-box-icon" style="font-size: 30px; margin: 20px 0px; color: #1e1d1c;"><strong>{{$plan->percent}}%</strong></div>
				<div class="text-center mt-4">
					<h3>@lang('my-text.key_33') {{$plan->day}} @lang('my-text.key_34')</h3><br>
					<h3>@lang('my-text.key_35') {{$plan->from_amount}}$</h3><br>
					<h3>@lang('my-text.key_36') {{$plan->to_amount}}$</h3><br>
				</div>
				<div class="text-center mt-4" style="margin-bottom: 1.5rem!important;"><a href="{{route('deposit')}}"><button type="button" class="btn btn-primary btn-sm" style="background-color: #0062cc;">@lang('my-text.key_41')</button></a></div>
              </div> 
			</div>
		  @endforeach
          </div>
          <!-- Style 5 end --> 
		
          <div class="row">
            <div class="col-lg-12 col-xl-12 my-auto">
              <div class="bg-white rounded shadow-md p-4">
				<input type="hidden" value="{{getJsonPlans()}}" name="js_plans" id="js_plans">
                <h3 class="text-5 mb-4 text-center">@lang('my-text.key_42')</h3>
                <hr class="mb-4 mx-n4">
                <form id="form-send-money" method="post">
					<div class="row">
					  <div class="col-lg-3 form-group">
						<label for="plan">@lang('my-text.key_28')</label>
						<select class="custom-select" name="plan" id="js-plan">
						  <option value selected>@lang('my-text.key_29')</option>
						  @foreach(getPlans() as $key => $plan)
  						  <div>
							<option value="{{++$key}}">
								{{$plan->title}}
							</option>
						  </div>
						  @endforeach
						</select>
					  </div>
					  <div class="col-lg-3 form-group">
						<label for="amount">@lang('my-text.key_30')</label>
						<div class="input-group">
						  <div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
						  <input type="number" id="js-amount" name="amount" class="form-control">
						</div>
					  </div>
					  <div class="col-lg-3 form-group">
						<label for="percent">@lang('my-text.key_31')</label>
						<div class="input-group">
						  <div class="input-group-prepend"> <span class="input-group-text">%</span> </div>
						  <input type="text" class="form-control" id="js-profit" readonly>
						</div>
					  </div>
					  <div class="col-lg-3 form-group">
						<label for="total">@lang('my-text.key_32')</label>
						<div class="input-group">
						  <div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
						  <input type="text" class="form-control" id="js-total" readonly>
						</div>
					  </div>
					</div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Send Money End --> 

    <!-- Why choose us
    ============================================= -->
    <section class="section bg-white" style="padding: 2.5rem 0;">
      <div class="container">
        <h2 class="text-9 text-center">@lang('my-text.key_263')</h2>
        <p class="lead text-center mb-5">@lang('my-text.our_mission')</p>
        <div class="row">
          <div class="col-md-6 mb-4 mb-md-0">
            <div class="hero-wrap section h-100 p-5 rounded">
              <div class="hero-mask rounded"></div>
              <div class="hero-bg rounded" style="background-image:url('{{ asset('components/images/bg/calc_bg.jpg') }}');"></div>
              <div class="hero-content">
                <h2 class="text-6 text-white mb-3">@lang('my-text.global_title')</h2>
                <p class="text-light mb-5">@lang('my-text.global_text')</p>
                <h2 class="text-6 text-white mb-3">@lang('my-text.pholosophy_title')</h2>
                <p class="text-light">@lang('my-text.pholosophy_text')</p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="featured-box style-1 mb-5 my-4">
              <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
              <h3>@lang('my-text.future_aqua')</h3>
            </div>
            <div class="featured-box style-1 mb-5">
              <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
              <h3>@lang('my-text.food_sector')</h3>
            </div>
            <div class="featured-box style-1 mb-5">
              <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
              <h3>@lang('my-text.ocean_conservation')</h3>
            </div>
            <div class="featured-box style-1 mb-5">
              <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
              <h3>@lang('my-text.food_security')</h3>
            </div>
            <div class="featured-box style-1 mb-5">
              <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
              <h3>@lang('my-text.new_jobs')</h3>
            </div>
            <div class="featured-box style-1 mb-5">
              <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
              <h3>@lang('my-text.excellent_team')</h3>
            </div>
          </div>
        </div>
		<p class="lead text-center mt-4"><a href="{{route('register')}}" class="btn btn-primary m-2">@lang('my-text.registration')</a></p>
      </div>
    </section>

    <!-- What can you do
    ============================================= -->
    <section class="section" style="padding: 2.5rem 0;">
      <div class="container">
        <h2 class="text-9 text-center mb-5">@lang('my-text.key_270')</h2>
        <div class="row">
          <div class="col-sm-6 col-lg-4 mb-4">
            <div class="featured-box style-5 rounded">
              <h3>@lang('my-text.key_271') 1 @lang('my-text.key_272')</h3>
              <div class="featured-box-icon text-primary"> <i class="fas fa-user"></i> </div>
              <div class="featured-box-icon text-primary"><span class="w-100 text-20 font-weight-500" style="color: #4a33c6; ">10%</span></div>
              <p class="text-3" style="background: #f1f5f6; padding: 8px 0; margin-bottom: 0px;">+ 10% @lang('my-text.key_273')</p>
            </div>
		  </div>
          <div class="col-sm-6 col-lg-4 mb-4">
            <div class="featured-box style-5 rounded" >
              <h3>@lang('my-text.key_271') 2 @lang('my-text.key_272')</h3>
              <div class="featured-box-icon text-primary"> <i class="fas fa-user-friends"></i> </div>
              <div class="featured-box-icon text-primary"><span class="w-100 text-20 font-weight-500" style="color: #4a33c6; ">5%</span></div>
              <p class="text-3" style="background: #f1f5f6; padding: 8px 0; margin-bottom: 0px;">+ 5% @lang('my-text.key_273')</p>
            </div>
		  </div>
          <div class="col-sm-6 col-lg-4 mb-4">
            <div class="featured-box style-5 rounded" >
              <h3>@lang('my-text.key_271') 3 @lang('my-text.key_272')</h3>
              <div class="featured-box-icon text-primary"> <i class="fas fa-users"></i> </div>
              <div class="featured-box-icon text-primary"><span class="w-100 text-20 font-weight-500" style="color: #4a33c6; ">2%</span></div>
              <p class="text-3" style="background: #f1f5f6; padding: 8px 0; margin-bottom: 0px;">+ 2% @lang('my-text.key_273')</p>
            </div>
		  </div>
        </div>
      </div>
    </section>
    <!-- What can you do end --> 

    <section class="section bg-white" style="padding: 2.5rem 0;">
    <div class="container">
      <h2 class="text-9 text-center">@lang('my-text.key_274')?</h2><br>
      <div class="row">
        <div class="col-xl-10 mx-auto">
          <div class="row">
            <div class="col-sm-6" style="padding-bottom: 2rem;">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-dollar-sign"></i> </div>
                <h3>@lang('my-text.key_275')</h3>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-file-signature"></i> </div>
                <h3>@lang('my-text.key_276')</h3>
              </div>
            </div>
            <div class="col-sm-6" style="padding-bottom: 2rem;">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-certificate"></i> </div>
                <h3>@lang('my-text.key_277')</h3>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-calendar-alt"></i> </div>
                <h3>@lang('my-text.key_278')</h3>
              </div>
            </div>
            <div class="col-sm-6" style="padding-bottom: 2rem;">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-shield-alt"></i> </div>
                <h3>@lang('my-text.key_279')</h3>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-user-secret"></i> </div>
                <h3>@lang('my-text.key_280')</h3>
              </div>
            </div>
            <div class="col-sm-6" style="padding-bottom: 2rem;">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-cubes"></i> </div>
                <h3>@lang('my-text.key_281')</h3>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="fas fa-headset"></i> </div>
                <h3>@lang('my-text.key_282')</h3>
              </div>
            </div>
          </div>
          <div class="text-center mt-4"><a href="{{route('register')}}" class="btn btn-outline-primary shadow-none text-uppercase">@lang('my-text.registration')</a></div>
        </div>
      </div>
    </div>
  </div>
  </section>
    
    <!-- Testimonial
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center">@lang('my-text.reviews_home')</h2>
        <div class="owl-carousel owl-theme" data-autoplay="true" data-nav="true" data-loop="true" data-margin="30" data-slideby="2" data-stagepadding="5" data-items-xs="1" data-items-sm="1" data-items-md="2" data-items-lg="2">

		  @foreach($reviews as $review)
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">"{{ $review->text }}"</p>
			  <span class="text-muted">{{ $review->author->name }}</span> 
			</div>
          </div>
		  @endforeach
        </div>
        <div class="text-center mt-4"><a href="{{ route('reviews') }}" class="btn-link text-4">@lang('my-text.more_review')<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
      </div>
    </section>
    <!-- Testimonial end --> 
    
  </div>
  <!-- Content end --> 


@endsection
