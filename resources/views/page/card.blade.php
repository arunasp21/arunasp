@extends('welcome')

@section('content')

    @include('partial.second_header')

    @include('partial.components._investment')

    <div class="tpl_wrap clearfix card-page">
        <div class="container">
            <div class="card-page__wrapper">
                <div class="card-page__column">
                    <img class="card-page__img" src="{{ asset('components/img/card-page__img.png') }}" alt="">
                    <ul class="card-page__list">
                        <li class="card-page__item">Банковская карта с балансом аккаунта <br>
                            Баланс аккаунта в личном кабинете - равен бонусу карты                    </li>
                        <li class="card-page__item">Оплата в магазинах</li>
                        <li class="card-page__item">Снятие наличных в банкоматах по всему миру - 1.5% комиссия</li>
                        <li class="card-page__item">Бесплатное годовое обслуживание</li>
                        <li class="card-page__item">Доставка почтой России или курьером <br> <b>Доставка карты за 7 дней</b></li>
                        <li class="card-page__item">Перевод электронных денежных средств - ООО “банк раунд”.</li>
                    </ul>
                    <button class="card-page__btn">Активировать карту</button>
                </div>
                <div class="card-page__column">
                    <form action="/new_card.php" class="card-page__form" method="post">
                        <label for="name" class="card-page__label">Ф.И.О</label>
                        <input id="name" name="name" type="text" class="card-page__input" value required>
                        <label for="email" class="card-page__label">E-Mail</label>
                        <input id="email" name="email" type="email" class="card-page__input" value required>
                        <label for="country" class="card-page__label">Страна</label>
                        <input id="country" name="country" type="text" class="card-page__input" value required>
                        <label for="city" class="card-page__label">Город</label>
                        <input id="city" name="city" type="text" class="card-page__input" value required>
                        <label for="adress" class="card-page__label">Улица/дом/квартира</label>
                        <input id="adress" name="address" type="text" class="card-page__input" value required>
                        <label for="index" class="card-page__label">Индекс</label>
                        <input id="index" name="postal_code" type="text" class="card-page__input" value required>
                        <label for="phone" class="card-page__label">Номер телефона</label>
                        <input id="phone" name="phone" type="tel" class="card-page__input" value required>
                        <input class="card-page__btn" type="submit" value="Заказать карту">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection