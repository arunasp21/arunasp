@extends('welcome')

@section('content')

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

    @include('partial.first_header')

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light py-5" style="background-image:url('{{ asset('components/images/bg/bg1.jpg') }}');">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="index.html">Home</a></li>
          <li class="active">Contacts</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>Contacts</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 

  <!-- Content
  ============================================= -->
  <div id="content">
    
  <div class="container">
    <div class="row">
      <div class="col-md-4 mb-4">
        <div class="bg-white shadow-md rounded h-100 p-3">
          <div class="featured-box text-center">
            <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-map-marker-alt"></i></div>
            <h3>@lang('my-text.key_86')</h3>
            <p>Nieuwegracht 73<br>
              3512 LS  Utrecht<br>
              The Netherlands<br>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-4">
        <div class="bg-white shadow-md rounded h-100 p-3">
          <div class="featured-box text-center">
            <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-phone"></i> </div>
            <h3>@lang('my-text.key_76')</h3>
            <p class="mb-0">+31 (0)30 7467835</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-4">
        <div class="bg-white shadow-md rounded h-100 p-3">
          <div class="featured-box text-center">
            <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-envelope"></i> </div>
            <h3>@lang('my-text.key_77')</h3>
            <p>support@arunasp.com</p>
          </div>
        </div>
      </div>
    </div>
  </div>	  <a name="support"></a>
  <section class="hero-wrap section shadow-md" >    <div class="hero-mask opacity-9 bg-primary"></div>
    <div class="hero-bg" style="background-image:url('images/bg/image-2.jpg');"></div>
    <div class="hero-content">
      <div class="container text-center">
        <h2 class="text-9 text-white">@lang('my-text.key_80')</h2>
        <p class="text-4 text-white mb-4">@lang('my-text.key_81')</p>
		<form method="post" action="{{ route('send.question.mail') }}">
			@csrf
			<div class="row">
				<div class="col-lg-6 form-group">
					<label for="user_name">@lang('my-text.key_82')</label>
					<input id="user_name" type="text" name="login" class="form-control" value required>
				</div>
				<div class="col-lg-6 form-group">
					<label for="user_email">@lang('my-text.key_83')</label>
					<input id="user_email" type="text" name="email" value class="form-control" required>
				</div>
			</div>
            <div class="form-group">
				<label for="user_email">@lang('my-text.key_84')</label>
				<textarea id="user_massage" name="message" class="form-control" cols="23" rows="4" maxlength="500"></textarea>
			</div>
            <div class="form-group">
				<input type="submit" value="{{__('my-text.key_85')}}" class="btn btn-light">
			</div>
		</form>
    </div>
  </section>

@endsection
