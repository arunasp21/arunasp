@extends('welcome')

@section('content')

  @include('partial.first_header')

  <!-- Content
  ============================================= -->
  <div id="content">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-9 col-lg-7 col-xl-5 mx-auto">
          <div class="bg-white shadow-md rounded p-3 pt-sm-4 pb-sm-5 px-sm-5">
            <h3 class="font-weight-400 text-center mb-4">@lang('my-text.key_420')?</h3>
            <hr class="mx-n5">
            <form method="POST" action="{{ route('password.update') }}">
              @csrf
			  <input type="hidden" name="token" value="{{ $token }}">
              <div class="form-group">
                <label for="email">@lang('my-text.key_422')</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
				@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
              </div>
              <div class="form-group">
                <label for="password">@lang('my-text.key_325')</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
				@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
              </div>
              <div class="form-group">
                <label for="password-confirm">@lang('my-text.key_324')</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
              </div>
              <button class="btn btn-primary btn-block my-4" type="submit">{{__('my-text.key_421')}}</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Content end --> 

@endsection
