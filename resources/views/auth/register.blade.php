@extends('welcome')

@section('content')

	@include('partial.first_header')

	<!-- Content
	============================================= -->
	<div id="content">
	<div class="container py-5">
	  <div class="row">
		<div class="col-md-9 col-lg-7 col-xl-5 mx-auto">
		  <div class="bg-white shadow-md rounded p-3 pt-sm-4 pb-sm-5 px-sm-5">
			<h3 class="font-weight-400 text-center mb-4">@lang('my-text.key_389')</h3>
			<hr class="mx-n4">
			<form action="{{ route('register') }}" method="post">
  			  @csrf
			  @if (isset($_GET['ref']))
				<input type="hidden" name="invited_it" value="{{$_GET['ref']}}">
			  @endif
			  <div class="form-group">
				<label for="name">@lang('my-text.key_391')</label>
				<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
				@if ($errors->has('name'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
				@endif
			  </div>
<!--
			  <div class="form-group">
				<label for="name">@lang('my-text.key_393')</label>
				<input id="nickname" type="text" class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}" name="nickname" value="{{ old('skype') }}">
				@if ($errors->has('skype'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('skype') }}</strong>
					</span>
				@endif
			  </div>
-->			  
			  <div class="form-group">
				<label for="email">@lang('my-text.key_395')</label>
				<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
				@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
			  </div>
			  <div class="form-group">
				<label for="email-confirm">@lang('my-text.key_397')</label>
				<input id="email-confirm" type="email" class="form-control" name="email_confirmation" required>
			  </div>
			  <div class="form-group">
				<label for="password">@lang('my-text.key_400')</label>
				<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
				@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
			  </div>
			  <div class="form-group">
				<label for="password">@lang('my-text.key_402')</label>
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
			  </div>
 			  <div class="form-group">
				  <div class="form-check custom-control custom-checkbox">
					<input type="checkbox" id="agree1" name="rules" value="1" class="custom-control-input{{ $errors->has('rules') ? ' is-invalid' : '' }}">
					<label class="custom-control-label" for="agree1">@lang('my-text.key_404') <a href="{{ route('rules') }}">@lang('my-text.key_405')</a></label>
					@if ($errors->has('rules'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('rules') }}</strong>
						</span>
					@endif
				  </div>
			  </div>
 			  <div class="form-group">
				  <div class="form-check custom-control custom-checkbox">
					<input type="checkbox" id="bonus" name="bonus" value="1" class="custom-control-input{{ $errors->has('bonus') ? ' is-invalid' : '' }}">
					<label class="custom-control-label" for="bonus">Получить приветственный бонус</label>
					@if ($errors->has('bonus'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('bonus') }}</strong>
						</span>
					@endif
				  </div>
			  </div>
			  <button class="btn btn-primary btn-block my-4" type="submit">Зарегистрироваться</button>
			</form>
			<p class="text-3 text-muted text-center mb-0">У Вас уже есть акааунт? <a class="btn-link" href="{{route('login')}}">Войдите</a></p>
		  </div>
		</div>
	  </div>
	</div>
	</div>
	<!-- Content end --> 

@endsection
