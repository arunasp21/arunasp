@extends('welcome')

@section('content')

	@include('partial.first_header')

	<!-- Content
	============================================= -->
	<div id="content">
	<div class="container py-5">
	  <div class="row">
		<div class="col-md-10 col-lg-8 col-xl-6 mx-auto">
		  <div class="bg-white shadow-md rounded p-3 pt-sm-4 pb-sm-5 px-sm-5">
			<h3 class="font-weight-400 text-center mb-4">Для продолжения активируйте аккаунт по ссылке, которая отправлена вам на почту!</h3>
		  </div>
		</div>
	  </div>
	</div>
	</div>
	<!-- Content end --> 

@endsection
