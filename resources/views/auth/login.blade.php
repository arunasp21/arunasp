@extends('welcome')

@section('content')

  @include('partial.first_header')

  <!-- Content
  ============================================= -->
  <div id="content">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-9 col-lg-7 col-xl-5 mx-auto">
          <div class="bg-white shadow-md rounded p-3 pt-sm-4 pb-sm-5 px-sm-5">
            <h3 class="font-weight-400 text-center mb-4">@lang('my-text.key_407')</h3>
            <hr class="mx-n5">
            <p class="lead text-center">@lang('my-text.we_glad')</p>
            <form method="POST" action="{{ route('login') }}">
              @csrf
              <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
              </div>
              <div class="form-group">
                <label for="password">@lang('my-text.key_412')</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
              </div>
              <div class="row">
                <div class="col-sm text-right"><a class="btn-link" href="{{ url('password/reset') }}">@lang('my-text.key_413')?</a></div>
              </div>
              <button class="btn btn-primary btn-block my-4" type="submit">@lang('my-text.key_407')</button>
            </form>
            <p class="text-3 text-muted text-center mb-0">@lang('my-text.not_account') <a class="btn-link" href="{{route('register')}}">@lang('my-text.registration')</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Content end --> 

@endsection
