<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
		<link href="images/favicon.png" rel="icon" />
		<title>{{ $title ?? 'ArunaSP' }}</title>

		<!-- Web Fonts
		============================================= -->
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

		<!-- Stylesheet
		============================================= -->
		<link rel="stylesheet" type="text/css" href="{{ asset('components/vendor/bootstrap/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('components/vendor/font-awesome/css/all.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('components/vendor/owl.carousel/assets/owl.carousel.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('components/css/stylesheet.css') }}" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-214565091-1">
		</script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-214565091-1');
		</script>
	</head>
	
    <body>

    @yield('content')

    @include('partial.footer')

	<script src="{{ asset('components/vendor/jquery/jquery.min.js') }}"></script> 
	<script src="{{ asset('components/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script> 
	<script src="{{ asset('components/vendor/owl.carousel/owl.carousel.min.js') }}"></script> 
	<script src="{{ asset('components/js/theme.js') }}"></script> 
	<script src="{{ asset('components/js/common00.js') }}"></script> 
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>

        function showMyModal(flag) {
            if (flag){
                document.getElementById('exampleModal').classList.remove('fade');
                document.getElementById('exampleModal').classList.add('show-modal');
                document.getElementById('my-overlay').classList.add('show-modal');
            } else {
                document.getElementById('exampleModal').classList.add('fade');
                document.getElementById('exampleModal').classList.remove('show-modal');
                document.getElementById('my-overlay').classList.remove('show-modal');
            }
        }

        function validation() {
            let field = 0;

            if (document.getElementById('recipient-name').value.length > 0){
                field++;
            }
            if (document.getElementById('message-text').value.length > 0){
                field++;
            }

            if (field == 2){
                document.getElementById('modal-form').submit()
            } else {
                document.getElementById('recipient-name').style.border = '1px red solid';
                document.getElementById('message-text').style.border = '1px red solid';
            }
        }

	$(document).ready(function(){
		<? if(session('status')){ ?>
				Swal.fire({
					icon: 'success',
					text: "{{ __('my-text.password_reset') }}",
					confirmButtonColor: "#66BB6A"
				}).then((result) => {
					window.location.href = "{{url('/')}}";
				});
		<? } ?>
	});

    </script>
    </body>
</html>
