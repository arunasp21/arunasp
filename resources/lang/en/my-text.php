<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

	/* My Langs */

	'upload_avatar' => 'Upload avatar',
	'phone_number' => 'Your phone number:',

	/* End My Langs */

    'head_menu_1' => 'Home',
    'head_menu_2' => 'About Us',
    'head_menu_3' => 'About the company',
    'head_menu_4' => 'Legal documents',
    'head_menu_5' => 'News',
    'head_menu_6' => 'How to start?',
    'head_menu_7' => 'FAQ',
    'head_menu_8' => 'Rules',
    'head_menu_9' => 'Reviews',
    'head_menu_10' => 'Contacts',
    'head_menu_11' => 'Anti-SPAM agreement',
    'head_menu_12' => 'Privacy policy',

    'account' => 'My account',
    'registration' => 'Registration',
    'login' => 'Login',
    'exit' => 'Exit',

    'key_1' => 'Club of successful investors',
    'key_2' => 'Join now!',
    'key_3' => 'Call center',
    'key_4' => 'If you still have questions, you can call us and contact us in any convenient way through the Contacts section',
    'key_5' => 'Fill in the registration data and become a member of Cab Solutions.',
    'key_6' => 'By going to this section you can find detailed information about the company’s activities, its history and achievements.',
    'key_7' => 'A detailed description of the initial actions, for those who want to become an investor and earn money with Cab Solutions.',
    'key_8' => 'Frequently Asked Questions',
    'key_9' => 'The section contains answers to the most frequently asked questions about the company and the operation of the Cab Solutions platform.',
    'key_10' => 'Terms of use',
    'key_11' => 'Code of rules for using the platform for online investment Cab Solutions.',
    'key_12' => 'Contacts',
    'key_13' => 'Company contacts. Contact us. We are always happy to answer your questions.',
    'key_14' => 'Info tour',
    'key_15' => 'You can always return to the tour by clicking on this button',
    'key_16' => 'Investment calculator',
    'key_17' => 'You can use the investment calculator to calculate profit.',
    'key_18' => 'Investment plan',
    'key_19' => 'Select the investment plan you are interested in.',
    'key_20' => 'Deposit amount',
    'key_21' => 'Enter the deposit amount you want to invest.',
    'key_22' => 'Total percentage of profit',
    'key_23' => 'The system will automatically calculate the total percentage of profit for the selected investment plan.',
    'key_24' => 'Total amount of profit',
    'key_25' => 'The total amount of profit in numerical terms for the selected investment plan.',
    'key_26' => 'Investment plans',
    'key_27' => 'Cab Solutions platform investment plans. Use the arrow icons to scroll.',
    'key_28' => 'Select a plan:',
    'key_29' => 'Select a plan',
    'key_30' => 'Deposit amount:',
    'key_31' => 'Accrued interest:',
    'key_32' => 'Total amount:',
    'key_33' => 'Daily during',
    'key_34' => 'days',
    'key_35' => 'Minimum:',
    'key_36' => 'Maximum:',
    'key_37' => 'Contribution',
    'key_38' => 'at the end of the term',
    'key_39' => 'Calendar',
    'key_40' => 'days',
    'key_41' => 'Сontribution',
    'key_42' => 'Calculator',
    'key_43' => 'Show plans',
    'key_44' => 'Open account',
    'key_45' => 'Not a member yet?',
    'key_46' => 'For registered members',
    'key_47' => 'Login',
    'key_48' => 'All rights reserved',
    'key_49' => 'Hello',
    'key_50' => 'Top up',
    'key_51' => 'Make a contribution',
    'key_52' => 'Withdraw funds',
    'key_53' => 'Your profile',
    'key_54' => 'Deposit History',
    'key_55' => 'Top-up history',
    'key_56' => 'Output history',
    'key_57' => 'Your partners',
    'key_58' => 'Settings',
    'key_59' => 'Revenue History',
    'key_60' => 'Log out of the account.',
    'key_61' => 'Account settings. Personal information, payment details.',
    'key_62' => 'Your affiliate network. Statistics, list of partners and your affiliate link.',
    'key_63' => 'Information about payments to the investor’s electronic wallet.',
    'key_64' => 'Information about the accruals to the balance of your account.',
    'key_65' => 'Information about your deposits in chronological order.',
    'key_66' => 'Click on the button to order a payment',
    'key_67' => 'Click on the button to open a deposit.',
    'key_68' => 'Click on the button to fund the account.',
    'key_69' => 'Admin panel',
    'key_70' => 'Plans',
    'key_71' => 'Reviews',
    'key_72' => 'Users',
    'key_73' => 'Statistics',
    'key_74' => 'Recent investment',
    'key_75' => 'Recent Payments',
    'key_76' => 'Phone:',
    'key_77' => 'Email:',
    'key_78' => 'Legal documents',
    'key_79' => 'Click here',
    'key_80' => 'Email us',
    'key_81' => 'In order to contact a representative of our company and raise a question, you can use the contact details provided below. The management of ArunaSP does not leave a single appeal without attention!',
    'key_82' => 'Your login:',
    'key_83' => 'Your Email:',
    'key_84' => 'Your message:',
    'key_85' => 'Submit',
    'key_86' => 'Address:',
    'key_87' => 'Amount',
    'key_88' => 'User',
    'key_89' => 'Recent payouts',
    'key_90' => 'Recent investment',
    'key_91' => 'REGISTRATION IN THE SYSTEM',
    'key_92' => 'To participate in the Cab Solutions system you need to go through a simple registration procedure on the site. Registration in the system is free and takes only a few minutes. After that, you will become an official participant and will be able to complete the operations specified in the rules. In order to register yourself as a Cab Solutions user, click on the “Register” button on the main page of the site, after which you will be redirected to the page with the registration form. Fill in all the necessary information and click the "Send" button so that you receive SMS notifications to confirm your identity. Please note that by agreeing to the Terms of Use when registering, you automatically confirm that you are an adult in the country of which you are a citizen and using this website does not violate any laws of your state.',
    'key_93' => 'OPEN DEPOSIT',
    'key_94' => 'To open a deposit in the Cab Solutions system, log in to your personal account using the registration data. On the main page of your personal account, click on the "Deposit" button, then select a payment system, enter the amount of investment. After that, click on the "Refill" button, which is located at the very bottom of the page. You will be redirected to the site of the payment system. After you complete the transaction, you will be redirected to the website of our company. After that, click on the “Make a Deposit” button, select a tariff plan and indicate the deposit amount below. Then click on the “Make a new deposit” button. Your deposit will be activated and credited to the investment plan that you have chosen. Next to the icon of each investment plan is a profit calculator that helps you calculate your profit.',
    'key_95' => 'RECEIVING PROFIT',
    'key_96' => 'The percentage of profit from your deposit will be automatically credited to your account balance in
                                according to the investment plan that you have chosen. Your income depends on the chosen
                                investment plan, interest may accrue daily or at the end of the deposit term. If
                                you chose daily plans, you will receive accruals daily on business days. For
                                in order to calculate your income, use the investment calculator.',
    'key_97' => 'WITHDRAW FUNDS',
    'key_98' => 'In order to withdraw your income or affiliate accruals you need to create a request for
                                withdrawal of funds to your electronic wallet. Log in to your personal account and click on the button.
                                "Withdraw funds." Fill out the withdrawal request form and click on the "Withdraw funds" button.
                                Your request for withdrawal of funds will be processed by our operators as soon as possible, in
                                Depending on the availability of our support staff, this can take up to 48 hours. IN
                                rare cases (force majeure, technical malfunctions, suspected fraud) processing
                                Payment may take more than 48 hours. There are no transfer commissions.',
    'key_99' => 'Reviews',
    'key_100' => 'Leave a review',
    'key_101' => 'Submit',
    'key_102' => 'You must be logged in to add a review!',
    'key_103' => 'Login',
    'key_104' => 'Privacy Policy',
    'key_105' => "This agreement describes the interaction between the personal data of users (collection procedures,
                    use, sorting information that we receive from you) and the company's information center
                   Cab Solutions.",
    'key_106' => 'What Affects the Privacy Policy',
    'key_107' => 'This privacy policy discusses the "personal information" that Cab Solutions receives from you when you use the http://www.cab-solutions.club web resource. In this document
                    the term "personal information" means any information that is associated with your name. Such
                    Information may include: your email address, payment system data, date
                    birth (if available), phone number (if available) or other information that you
                    provide in your personal profile.',
    'key_108' => 'This privacy policy does not apply to the information you provide to
                    other websites, even if we contact you on these websites. Information posted by you in
                    social networks (Instagram, Facebook, Twitter, YouTube, VK, etc.) are governed by the policy
                    privacy of these websites and is not applicable to this privacy policy.',
    'key_109' => 'Please note that we do not knowingly collect personal information from people under the age of 18.
                    If you are under the age of 18, you can view pages on our sites, but you should not
                    register, use or provide us with personal information.',
    'key_110' => 'Use of personal information of users',
    'key_111' => "First, the solution center of the company Cab Solutions uses the collected
                    data for timely acquaintance with news, additional information and our services
                    company. The company's specialists also have the right to use your data to contact you in
                    advertising purposes. Customer’s personal information may be used for other legitimate purposes.
                    business, including for identifying and preventing fraud, resolving contentious issues and for
                    maintaining business relations.",
    'key_112' => 'Cab Solutions reserves the right to share summary information (i.e.,
                    information that is not directly related to your name) if such information is anonymized and
                    compiled into a summary report.',
    'key_113' => 'Protecting user information',
    'key_114' => 'We are aware that our customers trust us with their personal information. We take protection seriously
                    information and support physical, electronic and procedural safeguards to protect your
                    personal data.',
    'key_115' => 'Interaction',
    'key_116' => 'If you have any questions regarding the confidentiality and security of your data or to you
                    I would like to request access to your account or make corrections to your personal data, you can
                    contact us in any way possible on the page "Contacts".',
    'key_117' => 'The company reserves the right to take reasonable measures to verify your identity before
                    providing access or making corrections. If you urgently need to contact you at
                    in relation to any event that includes your personal information, we can do this
                    using your personal data.',
    'key_118' => "Cab Solutions reserves the right to modify or modify any of
                    conditions contained in this document. Cab Solutions reserves
                    the right from time to time, without notice and at its discretion, to edit
                    this agreement. Further use of the site automatically means acceptance of such changes.
                    or modifications by you. If you do not agree with these changes or modifications, you must
                    stop using the company's web resources immediately.",
    'key_119' => 'News',
    'key_120' => 'Problems opening pages?',
    'key_121' => 'Some users may have a problem & nbsp; with opening one of the pages.',
    'key_122' => 'The solution is simple, clean the history and browser cache, the problem will instantly disappear.',
    'key_123' => 'Regards, Cab Solutions Team',
    'key_124' => 'Mail client MAIL.RU and not only',
    'key_125' => 'MAIL.RU mail client and all child domains!',
    'key_126' => 'Due to disagreement with this & nbsp; mail provider, letters for users with
                            so ',
    'key_127' => 'may not be delivered by postal address in order to avoid the difficulty of receiving answers & nbsp; we
                            we recommend using GMAIL.COM mail, etc.',
    'key_128' => 'Regards, Cab Solutions Team',
    'key_129' => 'The answer to the frequently asked question',
    'key_130' => 'The answer is NO, we urge all participants to read the FAQ section.',
    'key_131' => 'We try to get better for you!',
    'key_132' => 'Sincerely, the administration of Cab Solutions',
    'key_133' => 'Reviews',
    'key_134' => 'By popular demand of participants, a section has been added where you can leave your feedback, and
                            So
                            same payment screens and video reviews!',
    'key_135' => 'SSL certificate for advanced verification',
    'key_136' => 'Cab Solutions Enhanced Data Security Between Client and Website
                            Cab Solutions to banking security standards.',
    'key_137' => 'Licenses EV protocol SSL certificate indicates successful verification in our company
                            organized by a certification authority. You can view all the information by clicking on the green
                            panel
                            in the address bar of the browser.',
    'key_138' => 'Using this type of certificate is a guarantee of the security of data transmitted from
                            a guarantee of $ 750,000.',
    'key_139' => 'Legal documents',
    'key_140' => 'The certificate of registration of a private limited company is legal
                        document regarding the establishment of Cab Solutions. Investment
                        Cab Solutions online platform is the property of Cab Solutions. In the United Kingdom, a certificate of registration forms the main component
                        part of the constitutional documents of incorporation. This document officially confirms
                        company existence. You can check it on the government website',
    'key_141' => 'COMPANY REGISTRATION CERTIFICATE',
    'key_142' => 'Cab Solutions is an independent division of Cab Solutions,
                                which is governed by UK law. Cab Solutions
                                officially registered with the reference number 10365403. This information may
                                be verified on the United Kingdom Company House Authority state website. Cab Solutions
                                registered in England No. 10365403.',
    'key_143' => 'RISK INSURANCE',
    'key_144' => 'All investments carry an element of risk, which can vary significantly. Our mission
                    It is to ensure maximum profit for all our customers, while maintaining a minimum
                    possible risks. The company has its own stabilization fund. Every investor
                    insured. If you are not sure about the advisability of investing, you should apply for
                    professional independent financial advice. Taxation, laws and regulations of your
                    countries are subject to change without notice in the future, therefore responsibility for
                    taxation is completely dependent on your personal circumstances and your legislation
                    countries.',
    'key_145' => 'About the company',
    'key_146' => 'Cab Solutions was founded on December 16, 2009 in England',
    'key_147' => 'Due to the large number of people and visiting tourists in the United Kingdom, taxi services are in great demand. To keep up with the times we are constantly improving and developing in our segment of the business.',
    'key_148' => 'The demand for our services is so great that we can hardly satisfy it, therefore we attract additional investments.
                        The finances of our investors are spent on the modernization of our taxi fleet, which helps us save huge amounts of money, and as you know, the money saved is the money earned.',
    'key_149' => 'By modernization is meant updating the taxi fleet with special cabs - electric cars. Our drivers work in shifts to ensure maximum profitability.',
    'key_150' => 'It takes just over an hour to charge an electric car thanks to special Superfast Charging cables, which makes our cars simple to use, and the alternative fuel savings are just overwhelming.',
    'key_151' => 'Consider the feasibility of an investment using this example:',
    'key_152' => '1 liter of gasoline costs 1.26 pounds, which is equal to 109 rubles, for 2 shifts (day / night) two drivers drive an average of more than 800 km and spend about 90 liters of gasoline, which is equal to = 10 thousand rubles.
                        800 km on an electric car spend about 80 kW of electricity at a cost of 0.15 pence per kW, which is 1036 rubles.',
    'key_153' => 'Our fleet has more than 400 cars that transport thousands of people every day across London and other cities.
                            That is, 400 cars spend about 9,000,000 rubles per day only on gasoline, not to mention the depreciation costs and the cost of parts and services, if you use electric cars, the savings are simply colossal !!!',
    'key_154' => 'Anti-Spam Agreement',
    'key_155' => "The use of Cab Solutions's http://www.cab-solutions.club/ website is subject to
                    this policy. If you have been found to be in violation of these rules, Cab Solutions reserves the right, at its discretion, to warn you at any time,
                    pause or block your account. Please note that in
                    in accordance with the Terms of Use, we can change certain elements of this agreement to any
                    time.",
    'key_156' => 'Cab Solutions maintains a zero tolerance policy on spam
                    / UCE (Unsolicited Commercial Emails). This means that all recipients of your
                    emails must give legal consent to receive messages from you.',
    'key_157' => 'You are responsible for ensuring that your emails and promotional materials do not cause
                    numerous spam complaints. If Cab Solutions Technical Department accepts
                    The decision is that your spam complaint rate or bounce rate is higher than industry average
                    performance then Cab Solutions reserves the right to suspend or terminate
                    Your account. If you get low response rates to emails or high
                    bounce rates, we may request additional information about your mailing list so that
                    examine it and try to solve the problem.',
    'key_158' => 'You are responsible for posting false reviews or misinformation in public forums or
                    any rating sites.',
    'key_159' => 'Any violation of this agreement as a whole or of individual elements may lead to permanent
                    suspension of the account.',
    'key_160' => 'Terms of Use',
    'key_161' => 'Please read the following rules carefully before registering',
    'key_162' => 'General',
    'key_163' => 'The project withdrawal commission is 0% of the amount withdrawn by the user',
    'key_164' => 'Withdrawal of funds is possible only after processing your previous application',
    'key_165' => 'By registering in our system, you agree to these rules in full',
    'key_166' => 'The administration is not responsible for any damage caused to you as a result
                        use of this System',
    'key_167' => 'At the time of registration, the user must be at least 18 years old from the date of his birth',
    'key_168' => 'Our service does not conduct any secondary activities with the investments of its users and does not
                        exposes them to appropriate risks',
    'key_169' => 'Rights and obligations of the user',
    'key_170' => 'To deposit money into your balance',
    'key_171' => 'To inform and attract new participants to the system by various advertising
                        methods: sites, forum topics, social networks, etc.',
    'key_172' => 'Send your wishes and feedback to the System in order to improve the service of the System',
    'key_173' => 'Follow these rules in full',
    'key_174' => 'Carefully read the terms of crediting and payments',
    'key_175' => 'Do not mislead the System Administration by providing false
                        information',
    'key_176' => 'At least once every three days, again review these rules',
    'key_177' => 'If you find faults or some errors in the site script, report to
                        Customer Support',
    'key_178' => 'Do not attempt to hack a site or use possible errors in scripts. Upon attempt
                        hacking, the administration has the full right to remove, block or fine the user',
    'key_179' => 'Do not publish offensive messages, slander, or other types of messages that damage your reputation
                        System or its users',
    'key_180' => 'Do not create chains of 2 or more consecutive registrations with the aim of
                        repeated receipt of ref. bonuses from each of the payments. If such chains are found,
                        the administration has the full right to remove, block or fine the user',
    'key_181' => 'Rights and obligations of the administration',
    'key_182' => 'If users ignore these rules, the administration reserves the right to
                        delete, block or fine user account without warning and without explanation
                        reasons',
    'key_183' => 'The administration may make changes to these rules without warning users',
    'key_184' => 'If the user in the registration wrote false data, then the administration has full right
                        remove, block or fine him without warning and do not pay money
                        before correcting the data in the profile',
    'key_185' => 'Letters sent to administrations that have obscene content that are offensive
                        nature or threats - will be ignored and users deleted',
    'key_186' => 'If you try to mislead or deceive the administration, removal measures will be taken,
                        account blocking or penalty',
    'key_187' => 'The administration undertakes to maintain the confidentiality of information registered
                        user received from him during registration',
    'key_188' => 'The administration is not responsible for a possible hacking of accounts. To avoid hacking
                        need to use complex passwords',
    'key_189' => 'The administration is not responsible for possible losses resulting from the collapse of the System',
    'key_190' => 'The administration has the right to restart the service with resetting any credentials
                        upon reaching the zero balance of the System’s account',
    'key_191' => 'In case of refusal to accept the new rules, the administration reserves the right to refuse
                        user in further participation in our service',
    'key_192' => 'Official news related to the project are only news published on
                        this site',
    'key_193' => 'Payments',
    'key_194' => 'Users can make a deposit to the amount of',
    'key_195' => 'when
                        help of the following payment systems',
    'key_196' => 'The minimum withdrawal amount after replenishing the balance from the system is',
    'key_197' => 'Some payment directions may have separate
                        conditions for the minimum amount of withdrawal from the System and the maintenance procedure',
    'key_198' => 'The list of payment systems with which you can make deposits and withdrawals can
                        adjusted by the administration',
    'key_199' => 'The withdrawal of funds is instant. In the case of inaccurate, incomplete data on
                        withdrawal system by the client, the withdrawal of funds does not occur, and the withdrawal request is rejected until
                        Correction or clarification of all inaccuracies and data',
    'key_200' => 'Applications for withdrawing funds are accepted around the clock',
    'key_201' => 'Responsibility',
    'key_202' => 'The Administration does not send Users unreasonable electronic or other
                        messages (spam)',
    'key_203' => 'The administration is not responsible for temporary technical failures and interruptions in work
                        Project, for temporary failures and interruptions in the operation of communication lines, other similar failures, as well as for
                        problems of the computer from which the User accesses the Internet',
    'key_204' => 'The administration is not responsible to the User for the actions of other Users',
    'key_205' => 'By registering, you agree to the terms of this agreement',
    'key_206' => 'All possible complaints and feedback on the functioning of the System must be directed to
                        project’s official email address',
    'key_207' => 'This site is not intended for any tax, legal, insurance or investment
                        consultation, and nothing on this site should be construed as a recommendation by us or any
                        a third party to acquire or dispose of any investment',
    'key_208' => 'If you do not agree with the rules, please do not register. We are not forcing anyone or
                        we give no guarantees and promises',
    'key_209' => 'KEY ISSUES',
    'key_210' => 'PARTNERSHIP PROGRAM',
    'key_211' => 'FINANCIAL MATTERS',
    'key_212' => 'OTHER',
    'key_213' => 'KEY ISSUES',
    'key_214' => 'How to remove the restriction on output?',
    'key_215' => 'In order to remove restrictions on the withdrawal, you need to replenish the balance to any
                                    desired amount minimum amount is',
    'key_216' => 'How to register on the company website',
    'key_217' => 'To register, click "Register" and fill out all the necessary
                                    fields',
    'key_218' => 'How to access an account',
    'key_219' => 'In order to access your account, you must log in to your account by specifying
                                    login and password received during registration',
    'key_220' => 'Can I be sure of the security of my data',
    'key_221' => 'Yes, the company guarantees our investors the protection and security of personal data. we
                                    We do not transfer personal data of clients to third parties! Our site is protected from various types
                                    attacks, and all transmitted data is encrypted',
    'key_222' => 'Who can become a member of the system',
    'key_223' => 'Anyone who has reached the age of majority can become a member of the system',
    'key_224' => 'How many accounts can I create',
    'key_225' => 'You can create only one account in the system. If this condition is violated,
                                    site administration will take measures',
    'key_226' => 'How to open a deposit',
    'key_227' => 'In order to open a deposit, register, log in to your personal account and
                                    replenish the account',
    'key_228' => 'How many deposits can be opened',
    'key_229' => 'There are no restrictions on the number of deposits created',
    'key_230' => 'Can I change the data in my account',
    'key_231' => 'Yes. To do this, log in to your personal account and select the appropriate section. Save
                                    new data. If you can’t change something, please contact our support team',
    'key_232' => 'I cannot log in to my account',
    'key_233' => 'In this case, check if you entered the data correctly. If the data entered is correct,
                                    clear the cache and try again. If the problem persists, write to the service
                                    support',
    'key_234' => 'PARTNER PROGRAM',
    'key_235' => 'What are the benefits of participating in an affiliate program',
    'key_236' => 'The affiliate program is a great opportunity for every investor who wants to
                                    make extra profit. You don’t need to have an active deposit',
    'key_237' => 'What to do with referral deductions received from the invite bonus',
    'key_238' => 'In order to withdraw the referral received from the bonus when registering invitees
                                    participants, it is necessary that invited participants replenish the balance with personal
                                    means',
    'key_239' => 'Do you have an affiliate program',
    'key_240' => 'Yes, we have prepared a generous affiliate program for you: 10%, 5%, 2%. Note,
                                You can check the balance at any time by logging into your personal account    you will receive profit from the partner only if he went to our website
                                    by your referral link, registered and created a deposit through replenishment
                                    accounts',
    'key_241' => 'Do I need a deposit to participate in the affiliate program',
    'key_242' => 'No, it is not necessary to have an active deposit. All you have to do is distribute your
                                    referral links by any means available to you',
    'key_243' => 'How to become a member of the affiliate program',
    'key_244' => 'In your account you will find your referral link. Distribute it with all available
                                    ways for you and invite people. New users will go to our site
                                    at your link and register. After that, they will automatically be assigned to you. With
                                    all replenishment of the account that they carry out, you will receive affiliate
                                    reward',
    'key_245' => 'How many accounts can I create',
    'key_246' => 'You can create only one account in the system. If this condition is violated,
                                    site administration will take measures',
    'key_247' => 'How many deposits can be opened',
    'key_229' => 'There are no restrictions on the number of deposits created',
    'key_248' => 'An affiliate link is needed so that with its help you invite new investors to
                                    system, and it, in turn, automatically determined that they came from
                                    you',
    'key_249' => 'What you need to know about accruing affiliate rewards',
    'key_250' => 'Affiliate rewards will be received every time your partner replenishes an account.
                                    Enrollment comes instantly',
    'key_251' => 'How to track engagement results',
    'key_252' => 'You can view information about your affiliate income in your Personal Account',
    'key_253' => 'FINANCIAL MATTERS',
    'key_254' => 'How can I check my balance',
    'key_255' => 'You can check the balance at any time by logging into your personal account',
    'key_256' => 'How soon will my deposit account be credited',
    'key_257' => 'Typically, funds are credited instantly. However, it should be noted that time
                                    crediting may depend on the regulations of the payment systems themselves',
    'key_258' => 'Is there a commission for depositing and withdrawing funds',
    'key_259' => 'No, there is no commission in the system. The investor pays only the commission of the selected
                                    payment system, if any',
    'key_260' => 'OTHER',
    'key_261' => 'I still have questions, where should I go',
    'key_262' => 'If you did not find the answer to the question in the list of frequently asked questions, write to the service
                                    support',
    'key_263' => 'About the company',
    'key_264' => 'Cab Solutions was founded on December 16, 2009 in England',
    'key_265' => 'In connection with the large number of people and visiting tourists in the United Kingdom, taxi services are in great demand. To keep up with the times, we are constantly improving and developing in our business segment',
    'key_266' => 'The demand for our services is so great that we can hardly satisfy it, therefore we attract additional investments.
                            The finances of our investors are spent on the modernization of our taxi fleet, which helps us save huge amounts of money, and as you know, the money saved is the money earned.',
    'key_267' => 'By modernization is meant updating the taxi fleet with special cabs - electric cars. Our drivers work in shifts to maximize profitability.',
    'key_268' => 'It takes just over an hour to charge an electric vehicle with special Superfast Charging cables, which makes our cars simple to use, and the alternative fuel savings are just overwhelming.',
    'key_269' => 'Read more',
    'key_270' => 'Multilevel referral commission from creating a deposit and from daily charges',
    'key_271' => 'Referral',
    'key_272' => 'level',
    'key_273' => 'from charges',
    'key_274' => 'Why choose us',
    'key_275' => 'Stable income',
    'key_276' => 'Official company',
    'key_277' => 'Deposit insurance',
    'key_278' => 'Daily bills',
    'key_279' => 'Powerful DDoS protection',
    'key_280' => 'Encryption Comodo Green EV-SSL',
    'key_281' => 'Professional team',
    'key_282' => 'Qualified support',
    'key_283' => 'Statistics',
    'key_284' => 'Based',
    'key_285' => 'Start',
    'key_286' => 'Days of work',
    'key_287' => 'Total Users',
    'key_288' => 'Total deposits',
    'key_289' => 'Total payouts',
    'key_290' => 'Last user',
    'key_291' => 'Last contribution',
    'key_292' => 'Last payout',
    'key_293' => 'Recent investment',
    'key_294' => 'Investment statistics',
    'key_295' => 'Recent payouts',
    'key_296' => 'Payout statistics',
    'key_297' => 'Certificates',
    'key_298' => 'Available payment systems',
    'key_299' => 'Payment systems',
    'key_300' => 'Payment systems with which you can open a deposit in Sab Solutions',
    'key_301' => 'Certificates',
    'key_302' => 'Our certificates and registration documents. You can get acquainted with them in more detail by clicking on the picture',
    'key_303' => 'Check company',
    'key_304' => 'Platform health protection',
    'key_305' => 'Cab Solutions has several degrees of protection for personal information of investors and the health of the platform',
    'key_306' => 'Recent payments to investors of the Sab Solutions platform online',
    'key_307' => 'Recent open deposits on the Cab Solutions platform online',
    'key_308' => 'Statistics of the platform for online investment Cab Solutions',
    'key_309' => 'Why you should choose us',
    'key_310' => 'Advantages and achievements of the company',
    'key_311' => 'Affiliate program',
    'key_312' => 'The three-level affiliate program of Cab Solutions allows you to receive additional income from deposits and accruals of your referrals',
    'key_313' => 'Company Information',
    'key_314' => 'You can learn more about the company’s activities by clicking this button',
    'key_315' => 'Company Information',
    'key_316' => 'Brief information about the company and its activities',
    'key_317' => 'To continue, activate your account using the link that was sent to your mail',
    'key_319' => 'Welcome to ArunaSP',
    'key_320' => 'Follow the link for confirmation',
    'key_321' => 'Change your details',
    'key_322' => 'Your e-wallets',
    'key_323' => 'Change password',
    'key_324' => 'Retype password',
    'key_325' => 'New password',
    'key_326' => 'Old password',
    'key_327' => 'Your Email',
    'key_328' => 'Your skype login',
    'key_329' => 'Your full name',
    'key_330' => 'Registration date',
    'key_331' => 'Username',
    'key_332' => 'Your personal information',
    'key_333' => 'Your ref. link',
    'key_334' => 'Invited you',
    'key_335' => 'Referrals',
    'key_336' => 'Active referrals',
    'key_337' => 'Received from referrals',
    'key_338' => 'Login',
    'key_339' => 'Date',
    'key_340' => 'Top up',
    'key_341' => 'Referral',
    'key_342' => 'Level 1 Referrals',
    'key_343' => 'Level 2 Referrals',
    'key_344' => 'Level 3 Referrals',
    'key_345' => 'System',
    'key_346' => 'Wallet',
    'key_347' => 'Status',
    'key_348' => 'Amount',
    'key_349' => 'Current account balance',
    'key_350' => 'Select system to output',
    'key_351' => 'After choosing an investment plan you need to click on the icon of your payment system',
    'key_352' => 'Enter wallet number',
    'key_353' => 'Enter the amount',
    'key_354' => 'Print',
    'key_355' => 'Available balance',
    'key_356' => 'Deposit amount',
    'key_357' => 'Top up',
    'key_358' => 'Pay',
    'key_359' => 'Statistics',
    'key_360' => 'Information about the amount of your income',
    'key_361' => 'User',
    'key_362' => 'Registration date',
    'key_363' => 'Last Login',
    'key_364' => 'Balance',
    'key_365' => 'Last contribution',
    'key_366' => 'Total',
    'key_367' => 'Means under review',
    'key_368' => 'Total funds withdrawn',
    'key_369' => 'Active contributions',
    'key_370' => 'Total deposits',
    'key_371' => 'Your ref. link',
    'key_372' => 'Server time',
    'key_373' => 'Deposit date',
    'key_374' => 'Contribution',
    'key_375' => 'Profit',
    'key_376' => 'Works',
    'key_377' => 'Completed',
    'key_378' => 'Make a new contribution',
    'key_379' => 'Make a contribution',
    'key_380' => 'To complete the procedure for opening a deposit, click on the “Create Deposit” button',
    'key_381' => 'Enter the amount you want to invest',
    'key_382' => 'Deposit amount',
    'key_383' => 'Maximum',
    'key_384' => 'Minimum',
    'key_385' => 'days',
    'key_386' => 'through',
    'key_387' => 'per day',
    'key_388' => 'Plans',
    'key_389' => 'Registration',
    'key_390' => 'Personal information',
    'key_391' => 'Your full name',
    'key_392' => 'Enter your full name in the field',
    'key_393' => 'Your Skype username',
    'key_394' => 'Enter your username in the field',
    'key_395' => 'Your email',
    'key_396' => 'Enter your email address in the field. To this address
                                                            will be
                                                            your account is tied (alerts, function
                                                            password recovery, etc.). For the correct operation of all
                                                            Razleton HealthCare Limited system features recommend
                                                            Use your gmail email account. ',
    'key_397' => 'Retry your email',
    'key_398' => 'Repeat your email address',
    'key_399' => 'Login details',
    'key_400' => 'Create a password',
    'key_401' => 'Enter the password for authorization in the field. Do not divulge it and
                                                            not
                                                            pass on to third parties.',
    'key_402' => 'Retype password',
    'key_403' => 'Re-enter password',
    'key_404' => 'I agree with',
    'key_405' => 'terms and conditions',
    'key_406' => 'For registered users',
    'key_407' => 'Login',
    'key_408' => 'Stable income',
    'key_409' => 'Official company',
    'key_410' => 'Deposit insurance',
    'key_411' => 'Daily bills',
    'key_412' => 'Password',
    'key_413' => 'Forgot your password',
    'key_414' => 'Not yet registered',
    'key_415' => 'Open account',
    'key_416' => 'Powerful DDoS protection',
    'key_417' => 'Encryption Comodo Green EV-SSL',
    'key_418' => 'Professional team',
    'key_419' => 'Qualified support',
    'key_420' => 'Forgot your password',
    'key_421' => 'Restore',
    'key_422' => 'Enter e-mail',
	
// 12.04

	'our_mission' => 'Our mission is to move the aquaculture industry towards healthy, sustainable, affordable production with comparable financial returns. We use breakthrough science to accelerate sustainable innovations to market.',
	'global_title' => 'Redesigning our global food system',
	'global_text' => 'With our growing world population we are placing Earth’s natural resources under severe pressure. One of our most pressing challenges will be to feed everyone a balanced and nutritious diet while keeping our effects on the environment to a minimum.',
	'pholosophy_title' => 'Investment philosophy',
	'pholosophy_text' => 'Our investment philosophy is based on creating shareholder value while safeguarding the invested capital of our investors, via a solid and rigorous investment methodology.',
	'food_sector' => 'Promoting a healthy and sustainable food sector',
	'future_aqua' => 'Investing in the future of aquaculture',
	'ocean_conservation' => 'Ocean Conservation',
	'food_security' => 'Food Security',
	'new_jobs' => 'To promote wealth and job creation',
	'excellent_team' => 'Excellent management team',
	'about_us' => 'About Us',
	'about_text' => 'Today, fish supplies 17 percent of all the protein consumed in the world. This share is growing because of  rising income among consumers is accompanied by increased demand for high-quality fish. By 2030, the world is expected to eat 20 percent more fish than in 2016 – this increase can only come from aquaculture because we are already exploiting wild fish stocks. Aquaculture is growing faster than other major food production sectors.
	<p class="text-4">But achieving further growth means making demands on our increasingly stressed environment. Although aquaculture is, in many ways, more resource efficient than other animal production systems it still has a long way to go. We need to do much better.</p>
	<p class="text-4">We believe that reliably meeting the world’s growing demand for fish, while simultaneously sustaining our environment, will require a radical transformation of global aquaculture.</p>
	<p class="text-4"><b>This is why ArunaSP exists.</b></p>
	<p class="text-4">ArunaSP is a global investment fund based in Utrecht, the Netherlands, that is developing this optimal aquaculture food system by investing in companies all along the aquaculture value chain working to solve industry challenges, with a shared vision of a sustainable future.</p>',
	'our_priorities' => 'Our priorities',
	'ocean_conservation' => 'Ocean Conservation',
	'ocean_conservation_text' => 'Capture fisheries are on the verge of collapse due to overfishing, global climate change, ocean acidification and marine pollution. Aquaculture is instrumental in taking pressure off our oceans.',
	'food_security' => 'Food Security',
	'food_security_text' => 'Feeding a global population estimated to reach 9.6 billion by 2050, puts enormous pressure on land and freshwater. Aquaculture, done in a socially and environmentally friendly manner, is crucial to meet the growing demand for healthy and sustainably produced food.',
	'environment' => 'Environment',
	'environment_text' => 'Sustainable Aquaculture can have the lowest environmental impact of animal protein production - with limited use of land, freshwater and a low carbon footprint - and can even have restoratives effects on the environment.',
	'health_benefits' => 'Health Benefits',
	'health_benefits_text' => 'Fish is the only animal protein singled out for its unquestionable human health benefits, containing essential nutrients. Sustainable aquaculture production can create a positive and lasting impact on public health.',
	'invest_with_us' => 'Invest with Us',
	'invest_with_us_text1' => 'We want to secure a sustainable supply of healthy food.',
	'invest_with_us_text2' => 'With over 200 investors from more than 25 countries, ArunaSP is working with a global community of investors supporting the growth of a new sustainable, commercial aquaculture industry. Our investors are family offices, individuals, institutions and industry who see both the financial opportunity in creating a better food system and take pride in protecting our planet and population health.',
	'slogan_1_1' => 'A new innovative investment fund',
	'slogan_1_2' => 'focused on the health of the planet and food security',
	'slogan_1_3' => 'providing a high economic return on investment in AQUACULTURE',
	'slogan_2_1' => 'AQUACULTURE FOR GOOD!',
	'slogan_2_2' => 'For most of history, humans have had to fight nature to survive! Now we are beginning to understand that in order to survive, we must protect her.',
	'slogan_2_3' => 'Jacques Cousteau.',
	'reviews_home' => 'What people are saying about ArunaSP',
	'more_review' => 'See more people review',
	'we_glad' => 'We are glad to see you again!',
	'not_account' => "Don't have an account?",
	'deposit' => 'Deposit',
	'hi' => 'Hi',
	'hello' => 'Hello',
	'help' => 'Need Help?',
	'help_text' => 'Have questions or concerns? Our experts are here to help!',
	'help_btn' => 'Write to us',
	'sum_depo' => 'Deposit amount',
	'btn_depo' => 'Deposit',
	'select_plan' => 'Select plan',
	'daily' => 'Daily',
	'minimum' => 'Minimum',
	'maximum' => 'Maximum',
	'days' => 'days',
	'your_profile' => 'Your profile',
	'info_panel' => 'Dashboard',
	'password_reset' => 'A letter with a link to reset your password has been sent to your email',
	'password_reset_line1' => 'You are receiving this email because we received a password reset request for your account.',
	'password_reset_subject' => 'Reset password',
	'password_reset_line2' => 'If you did not request a password reset, no further action is required.',
	'reset_password_button' => 'Reset Password',
	'regards' => 'Regards',
	'click_problem1' => 'If you’re having trouble clicking the',
	
];
