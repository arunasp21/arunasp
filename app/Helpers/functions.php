<?php

use App\Entity\Plan;
const STATUS_DISABLE = 0;
const STATUS_ACTIVE = 1;


function getPlans()
{
    return Plan::all();
}

function getJsonPlans()
{
    $array = [];

    $plans = Plan::all();

    foreach ($plans as $plan) {
        array_push($array, [
            "id" => $plan->id,
            "description" => $plan->title,
            "percent" => $plan->percent,
            "count" => $plan->day,
            "seconds" => "86400",
            "img" => "",
            "min" => $plan->from_amount,
            "max" => $plan->to_amount,
            "return" => "1",
            "start_data" => "0",
            "end_data" => "0",
            "status" => "1",
            "delete" => "1",
        ]);
    }

    return json_encode($array, true);
}

function rstr($str, $a=0)
{
	if(strlen($str) > $a)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	
}