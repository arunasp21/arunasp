<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePayAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payeer_account' => 'nullable|string',
            'perfect_money_account' => 'nullable|string',
            'qiwi_account' => 'nullable|string',
            'yandex_money_account' => 'nullable|string',
            'bitcoin_account' => 'nullable|string',
            'card_account' => 'nullable|string',
        ];
    }
}
