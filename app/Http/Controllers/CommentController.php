<?php

namespace App\Http\Controllers;

use App\Entity\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['author_id'] = auth()->user()->id;
        Comment::create($data);

        return back()->with('success', 'Комментарий будет опубликован в течении суток');
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(Comment $comment)
    {
        $comment->update([
            'status' => STATUS_ACTIVE
        ]);

        return back()->with('success', 'approve');
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return back()->with('success', 'Comment deleted');
    }
}
