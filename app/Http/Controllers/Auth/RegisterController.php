<?php

namespace App\Http\Controllers\Auth;

use App\Entity\UserDetail;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Entity\Payment;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'confirmed', 'unique:users'],
//            'nickname' => ['required', 'string', 'max:255', 'unique:user_detail'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
//            'phone' => ['required'],
            'rules' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     * @throws \Throwable
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        try {
			$nick = stristr($data['email'], '@', true);
			
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'last_login' => Carbon::now(),
                'skype' => $nick,
                'clean_pass' => $data['password'],
//                'phone' => $data['phone']
            ]);

            if (!empty($data['invited_it'])) {
                $ref = $this->getReferralLvl($data['invited_it']);
            }

            UserDetail::create([
                'user_id' => $user->id,
                'nickname' => $nick,
                'referral_url' => config('app.url') . '/register/?ref=' . $nick,
                'invited_it' => isset($ref['invited_it']) ? $ref['invited_it'] : null,
                'referral_lvl' => isset($ref['referral_lvl']) ? $ref['referral_lvl'] : 1,
            ]);

            DB::commit();

			if(isset($_POST['bonus'])) 
			{
				DB::table('user_detail')->where('user_id', $user->id)->update(['amount' => 10]);
				Payment::create([
					'user_id' => $user->id,
					'payment_system' => 0,
					'payment_type' => 0,
					'wallet' => 'NY Bonus 10$',
					'amount' => 10,
					'status' => Payment::COMPLETE,
				]);
				
			}

            Mail::send('emails.verification-email', ['data' => $user->email], function($message) use ($user) {
                $message->to($user->email, 'Verification email')
                    ->subject('Verification email');
                $message->from('no-reply@arunasp.com', 'ArunaSP');
            });

            return $user;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function getReferralLvl($data)
    {
        $invited_user = UserDetail::whereNickname($data)->first();
        if (!empty($invited_user)) {
            return [
                'invited_it' => $invited_user->user_id,
                'referral_lvl' => $invited_user->referral_lvl + 1
            ];
        } else {
            return [
                'invited_it' => null,
                'referral_lvl' => 1
            ];
        }
    }
}
