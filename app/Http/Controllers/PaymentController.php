<?php

namespace App\Http\Controllers;

use App\Entity\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Entity\UserDetail;


class PaymentController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function paymentOutput(Request $request)
    {
        DB::beginTransaction();
        try {
			if((int)$request->payment_system < 1)
				return response()->json(array('response'=>false, 'code'=>0, 'text'=> 'Выберите платежную систему'));			
			
			if(!rstr($request->wallet))
				return response()->json(array('response'=>false, 'code'=>1, 'text'=> 'Введите номер кошелька'));			
			
			if($request->amount < 1)
				return response()->json(array('response'=>false, 'code'=>2, 'text'=> 'Введите сумму!<br>Минимальная сумма для вывода - 1$.'));			
			
            Payment::create([
                'user_id' => auth()->user()->id,
                'payment_system' => $request->payment_system,
                'payment_type' => $request->payment_type,
                'wallet' => $request->wallet,
                'amount' => $request->amount,
                'status' => Payment::EXPECTED,
            ]);

			UserDetail::whereUserId(auth()->user()->id)->update([
				'amount' => auth()->user()->detail->amount - $request->amount,
			]);

            DB::commit();

			return response()->json(array('response'=>true));			

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}
