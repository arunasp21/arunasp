<?php

namespace App\Http\Controllers;

use App\Entity\Comment;
use App\Entity\LastStatistic;
use App\Entity\Payment;
use App\Entity\Statistic;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$start = '15.07.2019';

//        $last_user = User::orderBy('created_at', 'desc')->first()->email;
//		$last_user = explode("@", $last_user);
		
        $statistics = [
            'founded_date' => '16.12.2009',
            'start_date' => $start,
            'day_works' => floor((time() - strtotime($start))/(60*60*24)),
            'count_users' => User::all()->count(),
            'all_input' => Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::INPUT)->sum('amount'),
            'all_output' => Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::OUTPUT)->sum('amount'),
//            'last_user_1' => $last_user[0],
//            'last_user_2' => $last_user[1],
            'last_input' => isset(Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::INPUT)->orderBy('created_at', 'desc')->first()->amount) ? Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::INPUT)->orderBy('created_at', 'desc')->first()->amount : 0,
            'last_output' => isset(Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::OUTPUT)->orderBy('created_at', 'desc')->first()->amount) ? Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::OUTPUT)->orderBy('created_at', 'desc')->first()->amount : 0,
        ];
//        $statistics = Statistic::first()->toArray();
		$reviews = Comment::with('author')->whereStatus(STATUS_ACTIVE)->orderBy('id', 'desc')->limit(6)->get();
//dd($reviews);

		$title = 'ArunaSP';

        return view('page.index', compact('statistics', 'reviews', 'title'));
    }

    public function started()
    {
		$title = __('my-text.head_menu_6');
		
        return view('page.started', compact('title'));
    }

    public function faq()
    {
		$title = 'FAQ';

        return view('page.faq', compact('title'));
    }

    public function rules()
    {
		$title = __('my-text.head_menu_8');
		
        return view('page.rules', compact('title'));
    }

    public function reviews()
    {
		$title = __('my-text.head_menu_9');

        return view('page.reviews',[
            'comments' => Comment::with('author')->whereStatus(STATUS_ACTIVE)->orderBy('id', 'desc')->get(),
			'title' => $title
        ]);
    }

    public function support()
    {
		$title = __('my-text.head_menu_10');

        return view('page.support', compact('title'));
    }

    public function about()
    {
		$title = __('my-text.head_menu_2');

        return view('page.about', compact('title'));
    }

    public function legalDoc()
    {
        return view('page.legal_doc');
    }

    public function news()
    {
		$title = __('my-text.head_menu_5');

        return view('page.news', compact('title'));
    }

    public function full_text()
    {
		$title = __('my-text.head_menu_5');

        return view('page.full_new', compact('title'));
    }

    public function card()
    {
        return view('page.card');
    }

    public function check_email()
    {
        return view('auth.check_email');
    }

    public function statistics()
    {
//        $last_input = LastStatistic::whereId(1)->first()->toArray();
//        $last_output = LastStatistic::whereId(2)->first()->toArray();

		$last_input = Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::INPUT)->orderBy('id', 'desc')->limit(10)->get();
		$last_output = Payment::where('status', Payment::COMPLETE)->where('payment_type', '=', Payment::OUTPUT)->orderBy('id', 'desc')->limit(10)->get();

        return view('page.statistic', compact('last_input', 'last_output'));
    }

    public function antiSpam()
    {
        return view('page.anti_spam');
    }

    public function policy()
    {
        return view('page.policy');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendQuestionInMail(Request $request)
    {
        Mail::send('emails.question-form', ['data' => $request->all()], function($message) {
            $message->to('support@arunasp.com', 'Запрос в техподдержку')
                ->subject('Запрос в техподдержку');
            $message->from(env('MAIL_USERNAME'), 'Партнер');
        });

        return back();
    }

    public function sendModalForm(Request $request)
    {
        Mail::send('emails.modal-form', ['data' => $request->all()], function($message) {
            $message->to(env('MAIL_USERNAME'), 'Question of user')
                ->subject('Question of user');
            $message->from(env('MAIL_USERNAME'),'Cab Solutions');
        });

        return back();
    }
}
