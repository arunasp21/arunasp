<?php

namespace App\Http\Controllers;


use App\Entity\Payment;
use App\Entity\UserDetail;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class PaySystemController extends Controller
{
    public function PayeerInit(Request $request){

        $payment= Payment::create([
            'user_id'=> auth()->user()->id,
            'payment_system'=> Payment::PAYEER,
            'payment_type'=> Payment::INPUT,
            'amount'=> $request->amount,
            'wallet'=> 'USD',
            'status'=> Payment::EXPECTED,
        ]);



        $m_shop = '793196380';  //замінити
        $m_orderid = md5(uniqid($payment->id, true));
        $m_amount = number_format($request->amount, 2, '.', '');
        $m_curr = 'USD';
        $m_desc = base64_encode($payment->id . rand(1, 1000));
        $m_key = 'Cabsolutions13';    //замінити

        $arHash = array(
            $m_shop,
            $m_orderid,
            $m_amount,
            $m_curr,
            $m_desc,
            //$m_key
        );
         $arHash[] = $m_key;
        $sign = strtoupper(hash('sha256', implode(":", $arHash)));

        $payment->update([
            'order'=> $m_orderid,
        ]);

        $mas[0]=$m_shop;
        $mas[1]=$m_orderid;
        $mas[2]=$m_amount;
        $mas[3]=$m_curr;
        $mas[4]=$m_desc;
        $mas[5]=$sign;

        return $mas;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function PayeerError(Request $request)
    {
        $data = $request->all();

        $payment= Payment::where('order', '=', $data['m_orderid'])->first();
        $payment->update([
            'amount'=>$data['m_amount'],
            'status'=> Payment::DENIED,
            'completed_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('account');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function PayeerSuccess(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $payment= Payment::where('order', '=', $data['m_orderid'])->first();
            $payment->update([
                'amount'=> $data['m_amount'],
                'status'=> Payment::COMPLETE,
                'completed_at'=>Carbon::now()->format('Y-m-d H:i:s')
            ]);

            $user = User::find($payment->user_id);

            $new_balance=$user->detail->amount + $data['m_amount'];
            UserDetail::where('id', '=', $user->detail->id)->update([
                'amount' => $new_balance
            ]);
            DB::commit();

            $response = ['message' => 'Success'];
        } catch (\Throwable $e) {
            DB::rollback();

            $response = ['error' => $e->getMessage()];
        }

        return redirect()->route('account')->with('message', $response);
    }

    public function PayeerStatus(Request $request){
        if (!in_array($_SERVER['REMOTE_ADDR'], array('185.71.65.92', '185.71.65.189', '149.202.17.210'))) return;

        if (isset($_POST['m_operation_id']) && isset($_POST['m_sign']))
        {
            $m_key = '793196380';

            $arHash = array(
                $_POST['m_operation_id'],
                $_POST['m_operation_ps'],
                $_POST['m_operation_date'],
                $_POST['m_operation_pay_date'],
                $_POST['m_shop'],
                $_POST['m_orderid'],
                $_POST['m_amount'],
                $_POST['m_curr'],
                $_POST['m_desc'],
                $_POST['m_status']
            );

            if (isset($_POST['m_params']))
            {
                $arHash[] = $_POST['m_params'];
            }

            $arHash[] = $m_key;

            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));

            if ($_POST['m_sign'] == $sign_hash && $_POST['m_status'] == 'success')
            {
                echo $_POST['m_orderid'].'|success';
                exit;
            }

            echo $_POST['m_orderid'].'|error';
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function Perfect_MoneyInit(Request $request){

        $payment= Payment::create([
            'user_id'=> auth()->user()->id,
            'payment_system'=> Payment::PERFECT,
            'payment_type'=> Payment::INPUT,
            'amount'=> $request->amount,
            'wallet'=> 'USD',
            'status'=> Payment::EXPECTED,
        ]);

        $m_orderid = md5(uniqid($payment->id, true));

        $payment->update([
            'order'=> $m_orderid,
        ]);

        //$PAYEE_ACCOUNT = 'U20279273';
        $PAYEE_ACCOUNT = 'U35021056';

        $PAYEE_NAME= 'ArunaSP';
        $PAYMENT_ID= $m_orderid;
        $PAYMENT_AMOUNT= $request->amount;
        $PAYMENT_UNITS= 'USD';
        $STATUS_URL='mailto:sparunasp@gmail.com';   //замінити
        $PAYMENT_URL='https://arunasp.com/pay-perfect-money-success';   //замінити
        $PAYMENT_URL_METHOD='GET';
        $NOPAYMENT_URL='https://arunasp.com/pay-perfect-money-error';   //замінити
        $NOPAYMENT_URL_METHOD='GET';
        $SUGGESTED_MEMO=''; //додаткове поле
        $BAGGAGE_FIELDS=''; //хз

        $mas[0]= $PAYEE_ACCOUNT;
        $mas[1]= $PAYEE_NAME;
        $mas[2]= $PAYMENT_ID;
        $mas[3]= $PAYMENT_AMOUNT;
        $mas[4]= $PAYMENT_UNITS;
        $mas[5]= $STATUS_URL;
        $mas[6]= $PAYMENT_URL;
        $mas[7]= $PAYMENT_URL_METHOD;
        $mas[8]= $NOPAYMENT_URL;
        $mas[9]= $NOPAYMENT_URL_METHOD;
        $mas[10]= $SUGGESTED_MEMO;
        $mas[11]= $BAGGAGE_FIELDS;
        $mas[12]= 'USD';

        return $mas;

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Perfect_MoneySuccess(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();

            $format="Y-m-d H:i:s";

            $payment= Payment::where('order', '=', $data['PAYMENT_ID'])->first();
            $payment->update([
                'amount'=>$data['PAYMENT_AMOUNT'],
                'status'=> Payment::COMPLETE,
                'completed_at'=>date($format,$data['TIMESTAMPGMT']),
            ]);

            $user = User::find($payment->user_id);

            $new_balance=$user->detail->amount + $data['PAYMENT_AMOUNT'];
            UserDetail::where('id', '=', $user->detail->id)->update([
                'amount' => $new_balance
            ]);
            DB::commit();

            $response = ['message' => 'Success'];
        } catch (\Throwable $e) {
            DB::rollback();

            $response = ['error' => $e->getMessage()];
        }

        return redirect()->route('account')->with('message', $response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Perfect_MoneyError(Request $request)
    {
	    $data = $request->all();

        $format="Y-m-d H:i:s";

        $payment= Payment::where('order', '=', $data['PAYMENT_ID'])->first();
        $payment->update([
            'amount'=>$data['PAYMENT_AMOUNT'],
            'status'=> Payment::DENIED,
            'completed_at'=> Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        return redirect()->route('account');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function AdvCashInit(Request $request){

        $payment= Payment::create([
                'user_id'=> auth()->user()->id,
                'payment_system'=> Payment::ADV,
                'payment_type'=> Payment::INPUT,
                'amount'=> $request->amount,
                'wallet'=> 'USD',
                'status'=> Payment::EXPECTED,
            ]);


        $m_orderid = md5(uniqid($payment->id, true));

        $payment->update([
                'order'=> $m_orderid,
            ]);

        $ac_account_email='viktorov1199@gmail.com';
        $ac_sci_name='cab-solutions';
        $ac_amount= $request->amount;
        $ac_currency= 'USD';
        $ac_order_id= $m_orderid;
        $ac_success_url='http://www.cab-solutions.club/pay-adv-cash-success';
        $ac_success_url_method='GET';
        $ac_fail_url='http://www.cab-solutions.club/pay-adv-cash-error';
        $ac_fail_url_method='GET';
        $ac_status_url='http://www.cab-solutions.club/pay-adv-cash-status';
        $ac_status_url_method='GET';
        $m_key='789qaZ';

        $arHash = array(
            $ac_account_email,
            $ac_sci_name,
            $ac_amount,
            $ac_currency,
            $m_key,
            $ac_order_id
        );

        $ac_sign = strtoupper(hash('sha256', implode(":", $arHash)));

        $mas[0]=$ac_account_email;
        $mas[1]=$ac_sci_name;
        $mas[2]=$ac_amount;
        $mas[3]=$ac_currency;
        $mas[4]=$ac_order_id;
        $mas[5]=$ac_sign;
        $mas[6]=$ac_success_url;
        $mas[7]=$ac_success_url_method;
        $mas[8]=$ac_fail_url;
        $mas[9]=$ac_fail_url_method;
        $mas[10]=$ac_status_url;
        $mas[11]=$ac_status_url_method;

        return $mas;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function AdvCashSuccess(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();

            $payment= Payment::where('order', '=',  $data['ac_order_id'])->first();
            $payment->update([
                'amount'=>$data['ac_amount'],
                'status'=>'completed',
                'completed_at'=>Carbon::parse($data['ac_start_date'])->format("Y-m-d H:i:s"),
            ]);

            $user = User::find($payment->user_id);

            $new_balance=$user->detail->amount + $data['ac_amount'];

            UserDetail::where('id', '=', $user->detail->id)->update([
                'amount' => $new_balance
            ]);
            DB::commit();

            $response = ['message' => 'Success'];
        } catch (\Throwable $e) {
            DB::rollback();

            $response = ['error' => $e->getMessage()];
        }

        return redirect()->route('account')->with('message', $response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function AdvCashError(Request $request)
    {
        $data = $request->all();
        $payment= Payment::where('order', '=', $data['ac_order_id'])->first();
        $payment->update([
            'amount'=>$data['ac_amount'],
            'status'=> Payment::DENIED,
            'completed_at'=>Carbon::parse($data['ac_start_date'])->format("Y-m-d H:i:s"),
        ]);

        return redirect()->route('account');
    }

    public function AdvCashStatus(){

        dd("status");
    }
}
