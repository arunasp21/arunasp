<?php

namespace App\Http\Controllers;

use App\Entity\Deposit;
use App\Entity\Percent;
use App\Entity\UserDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PercentController extends Controller
{
    /**
     * @return string
     * @throws \Throwable
     */
    public function runPercent()
    {
        DB::beginTransaction();
        try {
            $depositActive = Deposit::with('user.detail', 'plan')->whereStatus(1)->get();

            foreach ($depositActive as $deposit) {
                if (Carbon::parse($deposit->to_date) >= Carbon::now()) {
                    $percent = $deposit->amount * ($deposit->plan->percent / 100);
                    UserDetail::whereId($deposit->user->detail->id)->update([
                        'amount' => $deposit->user->detail->amount + $percent
                    ]);
                    Percent::create([
                        'user_id' => $deposit->user->id,
                        'amount' => $percent,
                        'deposit_id' => $deposit->id
                    ]);
                    if (Carbon::parse($deposit->to_date)->format('Y-m-d') == Carbon::now()->format('Y-m-d')) {
                        $deposit->update([
                            'status' => 0
                        ]);
                    }
                }

				echo $deposit->id.'<br>';
                if (Carbon::parse($deposit->to_date)->format('Y-m-d') == Carbon::now()->format('Y-m-d')) {
                    UserDetail::whereId($deposit->user->detail->id)->update([
                        'amount' => $deposit->user->detail->amount + $deposit->amount
                    ]);
                }
				
            }
            DB::commit();

            return 'success';

        } catch (\Throwable $e) {
            DB::rollback();

            return $e;
        }
    }
}
