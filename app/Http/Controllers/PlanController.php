<?php

namespace App\Http\Controllers;

use App\Entity\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function index()
    {
        $plans = Plan::all();
        $titleMenu = ' Плани';

        return view('cabinet.dashboard.plan.index', compact('plans', 'titleMenu'));
    }

    public function create()
    {
        $titleMenu = ' Плани';
		
        return view('cabinet.dashboard.plan.form', [
            'action' => 'Create',
            'titleMenu' => $titleMenu,
        ]);
    }

    public function store(Request $request)
    {
        Plan::create($request->all());

        return redirect()->route('plans.index');
    }

    public function edit(Plan $plan)
    {
        $titleMenu = ' Плани';
        return view('cabinet.dashboard.plan.form', [
            'action' => 'Create',
            'plan' => $plan,
            'titleMenu' => $titleMenu,
        ]);
    }

    public function update(Plan $plan, Request $request)
    {
        $plan->update($request->all());

        return redirect()->route('plans.index');
    }

}
