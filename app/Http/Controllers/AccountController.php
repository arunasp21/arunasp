<?php

namespace App\Http\Controllers;

use App\Entity\Comment;
use App\Entity\Deposit;
use App\Entity\LastStatistic;
use App\Entity\Payment;
use App\Entity\Plan;
use App\Entity\Referral;
use App\Entity\Statistic;
use App\Entity\UserDetail;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdatePayAccountRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $allDepositAmount = Deposit::whereUserId(auth()->user()->id)->sum('amount');
        $activeDepositAmount = Deposit::whereUserId(auth()->user()->id)->whereStatus(1)->sum('amount');
        $allAmountOutputComplete = Payment::whereUserId(auth()->user()->id)->wherePaymentType(Payment::OUTPUT)->whereStatus(Payment::COMPLETE)->sum('amount');
        $allAmountOutputExpected = Payment::whereUserId(auth()->user()->id)->wherePaymentType(Payment::OUTPUT)->whereStatus(Payment::EXPECTED)->sum('amount');
        $lastDeposit = Deposit::whereUserId(auth()->user()->id)->orderBy('created_at', 'desc')->first();
        $titleMenu = __('my-text.account');

        return view('cabinet.index', [
            'titleMenu' => $titleMenu,
            'allDepositAmount' => $allDepositAmount,
            'activeDepositAmount' => $activeDepositAmount,
            'allAmountOutputComplete' => $allAmountOutputComplete,
            'allAmountOutputExpected' => $allAmountOutputExpected,
            'lastDeposit' => !empty($lastDeposit) ? $lastDeposit->amount : 0,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invest()
    {
        $titleMenu = __('my-text.key_50');
        return view('cabinet.invest', [
            'titleMenu' => $titleMenu,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function payment()
    {
        $paymentSystem = Payment::$paymentSystem;
        $titleMenu = __('my-text.key_52');

        return view('cabinet.payment', compact('paymentSystem', 'titleMenu'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deposit()
    {
        $plans = Plan::all();
        $titleMenu = __('my-text.key_51');
        return view('cabinet.deposit', [
            'plans' => $plans,
            'titleMenu' => $titleMenu,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function depositHistory()
    {
        $deposits = Deposit::whereUserId(auth()->user()->id)->get();
        $titleMenu = __('my-text.key_54');

        return view('cabinet.deposit_history', [
            'deposits' => $deposits,
            'titleMenu' => $titleMenu,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function investHistory()
    {
        $payments = Payment::whereUserId(auth()->user()->id)->wherePaymentType(Payment::INPUT)->get();
        $paymentStatuses = Payment::$paymentStatus;
        $paymentSystems = Payment::$paymentSystem;
        $titleMenu = __('my-text.key_55');

        return view('cabinet.invest_history', [
            'payments' => $payments,
            'paymentStatuses' => $paymentStatuses,
            'paymentSystems' => $paymentSystems,
            'titleMenu' => $titleMenu,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paymentHistory()
    {
        $payments = Payment::whereUserId(auth()->user()->id)->wherePaymentType(Payment::OUTPUT)->get();
        $titleMenu = __('my-text.key_56');

        return view('cabinet.payment_history', [
            'payments' => $payments,
            'titleMenu' => $titleMenu,
        ]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function referral()
    {
        $amountReferrals = Referral::whereReferralId(auth()->user()->id)->sum('amount');
//        $referalUsers = Referral::with('user')->whereReferralId(auth()->user()->id)->get();
        $referalUsers = UserDetail::with('user')->whereInvitedIt(auth()->user()->id)->get();
		$InvitedIt = UserDetail::whereUserId(auth()->user()->id)->first();
		$InvitedIt = User::whereId($InvitedIt->invited_it)->first();

		$user_data = DB::table('users')->where('id', auth()->user()->id)->first();

		$user_data_new = DB::table('user_detail')->where('user_id', auth()->user()->id)->first();
		$invited_it = DB::table('user_detail')->where('user_id', $user_data_new->invited_it)->first();

//		$first_ref = UserDetail::whereUserId(auth()->user()->id)->get();
//		$first_ref = DB::table('user_detail')->where('user_id', auth()->user()->id)->first();

        $first_ref = DB::table('user_detail')
            ->join('referrals', function ($join) {
                $join->on('user_detail.user_id', '=', 'referrals.referral_id')
                    ->where('referrals.user_id', '=', auth()->user()->id)
                    ->where('referrals.percent', '=', 10);
            })
            ->get();

        $second_ref = DB::table('user_detail')
            ->join('referrals', function ($join) {
                $join->on('user_detail.user_id', '=', 'referrals.referral_id')
                    ->where('referrals.user_id', '=', auth()->user()->id)
                    ->where('referrals.percent', '=', 5);
            })
            ->get();

        $third_ref = DB::table('user_detail')
            ->join('referrals', function ($join) {
                $join->on('user_detail.user_id', '=', 'referrals.referral_id')
                    ->where('referrals.user_id', '=', auth()->user()->id)
                    ->where('referrals.percent', '=', 2);
            })
            ->get();

		$ref_cnt = DB::table('user_detail')->where('invited_it', '=', auth()->user()->id)->count();
		$ref_active_cnt = DB::table('referrals')
			->select('user_id', 'referral_id')
			->where('user_id', '=', auth()->user()->id)
			->where('percent', '=', 10)
			->distinct()
			->get();

		$total_ref = DB::table('referrals')->where('user_id', '=', auth()->user()->id)->sum('amount');

        $titleMenu = __('my-text.key_57');

        return view('cabinet.referral', [
            'amountReferrals' => $amountReferrals,
            'referalUsers' => $referalUsers,
            'titleMenu' => $titleMenu,
            'invitedIt' => $InvitedIt,
            'user_data' => $user_data,
            'invited_it' => $invited_it,
            'first_ref' => $first_ref,
            'second_ref' => $second_ref,
            'third_ref' => $third_ref,
            'ref_cnt' => $ref_cnt,
            'ref_active_cnt' => count($ref_active_cnt),
            'total_ref' => $total_ref,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reklama()
    {
        return view('cabinet.reklama');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setting()
    {
        $titleMenu = __('my-text.key_58');
        return view('cabinet.setting', [
            'titleMenu' => $titleMenu,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function card()
    {
        return view('cabinet.card');
    }

    /**
     * @param UpdatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = User::find(auth()->user()->id);

        if (Hash::check($request->last_password, $user->password)) {
            $data = [
                'skype' => $request->skype,
                'name' => $request->name
            ];
            if (!is_null($request->password)){
                $data['password'] = Hash::make($request->password);
            }
            $user->update($data);
        }

        return back();
    }

    public function updatePayAccounts(UpdatePayAccountRequest $request)
    {
        UserDetail::whereUserId(auth()->user()->id)->update($request->except('_token'));

        return back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function moderationComments()
    {
        $titleMenu = ' Отзиви';
        return view('cabinet.dashboard.comments', [
            'comments' => Comment::with('author')->whereStatus(STATUS_DISABLE)->get(),
            'titleMenu' => $titleMenu,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function users()
    {
        $titleMenu = 'Пользователи';
        return view('cabinet.dashboard.user.index',[
           'users' => User::paginate(config('app.pagination')),
            'titleMenu' => $titleMenu,
        ]);
    }

	public function change_all($user)
	{
        $titleMenu = 'Изменение настроек';
		$user_data = DB::table('users')->where('id', $user)->first();

        $amountReferrals = Referral::whereReferralId($user)->sum('amount');
        $referalUsers = UserDetail::with('user')->whereInvitedIt($user)->get();
		$InvitedIt = UserDetail::whereUserId($user)->first();
		$InvitedIt = User::whereId($InvitedIt->invited_it)->first();
		
		return view('cabinet.change_all', ['titleMenu'=>$titleMenu, 'user_data'=>$user_data, 'amountReferrals'=>$amountReferrals, 'referalUsers'=>$referalUsers, 'invitedIt'=>$InvitedIt]);
	}


    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus(User $user)
    {
        if ($user->status == STATUS_ACTIVE){
            $user->update(['status' => STATUS_DISABLE]);
        }else{
            $user->update(['status' => STATUS_ACTIVE]);
        }

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verificationEmail(Request $request)
    {
        User::where('email', $request->email)->update([
            'email_verified_at' => Carbon::now()
        ]);

        return redirect()->route('account');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistics()
    {
        $titleMenu = ' Статистика';
        $statistic = Statistic::first();

        return view('cabinet.dashboard.statistics', compact('titleMenu', 'statistic'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statisticsOutput()
    {
        $titleMenu = ' Последние выплаты';
        $last_output = LastStatistic::whereId(2)->first();

        return view('cabinet.dashboard.last-output', compact('titleMenu', 'last_output'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statisticsInput()
    {
        $titleMenu = ' Последние инвестиции';
        $last_input = LastStatistic::whereId(1)->first();


        return view('cabinet.dashboard.last-input', compact('titleMenu', 'last_input'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatistics(Request $request)
    {
        $statistic = Statistic::first();
        $statistic->update($request->except('_token'));

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatisticsInput(Request $request)
    {
        $last_input = LastStatistic::whereId(1)->first();

        $last_input->update($request->except('_token'));

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatisticsOutput(Request $request)
    {
        $last_output = LastStatistic::whereId(2)->first();

        $last_output->update($request->except('_token'));

        return back();
    }
	
    public function UploadAvatar(Request $request)
    {
      if($request->hasFile('new_avatar') && $request->file('new_avatar')->isValid())
      {
          $this->ext = $request->file('new_avatar')->extension();
          $this->fsize_in_bt = $request->file('new_avatar')->getClientSize();

          if($this->ext == 'gif' || $this->ext == 'jpg' || $this->ext == 'png' || $this->ext == 'jpeg')
          {
              $filesize = bcdiv(bcdiv($this->fsize_in_bt, 1024, 2), 1024, 2);

              if($filesize < 25)
              {
				  $file = md5(time()).'.'.$this->ext;

				  $contents = file_get_contents($request->file('new_avatar'));
				  Storage::disk('public')->put('/avatars/'.$file, $contents);
				  
   				  User::where('id', auth()->user()->id)->update([
					  'img' => $file
				  ]);
              }
              else
              {
                  return response()->json(array('response'=>false, 'code'=>0, 'text'=> 'ОШИБКА! Размер файла не должен превышать 25 Мб!'));
              }
          }
          else
          {
              return response()->json(array('response'=>false, 'code'=>0, 'text' => 'ОШИБКА! Недопустимое расширение файла!'));
          }
      }
      else
      {
          return response()->json(array('response'=>false, 'code'=>0, 'text'=> 'Ошибка файла!'));
      }
    }
	
    public function AddDeposit(Request $request)
    {
        DB::beginTransaction();
        try {

			if(auth()->user()->detail->amount < $request->input('amount'))
				return response()->json(array('response'=>false, 'code'=>1, 'text'=> 'Недостаточно средств!'));			
			
			if((int)$request->input('plan') > 0)
			{
				$plan = $this->validatePlan($request->all());
			}
			else
			{
				return response()->json(array('response'=>false, 'code'=>0, 'text'=> 'Выберите план!'));			
			}

			if(!is_null($plan))
			{
				if ($plan) 
				{
//					$deposit = Deposit::whereUserId(auth()->user()->id)->first();
//					if (is_null($deposit)){
//					}

					$depo_id = Deposit::create([
						'user_id' => auth()->user()->id,
						'plan_id' => $plan->id,
						'amount' => $request->amount,
						'from_date' => Carbon::now(),
						'to_date' => Carbon::now()->addDay($plan->day),
						'status' => 1,
					]);

					$depo = Deposit::whereId($depo_id->id)->first();

					$refs = $depo->amount * ($depo->plan->percent * $depo->plan->day)/100;

					if (!is_null(auth()->user()->detail->invited_it)){
						$status = $this->referral_calculation(auth()->user()->detail->invited_it, $request->amount, $refs, auth()->user()->detail->referral_lvl);
					}

					UserDetail::whereUserId(auth()->user()->id)->update([
						'amount' => auth()->user()->detail->amount - $request->amount,
					]);
	/*
					if (!is_null(auth()->user()->detail->invited_it)) {
						$owner = User::whereId(auth()->user()->detail->invited_it)->first();

						UserDetail::whereUserId($owner->id)->update([
							'amount' => $owner->detail->amount + ($referrals[auth()->user()->detail->referral_lvl] / 100) * $request->amount,
						]);
					}
	*/				
					DB::commit();

					return response()->json(array('response'=>true));			
				} else {
					return response()->json(array('response'=>false, 'code'=>1, 'text'=> 'Сумма не соответствует плану!'));			
				}
			}
			else
			{
				return response()->json(array('response'=>false, 'code'=>0, 'text'=> 'Выберите план!'));			
			}
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
	}

    public function validatePlan($data)
    {
        $plan = Plan::find($data['plan']);

        if ($data['amount'] >= $plan->from_amount && $data['amount'] <= $plan->to_amount) {
            return $plan;
        } else {
            return false;
        }
    }

    public function referral_calculation($user_id, $dep, $suma, $mylvl)
    {
        $referal_isset = false;

        $invited_it = DB::table('user_detail')
            ->where('user_id', '=', $user_id)
            ->first();

        $access_lvl = $mylvl - $invited_it->referral_lvl;
        //return $access_lvl;
        if ($access_lvl <= 3) {

                if ($access_lvl <= 3) {
                    if ($access_lvl == 1) {
                        $percent = 10;
                    } elseif ($access_lvl == 2) {
                        $percent = 5;
                    } elseif ($access_lvl == 3) {
                        $percent = 2;
                    }
                    $proc_money = ($percent / 100) * $suma;
                    $referal_isset = true;
                    DB::table('user_detail')
                        ->where('id', '=', $invited_it->id)
                        ->update([
                            'amount' => $invited_it->amount + $proc_money,
                        ]);
                }

            if ($referal_isset) {
				Referral::create([
					'user_id' => $invited_it->user_id,
					'depo' => $dep,
					'referral_id' => auth()->user()->id,
					'amount' => $proc_money,
					'percent' => $percent
				]);
            }

            if ($invited_it->invited_it != 0) {
                $this->referral_calculation($invited_it->invited_it, $dep, $suma, $mylvl);
                //return $invited_it->invited_it;
            } else {
                return 'success';
            }
        } else {
            return 'success';
        }
    }

    public function AddTop(Request $request)
    {
		$uid = $request->input('uid');
		$dt = $request->input('dt');
		$amount = $request->input('amount');
		$system = $request->input('system');
		$status = $request->input('status');

		$data = array(
			'user_id' => $uid,
			'payment_system' => $system,
			'payment_type' => 1,
			'wallet' => 'USD',
			'amount' => $amount,
			'status' => $status,
			'created_at' => $dt
		);

		DB::table('payments')->insert($data);
		
		if($status == 2)
		{
            $user = User::find($uid);

            $new_balance=$user->detail->amount + $amount;
            UserDetail::where('id', '=', $user->detail->id)->update([
                'amount' => $new_balance
            ]);
            DB::commit();
		}
	}

    public function AddRefOther(Request $request)
    {
		$uid = $request->input('uid');
		$call = $request->input('call');
		$count_ref = $request->input('count_ref');
		$active_ref = $request->input('active_ref');
		$amount_ref = $request->input('amount_ref');

		User::where('id', '=', $uid)->update([
			'count_ref' => $count_ref,
			'active_ref' => $active_ref,
			'amount_ref' => $amount_ref,
			'invited' => $call
		]);
		DB::commit();

	}

    public function AddRef(Request $request)
    {
		$uid = $request->input('uid');
		$dt = $request->input('dt');
		$login = $request->input('login');
		$depo = $request->input('depo');
		$referals = $request->input('referals');
		$level = $request->input('level');

		$data = array(
			'nickname' => $login,
			'invited_it' => $uid,
			'referral_lvl' => $level,
			'created_at' => $dt,
			'custom' => 1,
			'depo' => $depo,
			'referals' => $referals
		);

		DB::table('user_detail')->insert($data);
	}

    public function EditProfile(Request $request)
    {
		$uid = $request->input('uid');
		$balance = $request->input('balance');
		$last_depo = $request->input('last_depo');
		$total = $request->input('total');
		$ext_amount = $request->input('ext_amount');
		$withdrawals = $request->input('withdrawals');
		$active_depo = $request->input('active_depo');
		$total_depo = $request->input('total_depo');

		User::where('id', '=', $uid)->update([
			'custom_profile' => 1,
			'balance' => $balance,
			'last_depo' => $last_depo,
			'total' => $total,
			'ext_amount' => $ext_amount,
			'withdrawals' => $withdrawals,
			'active_depo' => $active_depo,
			'total_depo' => $total_depo
		]);
		DB::commit();
	}

    public function runSend(Request $request)
    {
		$emails = DB::table('mails')->select('email')->where('id', '>', 1717)->groupBy('email')->get();

		$i = 0;
		$start = 'OK';

		foreach($emails as $email)
		{
			Mail::send('emails.mail', ['data' => $start], function($message) use ($email) {
				$message->to($email->email, 'Приглашение к сотрудничеству')
					->subject('Приглашение к сотрудничеству');
				$message->from('no-reply@arunasp.com', 'ArunaSP');
			});
			$i++;
			sleep(1);
		}
	
		echo $start.'<br>'.$i;
		
	}


	
}
