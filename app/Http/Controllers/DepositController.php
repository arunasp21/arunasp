<?php

namespace App\Http\Controllers;

use App\Entity\Deposit;
use App\Entity\Plan;
use App\Entity\Referral;
use App\Entity\UserDetail;
use App\Http\Requests\StoreDepositRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepositController extends Controller
{
//    public function index()
//    {
//
//    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(StoreDepositRequest $request)
    {
        DB::beginTransaction();
        try {
            $plan = $this->validatePlan($request->all());

            if ($plan) {
                $deposit = Deposit::whereUserId(auth()->user()->id)->first();
                if (is_null($deposit)){
                    if (!is_null(auth()->user()->detail->invited_it)){
						$status = $this->referral_calculation(auth()->user()->detail->invited_it, $request->amount, auth()->user()->detail->referral_lvl);
                    }
                }

                Deposit::create([
                    'user_id' => auth()->user()->id,
                    'plan_id' => $plan->id,
                    'amount' => $request->amount,
                    'from_date' => Carbon::now(),
                    'to_date' => Carbon::now()->addDay($plan->day),
                    'status' => 1,
                ]);

                UserDetail::whereUserId(auth()->user()->id)->update([
                    'amount' => auth()->user()->detail->amount - $request->amount,
                ]);
/*
                if (!is_null(auth()->user()->detail->invited_it)) {
                    $owner = User::whereId(auth()->user()->detail->invited_it)->first();

                    UserDetail::whereUserId($owner->id)->update([
                        'amount' => $owner->detail->amount + ($referrals[auth()->user()->detail->referral_lvl] / 100) * $request->amount,
                    ]);
                }
*/				
                DB::commit();

                return redirect()->route('deposit.list');
            } else {
                return back()->with('error', 'Введіть суму у відповідності вимогам плану, або виберіть інший план');
            }
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function validatePlan($data)
    {
        $plan = Plan::find($data['plan']);

        if ($data['amount'] >= $plan->from_amount && $data['amount'] <= $plan->to_amount) {
            return $plan;
        } else {
            return false;
        }
    }
	
    public function referral_calculation($user_id, $suma, $mylvl)
    {
        $referal_isset = false;

        $invited_it = DB::table('user_detail')
            ->where('id', '=', $user_id)
            ->first();

        $access_lvl = $mylvl - $invited_it->referral_lvl;
        //return $access_lvl;
        if ($access_lvl <= 3) {

                if ($access_lvl <= 3) {
                    if ($access_lvl == 1) {
                        $percent = 10;
                    } elseif ($access_lvl == 2) {
                        $percent = 5;
                    } elseif ($access_lvl == 3) {
                        $percent = 2;
                    }
                    $proc_money = ($percent / 100) * $suma;
                    $referal_isset = true;
                    DB::table('user_detail')
                        ->where('id', '=', $invited_it->id)
                        ->update([
                            'amount' => $invited_it->amount + $proc_money,
                        ]);
                }

            if ($referal_isset) {
				Referral::create([
					'user_id' => $invited_it->id,
					'referral_id' => auth()->user()->id,
					'amount' => $proc_money,
					'percent' => $percent
				]);
            }

            if ($invited_it->invited_it != 0) {
                $this->referral_calculation($invited_it->invited_it, $suma, $mylvl);
                //return $invited_it->invited_it;
            } else {
                return 'success';
            }
        } else {
            return 'success';
        }

    }
	
}
