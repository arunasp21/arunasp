<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Percent extends Model
{
    protected $fillable = [
        'amount',
        'deposit_id',
        'user_id',
    ];
}
