<?php

namespace App\Entity;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    public $table = 'user_detail';

    protected $fillable = [
        'referral_lvl',
        'referral_url',
        'invited_it',
        'amount',
        'user_id',
        'nickname',
        'payeer_account',
        'perfect_money_account',
        'qiwi_account',
        'yandex_money_account',
        'bitcoin_account',
        'card_account',
    ];
	
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
	
}
