<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable = [
        'founded_date',
        'start_date',
        'day_works',
        'count_users',
        'all_input',
        'all_output',
        'last_user',
        'last_input',
        'last_output',
    ];
}
