<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class LastStatistic extends Model
{
    protected $fillable = [
        'user_1',
        'user_2',
        'user_3',
        'user_4',
        'user_5',
        'user_6',
        'user_7',
        'user_8',
        'user_9',
        'user_10',
        'amount_1',
        'amount_2',
        'amount_3',
        'amount_4',
        'amount_5',
        'amount_6',
        'amount_7',
        'amount_8',
        'amount_9',
        'amount_10',
    ];
}
