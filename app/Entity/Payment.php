<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id',
        'payment_system',
        'payment_type',
        'wallet',
        'amount',
        'status',
        'completed_at',
        'order'
    ];

    const EXPECTED = 1;
    const COMPLETE = 2;
    const DENIED = 3;

    const OUTPUT = 2;
    const INPUT = 1;

    const PERFECT = 1;
    const PAYEER = 2;
    const QIWI = 3;
    const VISA_MASTERCARD = 4;
    const BITCOIN = 5;

    public static $paymentStatus = [
        self::EXPECTED => 'expected',
        self::COMPLETE => 'complete',
        self::DENIED => 'denied',
    ];

    public static $paymentType = [
        self::INPUT => 'input',
        self::OUTPUT => 'output',
    ];

    public static $paymentSystem = [
        self::PERFECT => 'Perfect Money',
        self::PAYEER => 'Payeer',
        self::QIWI => 'Qiwi',
        self::VISA_MASTERCARD => 'Visa/Mastercard',
        self::BITCOIN => 'BitCoin',
    ];
}
