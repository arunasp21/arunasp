<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'percent',
        'day',
        'from_amount',
        'to_amount',
        'title',
    ];
}
