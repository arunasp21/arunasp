<?php

namespace App;

use App\Entity\UserDetail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\MailResetPasswordToken;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'skype',
        'last_login',
		'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function sendPasswordResetNotification($token)
	{
		$this->notify(new MailResetPasswordToken($token));
	}

    public function detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id', 'id');
    }

    const LVL_1 = 2;
    const LVL_2 = 3;
    const LVL_3 = 4;

    public static $percentReferrals = [
        self::LVL_1 => 10,
        self::LVL_2 => 5,
        self::LVL_3 => 2,
    ];

    public const ADMIN = 1;
    public const USER = 2;
}
