<?php

use App\Entity\Plan;
use Illuminate\Database\Seeder;


class PlanTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            1 => [
                'percent' => 2,
                'day' => 7,
                'from_amount' => 100,
                'to_amount' => 500,
                'title' => 'Silver',
            ],
            2 => [
                'percent' => 4,
                'day' => 5,
                'from_amount' => 500,
                'to_amount' => 1000,
                'title' => 'Gold',
            ],
            3 => [
                'percent' => 6,
                'day' => 3,
                'from_amount' => 1000,
                'to_amount' => 5000,
                'title' => 'Platinum',
            ],
            4 => [
                'percent' => 8,
                'day' => 1,
                'from_amount' => 5000,
                'to_amount' => 10000,
                'title' => 'Brilliant',
            ]
        ];

        foreach ($data as $item){
            Plan::create([
                'percent' => $item['percent'],
                'day' => $item['day'],
                'from_amount' => $item['from_amount'],
                'to_amount' => $item['to_amount'],
                'title' => $item['title'],
            ]);
        }
    }
}