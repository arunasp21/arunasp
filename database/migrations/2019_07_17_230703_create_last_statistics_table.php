<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_1')->nullable()->default(null);
            $table->string('user_2')->nullable()->default(null);
            $table->string('user_3')->nullable()->default(null);
            $table->string('user_4')->nullable()->default(null);
            $table->string('user_5')->nullable()->default(null);
            $table->string('user_6')->nullable()->default(null);
            $table->string('user_7')->nullable()->default(null);
            $table->string('user_8')->nullable()->default(null);
            $table->string('user_9')->nullable()->default(null);
            $table->string('user_10')->nullable()->default(null);
            $table->string('amount_1')->nullable()->default(null);
            $table->string('amount_2')->nullable()->default(null);
            $table->string('amount_3')->nullable()->default(null);
            $table->string('amount_4')->nullable()->default(null);
            $table->string('amount_5')->nullable()->default(null);
            $table->string('amount_6')->nullable()->default(null);
            $table->string('amount_7')->nullable()->default(null);
            $table->string('amount_8')->nullable()->default(null);
            $table->string('amount_9')->nullable()->default(null);
            $table->string('amount_10')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_statistics');
    }
}
