<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('founded_date')->nullable()->default(null);
            $table->string('start_date')->nullable()->default(null);
            $table->string('day_works')->nullable()->default(null);
            $table->string('count_users')->nullable()->default(null);
            $table->string('all_input')->nullable()->default(null);
            $table->string('all_output')->nullable()->default(null);
            $table->string('last_user')->nullable()->default(null);
            $table->string('last_input')->nullable()->default(null);
            $table->string('last_output')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
