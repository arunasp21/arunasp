<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayAccoutsInUserDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_detail', function (Blueprint $table) {
            $table->string('payeer_account')->nullable()->default(null);
            $table->string('perfect_money_account')->nullable()->default(null);
            $table->string('qiwi_account')->nullable()->default(null);
            $table->string('yandex_money_account')->nullable()->default(null);
            $table->string('bitcoin_account')->nullable()->default(null);
            $table->string('card_account')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_detail', function (Blueprint $table) {
            $table->dropColumn('payeer_account');
            $table->dropColumn('perfect_money_account');
            $table->dropColumn('qiwi_account');
            $table->dropColumn('yandex_money_account');
            $table->dropColumn('bitcoin_account');
            $table->dropColumn('card_account');
        });
    }
}
