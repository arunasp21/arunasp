<?php

Route::get('payeer_902403141.txt', function () { return '902403141'; });

Route::group(['prefix' => LaravelLocalization::setLocale()], function() {

    Auth::routes();

    Route::get('/', 'HomeController@index')->name('welcome');
    Route::get('/started', 'HomeController@started')->name('started');
    Route::get('/faq', 'HomeController@faq')->name('faq');
    Route::get('/rules', 'HomeController@rules')->name('rules');
    Route::get('/reviews', 'HomeController@reviews')->name('reviews');
    Route::get('/support', 'HomeController@support')->name('support');
    Route::get('/about', 'HomeController@about')->name('about');
    Route::get('/legal-doc', 'HomeController@legalDoc')->name('legal.doc');
    Route::get('/news', 'HomeController@news')->name('news');
    Route::get('/full-text', 'HomeController@full_text')->name('full-text');
    Route::get('/card', 'HomeController@card')->name('card');
    Route::get('/statistics', 'HomeController@statistics')->name('statistics');
    Route::get('/anti-spam', 'HomeController@antiSpam')->name('anti.spam');
    Route::get('/policy', 'HomeController@policy')->name('policy');
    Route::get('/check_email', 'HomeController@check_email')->name('check_email');

    Route::post('save-comment', 'CommentController@store')->name('save.comment');
    Route::post('send-question-in-mail', 'HomeController@sendQuestionInMail')->name('send.question.mail');
    Route::post('send-modal-form', 'HomeController@sendModalForm')->name('send.modal.form');

    Route::group(['prefix' => 'account', 'middleware' => 'auth'], function () {

        Route::get('/profile', 'AccountController@index')->name('account');
        Route::get('/deposit-history', 'AccountController@depositHistory')->name('deposit.list');
        Route::get('/invest-history', 'AccountController@investHistory')->name('invest.list');
        Route::get('/payment-history', 'AccountController@paymentHistory')->name('payment.list');
        Route::get('/referral', 'AccountController@referral')->name('referral');
        Route::get('/banners', 'AccountController@reklama')->name('banners');
        Route::get('/settings', 'AccountController@setting')->name('settings');
        Route::get('/card', 'AccountController@card')->name('card');

        Route::get('/invest', 'AccountController@invest')->name('invest');
        Route::get('/deposit', 'AccountController@deposit')->name('deposit');
        Route::get('/payment', 'AccountController@payment')->name('payment');

        Route::post('update-password', 'AccountController@updatePassword')->name('update.password');
        Route::post('update-pay-accounts', 'AccountController@updatePayAccounts')->name('update.pay.accounts');

    });
	
    Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
        Route::resource('plans', 'PlanController');
        Route::get('approve-comment/{comment}', 'CommentController@approve')->name('approve.comment');
        Route::get('delete-comment/{comment}', 'CommentController@destroy')->name('delete.comment');
        Route::get('comments', 'AccountController@moderationComments')->name('moderation.comments');
        Route::resource('deposit', 'DepositController');
//        Route::post('payment-output', 'PaymentController@paymentOutput')->name('payment.output');
        Route::get('users', 'AccountController@users')->name('users');
//change_all//		
        Route::get('change_all/{user}', 'AccountController@change_all')->name('change_all');
//change_all-end//		
        Route::get('statistics', 'AccountController@statistics')->name('statistics.admin');
        Route::post('statistics', 'AccountController@updateStatistics')->name('update.statistics');
        Route::post('statistics-input', 'AccountController@updateStatisticsInput')->name('update.statistics.last.input');
        Route::get('statistics-input', 'AccountController@statisticsInput')->name('statistics.last.input');
        Route::post('statistics-output', 'AccountController@updateStatisticsOutput')->name('update.statistics.last.output');
        Route::get('statistics-output', 'AccountController@statisticsOutput')->name('statistics.last.output');
        Route::get('change-status/{user}', 'AccountController@changeStatus')->name('change.status');
    });

    Route::get('/verification-email', 'AccountController@verificationEmail');

});
//*****************************SYSTEM PAY********************
/**
 *              PAYEER
 */

Route::post('/pay-payeer', 'PaySystemController@PayeerInit');
Route::get('/pay-payeer-error', 'PaySystemController@PayeerError');
Route::get('/pay-payeer-success', 'PaySystemController@PayeerSuccess');
Route::get('/pay-payeer-status', 'PaySystemController@PayeerStatus');

/**
 *              PERFECT MONEY
 */
Route::post('/pay-perfect-money', 'PaySystemController@Perfect_MoneyInit');
Route::get('/pay-perfect-money-error', 'PaySystemController@Perfect_MoneyError');
Route::get('/pay-perfect-money-success', 'PaySystemController@Perfect_MoneySuccess');

/**
 *              Advanced Cash
 */
Route::post('/pay-adv-cash', 'PaySystemController@AdvCashInit');
Route::get('/pay-adv-cash-error', 'PaySystemController@AdvCashError');
Route::get('/pay-adv-cash-success', 'PaySystemController@AdvCashSuccess');
Route::get('/pay-adv-cash-status', 'PaySystemController@AdvCashStatus');

/*				FREE-KASSA							*/

Route::get('/fk-error', 'PaySystemController@FkError');
Route::get('/fk-success', 'PaySystemController@FkSuccess');
Route::get('/fk-status', 'PaySystemController@FKStatus');


//*********************************************************************

Route::post('upload-avatar', 'AccountController@UploadAvatar')->name('upload.avatar');
Route::post('add-deposit', 'AccountController@AddDeposit')->name('add.deposit');
Route::post('add-top', 'AccountController@AddTop')->name('add.top');
Route::post('add-other', 'AccountController@AddRefOther')->name('add.other');
Route::post('add-ref', 'AccountController@AddRef')->name('add.ref');
Route::post('edit-profile', 'AccountController@EditProfile')->name('edit.profile');
Route::post('payment-output', 'PaymentController@paymentOutput')->name('payment.output');

/* Cron */
Route::get('/cron-run-percent-793196380', 'PercentController@runPercent');
Route::get('/send_mail', 'AccountController@runSend');